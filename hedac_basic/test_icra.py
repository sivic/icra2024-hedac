# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 18:32:03 2015

@author: stefan
"""

from HEDAC_basic import *
import numpy as np
from matplotlib import image

image = np.mean(image.imread('icra2024.png'), axis=2)[::-1, :]
print(f'{image.shape=}')

test = HEDAC_basic()

test.method = 'hedac'
test.results_dir = 'test_icra/hedac_full'
test.sigma_m = 2
test.sigma_c = 2

# test.method = 'smc'
# test.results_dir = 'test_icra/smc_full'
# test.sigma_m = 0.01
# test.sigma_c = 0.01

test.X = np.arange(image.shape[1])
test.Y = np.arange(image.shape[0])
test.T = np.arange(5001)

test.samples = image
test.alpha = 1e-1
test.beta = 1e-3
test.gamma = 1e-2
test.va = 0.5
test.sigma_ac = 0.01
test.sourcefun = difsquaredsource
# logsource, difsource, difsquaredsource, divsource, fullcoveragecource generate_difpowersource(0.5) generate_divpowersource(power=2.0)

test.outputStep = 2

"""
agents = []
for iA in range(8):
    xa, ya = -100, -100
    while not m(xa, ya) > 0.0:
        xa = np.random.uniform(0.0, 1.0) 
        ya = np.random.uniform(0.0, 1.0)
    agents.append([xa, ya])
test.agents = agents
test.agents = [[0.125,0.1], [0.125,0.9], [0.875,0.1], [0.875,0.9], [0.5, 0.5]]
test.agents = [[0.125,0.1], [0.125,0.3], [0.5,0.5], [0.875,0.7], [0.875,0.9]]
"""
test.agents = [(5 + 5 * i, 5 + 5 * i) for i in range(10)]
        
test.search()