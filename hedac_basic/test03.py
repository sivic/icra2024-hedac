# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 18:32:03 2015

@author: stefan
"""

import numpy as np
from HEDAC_basic import *




def m(x, y):
    
    _m1 = np.sin(x-np.pi) ** 2  * np.sin(y-np.pi) ** 2 
    _m2 = np.sin(x/2)*np.sin(y/2)/10
    _m3 = np.cos(x*y / 2*np.pi) + 2
    #_m3 = 1.0
    return (_m1 + _m2) / _m3
    return _m1 / _m3
    
test = HEDAC_basic()

"""
test.method = 'hedac'
test.results_dir = 'test_03/hedac_full'
"""
test.method = 'smc'
test.results_dir = 'test_03/smc_full'



test.X = np.linspace(-0.0, 2.0*np.pi, 501)
test.Y = np.linspace(-0.0, 2.0*np.pi, 501)
test.T = np.linspace(0, 200, 10001)
test.samples = m
test.alpha = 1.0
test.beta = 1.0
test.gamma = 100.0
test.kappa = 0.0

test.va = 1.0
test.sigma_m = np.pi/100.
test.sigma_c = np.pi/100.
test.sigma_ac = np.pi/50.
test.sourcefun=difsquaredsource 
# logsource, difsource, difsquaredsource, divsource, fullcoveragecource generate_difpowersource(0.5) generate_divpowersource(power=2.0)
#test.outputStep=5
test.outputStep=3

"""
agents = []
for iA in range(5):
    xa, ya = np.nan, np.nan
    while not m(xa, ya) > 0.0:
        xa = np.random.uniform(0.0, 2*np.pi) 
        ya = np.random.uniform(0.0, 2*np.pi)
    agents.append([xa, ya])
test.agents = agents
"""

#test.agents = [[x, y] for x,y in zip(np.linspace(0.5*np.pi, 1.5*np.pi, 20), np.linspace(0.8*np.pi, 1.2*np.pi, 20))]

test.agents = []
for x in np.linspace(0.3*np.pi, 1.7*np.pi, 5):
    for y in np.linspace(0.5*np.pi, 1.5*np.pi, 4):
        test.agents.append([x, y])
        
test.search()
#test.smc_search()