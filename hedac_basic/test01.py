# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 18:32:03 2015

@author: stefan
"""

from HEDAC_basic import *
import numpy as np


def m(x, y):
    _m = 0.0
    if x >= 0.2 and x <= 0.5 and y >= 0.1 and y <= 0.4:
        _m = 1.0
    if x >= 0.5 and x <= 0.7 and y >= 0.6 and y <= 0.9:
        _m = 1.0
    if x >= 0.1 and x <= 0.9 and y >= 0.4 and y <= 0.6:
        _m = 1.0

    return _m


    
test = HEDAC_basic()

# test.method = 'hedac'
# test.results_dir = 'test_01/hedac_full'
# test.sigma_m = 0.01
# test.sigma_c = 0.01

test.method = 'smc'
test.results_dir = 'test_01/smc_full'
test.sigma_m = 0.01
test.sigma_c = 0.01

test.X = np.linspace(0, 1, 251)
test.Y = np.linspace(0, 1, 251)
test.T = np.linspace(0, 1, 5001)

test.samples = m
test.alpha = 1.0
test.beta = 50.0
test.gamma = 100.0
test.va = 20
test.sigma_ac = 0.01
test.sourcefun = difsquaredsource
# logsource, difsource, difsquaredsource, divsource, fullcoveragecource generate_difpowersource(0.5) generate_divpowersource(power=2.0)

test.outputStep = 2
"""
agents = []
for iA in range(5):
    xa, ya = None, None
    while not m(xa, ya) > 0.0:
        xa = np.random.uniform(0.0, 1.0)
        ya = np.random.uniform(0.0, 1.0)
    agents.append([xa, ya])
test.agents = agents
"""
test.agents = [[0.2, 0.56], [0.35, 0.53], [0.5, 0.5], [0.65, 0.47], [0.8, 0.44]]
#test.agents = [[0.2, 0.56], [0.5, 0.5], [0.8, 0.44]]

test.search()

       
        