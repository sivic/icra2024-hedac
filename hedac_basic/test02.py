# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 18:32:03 2015

@author: stefan
"""

from HEDAC_basic import *
import numpy as np


def m(x, y):
    _m = 1.0
    if x < 0.0 or x > 1.0 or y < 0.0 or y > 1.0:
        _m = 0.0
        
    for xc in np.linspace(0.0, 1.0, 5):
        for yc in np.linspace(0, 1, 6):
            if np.sqrt( (x - xc) ** 2 + (y - yc) ** 2) < 0.08:
                _m = 0.0
        
    return _m

test = HEDAC_basic()

test.method = 'hedac'
test.results_dir = 'test_02/hedac_full'
test.sigma_m = 0.01
test.sigma_c = 0.01

# test.method = 'smc'
# test.results_dir = 'test_02/smc_full'
# test.sigma_m = 0.01
# test.sigma_c = 0.01

test.X = np.linspace(0, 1, 401)
test.Y = np.linspace(0, 1, 401)
test.T = np.linspace(0, 1, 5001)

test.samples = m
test.alpha = 1.0
test.beta = 20.0
test.gamma = 1000.0
test.va = 20
test.sigma_ac = 0.01
test.sourcefun = difsquaredsource
# logsource, difsource, difsquaredsource, divsource, fullcoveragecource generate_difpowersource(0.5) generate_divpowersource(power=2.0)

test.outputStep = 2

"""
agents = []
for iA in range(8):
    xa, ya = -100, -100
    while not m(xa, ya) > 0.0:
        xa = np.random.uniform(0.0, 1.0) 
        ya = np.random.uniform(0.0, 1.0)
    agents.append([xa, ya])
test.agents = agents
test.agents = [[0.125,0.1], [0.125,0.9], [0.875,0.1], [0.875,0.9], [0.5, 0.5]]
test.agents = [[0.125,0.1], [0.125,0.3], [0.5,0.5], [0.875,0.7], [0.875,0.9]]
"""
test.agents = [[0.125,0.1], [0.375, 0.3], [0.625, 0.5], [0.875, 0.7], [0.125, 0.9], [0.25, 0.9]]
        
test.search()