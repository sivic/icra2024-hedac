# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 18:32:03 2015

@author: stefan
"""

#import matplotlib
#print(matplotlib.get_backend())
#matplotlib.use('agg')
#print(matplotlib.get_backend())
#print(matplotlib.matplotlib_fname())

from hedac_spraying import *
import numpy as np
import sys
from shapely.geometry import Point, Polygon
import os


"""
Defining spraying region
"""
polygon = Polygon(((50, 12.5), (125, 12.5), (237.5, 200), (175, 237.5), (12.5, 162.5)))
def m(x, y):
    
    if polygon.contains(Point(x,y)):
        return 0.0075
    else:
        return 0.0

"""
Spraying pattern
"""    
def phi(x, y):
    
    A = [0.004, 0.004, 0.004]
    sx2 = 1.5**2
    sy2 = 1.0**2
    s = A[0] * np.exp(- (x**2 / 2 / sx2 + y**2 / 2 / sy2 )) + \
        A[1] * np.exp(- ((x-5)**2 / 2 / sx2 + y**2 / 2 / sy2 )) + \
        A[2] * np.exp(- ((x+5)**2 / 2 / sx2 + y**2 / 2 / sy2 ))
    return  s
        
"""
Single run function
"""
def run_test_01(method, motion, switch=True, run=-1):
    
    print()
    print('Running HEDAC spraying' )
    print(' method: ' + method)
    print(' motion: ' + motion)
    print(' switch: ' + str(switch))
    print(' run:    ' + str(run))
    print()
    
    test = HEDAC_SPRAYING()
    
    test.case_name = 'test_01'
    test.method = method
    test.motion_model = motion
    if run < 0:
        test.results_dir = '../results/test_01/%s_%s_%s' % (method, motion, 'switch' if switch else 'noswitch')
    else:
        test.results_dir = '../results/test_01/stats/%s_%s_%s_%03d' % (method, motion, 'switch' if switch else 'noswitch', run)
    
    if os.path.exists(test.results_dir):
        print(f'Results directory {test.results_dir} already exists, skipping simulation')
    
    test.X = np.linspace(0, 250, 251)
    test.Y = np.linspace(0, 250, 251)
    test.T = np.linspace(0, 600, 1201)
    #test.T = np.linspace(0, 1500, 6001)
    
    test.samples = m
    test.spraying_pattern = phi
    test.alpha = 3.0e-2
    test.beta = 4.0
    test.phi_threshold_ratio = 0.01
    test.support_radius = 10.0
    test.plot_options['c_levels'] = np.linspace(0, 0.01, 11)
    test.plot_options['phi_levels'] = np.linspace(0, 0.005, 6)
    test.plot_options['dc_orders'] = [0.1, 0.01, 0.001]
    test.plot_options['spray_pattern_plot'] = [10, 5, 5.0, 0.5, 9.5, 4.5, 0.02, 0.96, 0.06, 0.95, 0.965, 0.35]
    test.spray_switching = switch
    
    test.lawnmower_hull = polygon.convex_hull
    test.min_distance = 11.0
    test.CAWD = 20
    
    if run < 0:
        # Creating figures each n steps
        test.outputStep = 1
        # Saving state at given times
        test.t_state_save = [0.0, 60.0, 120.0, 300.0, 600.0]
    else:
        # Creating figures each n steps
        test.outputStep = 0
        # Saving state at given times
        test.t_state_save = [600.0]
        
    
    VA = [5.0] * 5
    RM = [7.5] * 5
    
    agents = []
    if method == 'hedac':
        #if os.path.exists('test02_agents.txt'):
        A = np.loadtxt('../results/test_01/lawnmower_dubins_switch/agent_parameters.txt')
        XA = A[:,0]
        YA = A[:,1]
        VXA = A[:,2]
        VYA = A[:,3]
        DIR = np.arctan2(VYA, VXA)
    else:
        XA = np.array([230.0, 140.392225, 32.887301,  14.548920, 232.054486])
        YA = np.array([175.0,  27.158072, 52.271895, 134.442200, 204.197233])
        DIR = np.array([0.5, -0.9, -0.3, 0.3, 0.9]) * np.pi
    
    for iA in range(5):
        xa = XA[iA]
        ya = YA[iA]

        if run < 0:
            direction = (iA) * 2 * np.pi / 5 + np.pi
            if test.method == 'hedac':
                direction = DIR[iA]
        else:
            xa, ya = -1e10, -1e10
            while not m(xa, ya) > 0.0:
                xa = np.random.uniform(test.X[0], test.X[-1])
                ya = np.random.uniform(test.Y[0], test.Y[-1])
            if test.method == 'lawnmower':
                direction = np.pi
            else:
                direction = np.random.uniform(0.0, 2.0*np.pi)
            
        agent = {}
        agent['x'] = xa
        agent['y'] = ya
        agent['vx'] = np.cos(direction)
        agent['vy'] = np.sin(direction)
        agent['va'] = VA[iA]
        agent['rm'] = RM[iA]
        #agent['dp'] = DP[iA]
        
        agents.append(agent)
        
    test.agents = agents
    #test.agents = 'test01_agent_parameters.txt'
    test.search()
       

if __name__ == "__main__":
    
    print('*' * 30)
    print('*' + 'Test 1'.center(28) + '*')
    print('*' * 30)
    print()
    
    narg = len(sys.argv) - 1
    #print('Number of arguments:', narg, 'arguments.')
    #print('Argument List:', str(sys.argv[1:]))
    
    CONTROL_METHODS = {'h': 'hedac',
                       's': 'smc',
                       'l': 'lawnmower'}
    MOTION_MODELS = {'k': 'kinematic',
                     'd': 'dubins',
                     'cd': 'cadubins'}
    
    if narg == 0:
        
        print('No arguments given. Please specifiy run options.\n')
                
        method = ''
        while not method in ['h', 'l', 's']:
            method = input('Select control method HEDAC/Lawnmower/SMC ([h]/l/s): ')
            if method == '': method = 'h'

        motion = ''
        while not motion in ['d', 'k', 'cd']:
            motion = input('Select motion model Dubins/Kinematic/CADubins (d/k/[cd]): ')
            if motion == '': motion = 'cd'
            
        switch = ''
        while not switch in ['y', 'n']:
            switch = input('Turn on spraying switching ([y]/n): ')
            if switch == '': switch = 'y'
        switch = switch == 'y'
         
        nruns = input('Number of runs [hit return for detailed run]: ') 
        if nruns.isnumeric():
            nruns = int(nruns)
            for irun in range(nruns):
                run_test_01(CONTROL_METHODS[method], MOTION_MODELS[motion], switch, irun)
                
        else:
            run_test_01(CONTROL_METHODS[method], MOTION_MODELS[motion], switch)
            
    elif narg == 2:
        
        run_test_01(sys.argv[1], sys.argv[2])
        
    elif narg == 3:
        
        nruns = int(sys.argv[3])
        for irun in range(nruns):
            run_test_01(sys.argv[1], sys.argv[2], True, irun)
    else:
        print('Error: unexpected number of arguments!')
    
    
