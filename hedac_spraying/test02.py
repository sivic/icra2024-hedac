# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 18:32:03 2015

@author: stefan
"""

from hedac_spraying import *
import numpy as np
import sys
from shapely.geometry import Point, Polygon
import os



"""
Defining spraying region
"""
poly1 = Polygon([( 25.0, 25.0), ( 19.0, 295.0), (115.0, 271.0), (130.0,  55.0)])
poly2 = Polygon([(157.0, 40.0), (178.0, 295.0), (295.0, 280.0), (268.0, 124.0)])
poly3 = Polygon([(166.0, 16.0), (268.0, 115.0), (295.0,  25.0)])
polygon = poly1.union(poly2).union(poly3)
def m(x, y):
    
    p = Point(x,y)
    _m = 0.0
    if poly1.contains(p):        
        _m = 0.0105 * np.exp(- ((x-100)**2 / (2 * 40**2) + (y-250)**2 / (2 * 40**2 )))
        _m += 0.008 * np.exp(- ((x-40)**2 / (2 * 70**2) + (y-60)**2 / (2 * 70**2 )))        
    elif poly2.contains(p):
        _m = 0.01 * np.exp(- ((x-250)**2 / (2 * 120**2) + (y-270)**2 / (2 * 120**2 )))
    elif poly3.contains(p):
        _m = 0.004
    return _m


"""
Spraying pattern
"""    
def phi(x, y):
    
    A = [0.004, 0.004, 0.004]
    sx2 = 1.5**2
    sy2 = 1.0**2
    s = A[0] * np.exp(- (x**2 / 2 / sx2 + y**2 / 2 / sy2 )) + \
        A[1] * np.exp(- ((x-5)**2 / 2 / sx2 + y**2 / 2 / sy2 )) + \
        A[2] * np.exp(- ((x+5)**2 / 2 / sx2 + y**2 / 2 / sy2 ))
    return  s


"""
Single run function
"""
def run_test_02(method, motion, switch=True, run=-1):
    
    print()
    print('Running HEDAC spraying' )
    print(' method: ' + method)
    print(' motion: ' + motion)
    print(' switch: ' + str(switch))
    print(' run:    ' + str(run))
    print()
    
    test = HEDAC_SPRAYING()
    
    test.case_name = 'test_02'
    test.method = method
    test.motion_model = motion
    if run < 0:
        test.results_dir = '../results/test_02/%s_%s_%s' % (method, motion, 'switch' if switch else 'noswitch')
    else:
        test.results_dir = '../results/test_02/stats/%s_%s_%s_%03d' % (method, motion, 'switch' if switch else 'noswitch', run)
    
    if os.path.exists(test.results_dir):
        return
    
    test.X = np.linspace(0, 320, 321)
    test.Y = np.linspace(0, 320, 321)
    #test.T = np.linspace(0, 1200, 2401)
    if run < 0:
        test.T = np.linspace(0, 600, 1201)
    else:
        #test.T = np.linspace(0, 1200, 2401)
        test.T = np.linspace(0, 600, 1201)
    #test.T = np.linspace(0, 6000, 12001)
    
    test.samples = m
    test.spraying_pattern = phi
    test.alpha = 3.0e-2
    test.beta = 4.0
    test.phi_threshold_ratio = 0.01
    test.support_radius = 10.0
    test.plot_options['c_levels'] = np.linspace(0, 0.02, 11)
    test.plot_options['phi_levels'] = np.linspace(0, 0.005, 6)
    test.plot_options['dc_orders'] = [0.1, 0.01, 0.001]
    test.plot_options['spray_pattern_plot'] = [10, 5, 5.0, 0.5, 9.5, 4.5, 0.02, 0.96, 0.06, 0.95, 0.965, 0.35]
    test.spray_switching = switch
    
    test.lawnmower_hull = polygon.convex_hull      
    test.min_distance = 11.0
    test.CAWD = 35
    
    
    # Creating figures each n steps
    if run < 0:
        test.outputStep = 1
        # Saving state at given times
        test.t_state_save = [0, 60, 120, 300, 600, 900, 1200]
    else:
        test.outputStep = 0
        # Saving state at given times
        test.t_state_save = [600, 1200]
        
    
    
    agents = []
    
    VA = [5.0] * 8
    RM = [7.5] * 8
    if method == 'hedac':
        #if os.path.exists('test02_agents.txt'):
        A = np.loadtxt('../results/test_02/lawnmower_dubins_switch/agent_parameters.txt')
        XA = A[:,0]
        YA = A[:,1]
        VXA = A[:,2]
        VYA = A[:,3]
        DIR = np.arctan2(VYA, VXA)
    else:
        XA = np.array([285.082900, 300.303301, 24.783743, 19.060359, 28.105949,  44.243956,  19.383815, 300.303301])
        YA = np.array([282.689935,  49.569882, 27.231554, 48.331984, 24.323025, 300.303301, 285.228342, 261.489315])
        DIR = np.array([-2, -3, 0, -1, 2, -3, 0, 3]) * 0.25 * np.pi
    
    #print(DIR/np.pi)
    #input()
    
    for iA in range(7):
        xa = XA[iA]
        ya = YA[iA]

        if run < 0:
            direction = (iA) * 2.0 * np.pi / 7.0 #- np.pi
            if test.method == 'hedac':
                direction = DIR[iA]
        else:
            xa, ya = -1e10, -1e10
            while not m(xa, ya) > 0.0:
                xa = np.random.uniform(test.X[0], test.X[-1])
                ya = np.random.uniform(test.Y[0], test.Y[-1])
                direction = np.random.uniform(0.0, 2.0*np.pi)
            
        agent = {}
        agent['x'] = xa
        agent['y'] = ya
        agent['vx'] = np.cos(direction)
        agent['vy'] = np.sin(direction)
        agent['va'] = VA[iA]
        agent['rm'] = RM[iA]
        #agent['dp'] = DP[iA]
        
        agents.append(agent)
        
    test.agents = agents
    #test.agents = 'test01_agent_parameters.txt'
    
    test.search()
       

if __name__ == "__main__":
    
    print('*' * 30)
    print('*' + 'Test 2'.center(28) + '*')
    print('*' * 30)
    print()
    
    narg = len(sys.argv) - 1
    #print('Number of arguments:', narg, 'arguments.')
    #print('Argument List:', str(sys.argv[1:]))
    
    CONTROL_METHODS = {'h': 'hedac',
                       's': 'smc',
                       'l': 'lawnmower'}
    MOTION_MODELS = {'k': 'kinematic',
                     'd': 'dubins',
                     'cd': 'cadubins'}
    
    if narg == 0:
        
        print('No arguments given. Please specifiy run options.\n')
                
        method = ''
        while not method in ['h', 'l', 's']:
            method = input('Select control method HEDAC/Lawnmower/SMC ([h]/l/s): ')
            if method == '': method = 'h'

        motion = ''
        while not motion in ['d', 'k', 'cd']:
            motion = input('Select motion model Dubins/Kinematic/CADubins (d/k/[cd]): ')
            if motion == '': motion = 'cd'
            
        switch = ''
        while not switch in ['y', 'n']:
            switch = input('Turn on spraying switching ([y]/n): ')
            if switch == '': switch = 'y'
        switch = switch == 'y'
        
        nruns = input('Number of runs [hit return for detailed run]: ') 
        if nruns.isnumeric():
            nruns = int(nruns)
            for irun in range(nruns):
                run_test_02(CONTROL_METHODS[method], MOTION_MODELS[motion], switch, irun)
                
        else:
            run_test_02(CONTROL_METHODS[method], MOTION_MODELS[motion], switch)
        
    elif narg == 2:
        
        run_test_02(sys.argv[1], sys.argv[2])
        
    elif narg == 3:
        
        nruns = int(sys.argv[3])
        for irun in range(nruns):
            run_test_02(sys.argv[1], sys.argv[2], True, irun)
    else:
        print('Error: unexpected number of arguments!')
    
    
