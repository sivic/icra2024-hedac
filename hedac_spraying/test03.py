# -*- coding: utf-8 -*-
"""
Created on Sat Sep  9 18:06:08 2017

@author: aleksandr-pc
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 18:32:03 2015

@author: stefan
"""

#import matplotlib
#print(matplotlib.get_backend())
#matplotlib.use('agg')
#print(matplotlib.get_backend())
#print(matplotlib.matplotlib_fname())

from hedac_spraying import *
import numpy as np
import sys
import scipy.interpolate as intrp
from shapely.geometry import Point, Polygon, MultiPoint
import os


"""
Defining spraying region
"""
#polygon = Polygon([(50, 12.5), (125, 12.5), (237.5, 200), (175, 237.5), (12.5, 162.5)])

mdata = np.load('m.npz')
#mX = mdata['x']
#mY = mdata['y']
#M = mdata['m']
mY = mdata['x']
mX = mdata['y']
Mf = mdata['m'].T
ny, nx = np.shape(Mf)
#print(ny, nx)
dx = (mX[1] - mX[0])
dy = (mY[1] - mY[0])

mX = np.arange(nx + 20) * dx
mY = np.arange(ny + 20) * dy
M = np.zeros([ny + 20, nx + 20])
M[10:-10,10:-10] = Mf * 0.01
print(M.shape)
print(dx, dy)
ny, nx = np.shape(M)

"""
def m(x, y):
    ix = np.argmin(np.abs(mX-x))
    iy = np.argmin(np.abs(mY-y))
    return M[iy,ix]*0.01
"""
m = intrp.interp2d(mX, mY, M)
#mXm, mYm = np.meshgrid(mX, mY)
#m = intrp.interp2d(mXm, mYm, M.T)

"""
print(M.max())

import matplotlib.pyplot as plt
plt.contourf(mX, mY, M)
plt.contour(mX, mY, M, [5e-4])
plt.show()
"""

"""
Spraying pattern
"""    
def phi(x, y):
    
    A = [0.001, 0.001, 0.001, 0.001, 0.001]
    sx2 = 1.5**2
    sy2 = 1.0**2
    s = A[0] * np.exp(- (x**2 / 2 / sx2 + y**2 / 2 / sy2 )) + \
        A[1] * np.exp(- ((x-5)**2 / 2 / sx2 + y**2 / 2 / sy2 )) + \
        A[2] * np.exp(- ((x+5)**2 / 2 / sx2 + y**2 / 2 / sy2 )) + \
        A[3] * np.exp(- ((x+2.5)**2 / 2 / sx2 + (y-4)**2 / 2 / sy2 )) + \
        A[4] * np.exp(- ((x-2.5)**2 / 2 / sx2 + (y-4)**2 / 2 / sy2 ))
    return  s

def phi2(x, y):
    
    A = np.array([0.005, 0.005, 0.005, 0.005, 0.005])
    s1 = A[0] * (-x**2 / 2.5**2 - y**2 / 0.5**2 + 1.0)
    s1[s1 < 0.0] = 0.0
    s2 = A[1] * (-(x-5.0)**2 / 2.5**2 - y**2 / 0.5**2 + 1.0)
    s2[s2 < 0.0] = 0.0
    s3 = A[2] * (-(x+5.0)**2 / 2.5**2 - y**2 / 0.5**2 + 1.0)
    s3[s3 < 0.0] = 0.0
    s4 = A[3] * (-(x+2.5)**2 / 2.5**2 - (y-1.0)**2 / 0.5**2 + 1.0)
    s4[s4 < 0.0] = 0.0
    s5 = A[4] * (-(x-2.5)**2 / 2.5**2 - (y-1.0)**2 / 0.5**2 + 1.0)
    s5[s5 < 0.0] = 0.0
    s = s1+s2+s3+s4+s5
    return  s

def phi_new(x, y):
    
    Xn = [0.0,   7.0,    -7.0,   3.5,    -3.5]
    Yn = [1.0,   1.0,    1.0,    -1.0,   -1.0]
    An = np.ones([5]) * 0.01
    
    theta = np.deg2rad(15.0)
    
    S = np.zeros_like(x)
    for xn, yn, an in zip(Xn, Yn, An):
        rx = xn + (x - xn) * np.cos(theta) + (y - yn) * np.sin(theta)
        ry = yn + (x - xn) * np.sin(theta) - (y - yn) * np.cos(theta)
        s = an * (1.0 - (rx - xn)**2 / 2.5**2 - (ry - yn)**2 / 0.5**2)
        s[s < 0.0] = 0.0
        S += s
    return S
        
"""
Single run function
"""
def run_test_03(method, motion, switch=True, run=-1):
    
    print()
    print('Running HEDAC spraying' )
    print(' method: ' + method)
    print(' motion: ' + motion)
    print(' switch: ' + str(switch))
    print(' run:    ' + str(run))
    print()
    
    test = HEDAC_SPRAYING()
    
    test.case_name = 'test_03'
    test.method = method
    test.motion_model = motion
    if run < 0:
        test.results_dir = '../results/test_03/%s_%s_%s' % (method, motion, 'switch' if switch else 'noswitch')
    else:
        test.results_dir = '../results/test_03/stats/%s_%s_%s_%03d' % (method, motion, 'switch' if switch else 'noswitch', run)
    
    if os.path.exists(test.results_dir):
        return
    
    lx = np.floor(np.max(mX) / 1.5) * 1.5
    ly = np.floor(np.max(mY) / 1.5) * 1.5
    test.X = np.linspace(0, lx, 2*int(lx/1.5)+1)
    test.Y = np.linspace(0, ly, 2*int(ly/1.5)+1)
    if run < 0:
        if method == 'hedac':
            test.T = np.linspace(0, 25*60, 25*60*4+1)
        else:
            test.T = np.linspace(0, 25*60, 50*60*4+1)
    else:
        #test.T = np.linspace(0, 50*60, 50*60*4+1)
        test.T = np.linspace(0, 25*60, 25*60*4+1)
        
    
    test.samples = m
    test.spraying_pattern = phi_new
    test.alpha = 5.0e-3
    test.beta = 20.0
    test.phi_threshold_ratio = 0.01
    test.support_radius = 15.0
    #test.FDT = [1.0]
    test.plot_options['plot_width'] = 26.2
    test.plot_options['left'] = 0.015
    test.plot_options['wspace'] = 0.2
    test.plot_options['cbar_pad'] = 0.005
    test.plot_options['c_levels'] = np.linspace(0, 0.012, 13)
    test.plot_options['phi_levels'] = np.linspace(0, 0.01, 6)
    test.plot_options['dc_orders'] = [0.1, 0.01, 0.001]
    test.plot_options['spray_pattern_plot'] = [10, 3.2, 5.0, 0.5, 9.5, 4.5, 0.02, 0.95, 0.07, 0.94, 0.955, 0.435]
    test.spray_switching = switch
    
     
    points = [(660, 292),
              (660, 274),
              (556, 104),
              (469, 52),
              (198, 30),
              (136, 50),
              (65, 206),
              (130, 346),
              (182, 418),
              (244, 477),
              (380, 477),
              (437, 38),
              (195, 26),
              (150, 30),
              (59, 221),
              (63, 331),
              (68, 343),
              (103, 381),
              (128, 400),
              (202, 448),
              (219, 460)]
    points = MultiPoint(points)
    
    test.lawnmower_hull = points.convex_hull
    #test.lawnmower_hull = polygon.convex_hull       
    test.min_distance = 15.0
    test.CAWD = 24
    
    
    if run < 0:
        # Creating figures each n steps
        test.outputStep = 4
        # Saving state at given times
        test.t_state_save = [0.0, 60.0, 120.0, 300.0, 600.0, 900.0, 1200.0, 1800.0, 2400.]
    else:
        # Creating figures each n steps
        test.outputStep = 0
        # Saving state at given times
        #test.t_state_save = [2400.0, 3000.0]
        test.t_state_save = [1500]
        
    
    nagents = 10
    VA = [4.5] * nagents
    RM = [8.5] * nagents
    
    if method == 'hedac':
        #if os.path.exists('test02_agents.txt'):
        A = np.loadtxt('../results/test_03/lawnmower_dubins_switch/agent_parameters.txt')
        XA = A[:,0]
        YA = A[:,1]
        VXA = A[:,2]
        VYA = A[:,3]
        DIR = np.arctan2(VYA, VXA)
    else:
        XA = np.random.uniform(mX.min(), mX.max(), nagents)
        YA = np.random.uniform(mY.min(), mY.max(), nagents)
        DIR = np.random.uniform(0, 2*np.pi, nagents)
    
    agents = []
    for iA in range(nagents):
        xa = XA[iA]
        ya = YA[iA]

        if run < 0:
            direction = (iA) * 2 * np.pi / nagents + np.pi
            if test.method == 'hedac':
                direction = DIR[iA]
        else:
            xa, ya = -1e10, -1e10
            while not m(xa, ya) > 0.0:
                xa = np.random.uniform(test.X[0], test.X[-1])
                ya = np.random.uniform(test.Y[0], test.Y[-1])
            if test.method == 'lawnmower':
                direction = np.pi
            else:
                direction = np.random.uniform(0.0, 2.0*np.pi)
            
        agent = {}
        agent['x'] = xa
        agent['y'] = ya
        agent['vx'] = np.cos(direction)
        agent['vy'] = np.sin(direction)
        agent['va'] = VA[iA]
        agent['rm'] = RM[iA]
        #agent['dp'] = DP[iA]
        
        agents.append(agent)
        
    test.agents = agents
    #test.agents = 'test01_agent_parameters.txt'
    
    test.search()
       

if __name__ == "__main__":
    
    print('*' * 30)
    print('*' + 'Test 3'.center(28) + '*')
    print('*' * 30)
    print()
    
    narg = len(sys.argv) - 1
    #print('Number of arguments:', narg, 'arguments.')
    #print('Argument List:', str(sys.argv[1:]))
    
    CONTROL_METHODS = {'h': 'hedac',
                       's': 'smc',
                       'l': 'lawnmower'}
    MOTION_MODELS = {'k': 'kinematic',
                     'd': 'dubins',
                     'cd': 'cadubins'}
    
    if narg == 0:
        
        print('No arguments given. Please specifiy run options.\n')
                
        method = ''
        while not method in ['h', 'l', 's']:
            method = input('Select control method HEDAC/Lawnmower/SMC ([h]/l/s): ')
            if method == '': method = 'h'

        motion = ''
        while not motion in ['d', 'k', 'cd']:
            motion = input('Select motion model Dubins/Kinematic (d/k/[cd]): ')
            if motion == '': motion = 'cd'
            
        switch = ''
        while not switch in ['y', 'n']:
            switch = input('Turn on spraying switching ([y]/n): ')
            if switch == '': switch = 'y'
        switch = switch == 'y'
        
        nruns = input('Number of runs [hit return for detailed run]: ') 
        if nruns.isnumeric():
            nruns = int(nruns)
            for irun in range(nruns):
                run_test_03(CONTROL_METHODS[method], MOTION_MODELS[motion], switch, irun)
                
        else:
            run_test_03(CONTROL_METHODS[method], MOTION_MODELS[motion], switch)
        
    elif narg == 2:
        
        run_test_03(sys.argv[1], sys.argv[2])
        
    elif narg == 3:
        
        nruns = int(sys.argv[3])
        for irun in range(nruns):
            run_test_03(sys.argv[1], sys.argv[2], True, irun)
    else:
        print('Error: unexpected number of arguments!')
    
    
