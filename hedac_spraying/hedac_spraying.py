# -*- coding: utf-8 -*-
"""
Created on Sat Feb 14 17:25:16 2015

@author: stefan
"""

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from matplotlib.collections import PatchCollection
from matplotlib.colors import Normalize
import matplotlib.ticker as tkr
import matplotlib.colors as colors
import scipy.interpolate as intrp
import os
import shutil
import scipy.sparse as sprs
from scipy.sparse.linalg import splu
import subprocess
import datetime
from scipy.fftpack import dct, idct
from shapely.geometry import Point, MultiPoint, LineString
from shapely import affinity


# rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
# rc('text', usetex=True)

# CM = np.loadtxt('cmap.txt').reshape(256,3)
# print np.shape(CM)

class MidPointNorm(Normalize):
    def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
        Normalize.__init__(self, vmin, vmax, clip)
        self.midpoint = midpoint

    def __call__(self, value, clip=None):
        if clip is None:
            clip = self.clip

        result, is_scalar = self.process_value(value)

        self.autoscale_None(result)
        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

        if not (vmin < midpoint < vmax):
            raise ValueError("midpoint must be between maxvalue and minvalue.")
        elif vmin == vmax:
            result.fill(0)  # Or should it be all masked? Or 0.5?
        elif vmin > vmax:
            raise ValueError("maxvalue must be bigger than minvalue")
        else:
            vmin = float(vmin)
            vmax = float(vmax)
            if clip:
                mask = mpl.getmask(result)
                result = mpl.array(np.clip(result.filled(vmax), vmin, vmax),
                                   mask=mask)

            # ma division is very slow; we can take a shortcut
            resdat = result.data

            # First scale to -1 to 1 range, than to from 0 to 1.
            resdat -= midpoint
            resdat[resdat > 0] /= abs(vmax - midpoint)
            resdat[resdat < 0] /= abs(vmin - midpoint)

            resdat /= 2.
            resdat += 0.5
            result = np.ma.array(resdat, mask=result.mask, copy=False)

        if is_scalar:
            result = result[0]
        return result

    def inverse(self, value):
        if not self.scaled():
            raise ValueError("Not invertible until scaled")
        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

        if mpl.cbook.iterable(value):
            val = np.asarray(value)
            val = 2 * (val - 0.5)
            val[val > 0] *= abs(vmax - midpoint)
            val[val < 0] *= abs(vmin - midpoint)
            val += midpoint
            return val
        else:
            val = 2 * (val - 0.5)
            if val < 0:
                return val * abs(vmin - midpoint) + midpoint
            else:
                return val * abs(vmax - midpoint) + midpoint


class MidpointNormalize(colors.Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))


def DCT(field):
    return dct(dct(field.T, norm='ortho').T, norm='ortho')


def IDCT(coefficients):
    return idct(idct(coefficients.T, norm='ortho').T, norm='ortho')


# Plots a Polygon to pyplot `ax`
def plot_polygon(ax, poly, **kwargs):
    path = Path.make_compound_path(
        Path(np.asarray(poly.exterior.coords)[:, :2]),
        *[Path(np.asarray(ring.coords)[:, :2]) for ring in poly.interiors])

    patch = PathPatch(path, **kwargs)
    collection = PatchCollection([patch], **kwargs)

    ax.add_collection(collection, autolim=True)
    ax.autoscale_view()
    return collection


class HEDAC_SPRAYING():

    def __init__(self):

        self.start_time = datetime.datetime.now()
        self.X = np.linspace(0, 1, 200)
        self.Y = np.linspace(0, 1, 200)
        self.T = np.linspace(0, 1, 101)
        self.t_state_save = []

        self.case_name = 'case'
        self.results_dir = None
        self.alpha = 1.0
        self.beta = 1.0
        self.kappa = 0.
        self.nu = 0.01
        # self.va = None
        self.outputStep = 2
        # self.number_of_agents = 0
        self.agents = 0
        self.samples = None
        self.method = 'hedac'
        self.motion_model = 'kinematic'
        self.spraying_pattern = None
        self.spray_switching = True
        self.spray_threshold = None
        self.phi_threshold_ratio = 0.01
        self.support_radius = 10.0

        self.min_distance = 11.0

        self.FDT = [0.25, 0.5, 0.75, 1.0]

        self.lawnmower_finished = 0
        self.sit = -1
        self.lawnmower_hull = None

        # Plot options
        self.plot_options = {}
        self.plot_options['c_levels'] = np.linspace(0, 1.1, 12)
        self.plot_options['dc_levels'] = np.linspace(-1.1, 1.1, 23)
        self.plot_options['qiver_size'] = 50

        self.plot_options['plot_width'] = 20
        self.plot_options['plot_height'] = 14.1
        self.plot_options['left'] = 0.012
        self.plot_options['right'] = 0.97
        self.plot_options['wspace'] = 0.3
        self.plot_options['hspace'] = 0.1
        self.plot_options['top'] = 0.99
        self.plot_options['bottom'] = 0.035
        self.plot_options['cbar_pad'] = -0.02
        # self.plot_options['spray_pattern_plot'] = [10, 5, 5.0, 0.5, 9.5, 4.5, 0.965, 0.35]

    def write_info(self):

        info = open(self.results_dir + '/info.txt', 'w')

        info.write('*' * 50 + '\n')
        info.write('*' + 'H E D A C   s e a r c h'.center(48) + '*\n')
        info.write('*' * 50 + '\n')

        info.write('Started on: ' + str(self.start_time) + '\n')

        info.write('\n')
        info.write('Parameters:\n')

        info.write('%15s: %10s\n' % ('method', self.method))
        info.write('%15s: %10.3f\n' % ('x_min', self.X[0]))
        info.write('%15s: %10.3f\n' % ('x_max', self.X[-1]))
        info.write('%15s: %10.3f\n' % ('dx', self.dx))
        info.write('%15s: %10.3f\n' % ('y_min', self.Y[0]))
        info.write('%15s: %10.3f\n' % ('y_max', self.Y[-1]))
        info.write('%15s: %10.3f\n' % ('dy', self.dy))

        info.write('\n')
        info.write('%15s: %10.3f\n' % ('alpha', self.alpha))
        info.write('%15s: %10.3f\n' % ('beta', self.beta))
        info.write('%15s: %10.3f\n' % ('kappa', self.kappa))
        info.write('%15s: %10.3f\n' % ('nu', self.nu))
        # info.write('%15s: %10.3f\n' % ('sigma_m', self.sigma_m) )
        # info.write('%15s: %10.3f\n' % ('sigma_c', self.sigma_c) )
        # info.write('%15s: %10.3f\n' % ('sigma_ac', self.sigma_ac) )

        info.write('\n')
        info.write('%15s: %10d\n' % ('agents', len(self.agents)))

        akeys = ['x', 'y', 'vx', 'vy', 'va', 'rm']
        info.write(' ' * 17 + ''.join(['%10s' % ak for ak in akeys]) + '\n')
        for a in self.agents:
            # info.write( ' '*15 + '%10.3f, %10.3f %10.3f, %10.3f\n' % tuple(a))
            info.write(' ' * 17)
            for key in akeys:
                info.write('%10.3f' % (a[key]))
            info.write('\n')

        info.close()

    def init(self):

        print('Initializing')

        """
        Delete and recreate results directory
        """
        if os.path.exists(self.results_dir):
            shutil.rmtree(self.results_dir)
        # if not os.path.exists(self.results_dir):
        os.makedirs(self.results_dir)

        """
        Discretization
        """
        self.dx = self.X[1] - self.X[0]
        self.dy = self.Y[1] - self.Y[0]
        self.nx = np.size(self.X)
        self.ny = np.size(self.Y)
        self.Xc = np.linspace(self.X[0] - 0.5 * self.dx, self.X[-1] + 0.5 * self.dx, self.nx + 1)
        self.Yc = np.linspace(self.Y[0] - 0.5 * self.dy, self.Y[-1] + 0.5 * self.dy, self.ny + 1)
        self.area = ((self.X[-1] - self.X[0]) * (self.Y[-1] - self.Y[0]))
        self.dt = self.T[1] - self.T[0]

        self.Xmg, self.Ymg = np.meshgrid(self.X, self.Y)

        # Integration weights
        self.IW = np.ones_like(self.Xmg)
        self.IW[0, :] = 0.5
        self.IW[-1, :] = 0.5
        self.IW[:, 0] = 0.5
        self.IW[:, -1] = 0.5
        self.IW[0, 0] = 0.25
        self.IW[0, -1] = 0.25
        self.IW[-1, 0] = 0.25
        self.IW[-1, -1] = 0.25
        self.IW *= self.dx * self.dy

        # print self.dx
        # print self.Xc
        self.it_state_save = []
        for tss in self.t_state_save:
            self.it_state_save.append(np.argmin(np.abs(tss - self.T)))

        """
        Goal coverage initialization
        """
        self.m = np.zeros([self.ny, self.nx])
        if hasattr(self.samples, '__call__'):
            # Defined as probability function
            for iY, y in enumerate(self.Y):
                for iX, x in enumerate(self.X):
                    self.m[iY, iX] = self.samples(x, y)

        elif isinstance(self.samples, basestring):
            # Defined as file containing samples locations

            XM, YM = np.loadtxt(self.samples, delimiter=',', unpack=True)
            """
            # scale and center
            scale = 1.1 * np.max([(np.max(XM) - np.min(XM)), (np.max(YM) - np.min(YM))])
            self.XM = (XM - np.min(XM)) / scale
            self.YM = (YM - np.min(YM)) / scale
            xc = 0.5 * (np.min(self.XM) + np.max(self.XM))
            yc = 0.5 * (np.min(self.YM) + np.max(self.YM))
            self.XM += (0.5 - xc)
            self.YM += (0.5 - yc)
            """
            self.m, _, _ = np.histogram2d(self.YM, self.XM, bins=[self.Yc, self.Xc])

        else:
            raise Exception('Probability initialization failed!')

        self.m  # /= np.sum(self.m)
        self.plot_options['s_max_log'] = np.ceil(np.log10(np.max(self.m)))
        # print('s_max_log', self.plot_options['s_max_log'])

        self.Vm = np.sum(self.m * self.IW)
        #
        # self.ms = filters.gaussian_filter(self.m, self.sigma_m/self.dx)

        # self.mk = DCT(self.ms)

        # print np.min(self.ms), np.max(self.ms), np.mean(self.ms)

        """
        Agent initialization
        """
        keys = 'x y vx vy va rm'.split()

        if type(self.agents) == str:
            # Load agent parameters from file
            agents = []
            agent_params_file = open(self.agents, 'r')
            lines = agent_params_file.readlines()
            for line in lines:
                a = {}
                for k, p in zip(keys, line.split()):
                    a[k] = float(p)
                agents.append(a)
            agent_params_file.close()
            self.agents = agents

        self.XA = [[a['x']] for a in self.agents]
        self.YA = [[a['y']] for a in self.agents]
        self.VXA = [[a['vx']] for a in self.agents]
        self.VYA = [[a['vy']] for a in self.agents]

        for xa, ya, vxa, vya in zip(self.XA, self.YA, self.VXA, self.VYA):
            print('%.5f %.5f   - %.5f %.5f' % (xa[0], ya[0], vxa[0], vya[0]))

        self.ky, self.kx = np.meshgrid(np.arange(self.ny), np.arange(self.nx))
        self.E = []

        self.c = np.zeros([self.ny, self.nx])
        self.sc = np.zeros([self.ny, self.nx])
        self.u = np.zeros([self.ny, self.nx])

        if self.method == 'hedac':

            A = sprs.lil_matrix((self.nx * self.ny, self.nx * self.ny))
            self.B = np.zeros(self.nx * self.ny)

            i = 0
            for iY in range(self.ny):
                for iX in range(self.nx):
                    #
                    k = [-2., -2., 1., 1., 1., 1.]
                    if iY == 0:
                        k[1] -= 2. * self.kappa * self.dy
                        k[4] = 2.0
                        k[5] = 0.0
                    if iY == self.ny - 1:
                        k[1] -= 2. * self.kappa * self.dy
                        k[4] = 0.0
                        k[5] = 2.0
                    if iX == 0:
                        k[0] -= 2. * self.kappa * self.dx
                        k[2] = 2.0
                        k[3] = 0.
                    if iX == self.nx - 1:
                        k[0] -= 2. * self.kappa * self.dx
                        k[2] = 0.
                        k[3] = 2.0

                    A[i, i] = (k[0] / self.dx ** 2 + k[1] / self.dy ** 2) - self.beta / self.alpha / self.area

                    if iX != self.nx - 1:
                        A[i, i + 1] = k[2] / self.dx ** 2
                    if iX != 0:
                        A[i, i - 1] = k[3] / self.dx ** 2
                    if iY != self.ny - 1:
                        A[i, i + self.nx] = k[4] / self.dy ** 2
                    if iY != 0:
                        A[i, i - self.nx] = k[5] / self.dy ** 2

                    i += 1
            A = A.tocsc()
            self.lu = splu(A)

        if self.method == 'smc':
            self.ms = np.log(self.m / 1e-7)
            self.ms[self.ms < 0.0] = 0.0
            print('max m:', np.max(self.m))
            print('Log cutos m:', np.sum(self.m[self.ms > 0.0]))
            self.ms = self.ms / np.sum(self.ms)

            self.mk = DCT(self.ms)

        if self.method == 'lawnmower':

            print(f'{self.lawnmower_hull=}')
            if self.lawnmower_hull == None:
                points = []
                for ix, x in enumerate(self.X):
                    for iy, y in enumerate(self.Y):
                        if self.m[iy, ix] > 0.0:
                            points.append((x, y))
                points = MultiPoint(points)

                self.lawnmower_hull = points.convex_hull
            print(f'{self.lawnmower_hull=}')

            # xyh = np.array(self.lawnmower_hull.exterior.coords)
            minx, miny, maxx, maxy = self.lawnmower_hull.bounds
            print('Lawnmower hull bounds: [%f, %f] x [%f, %f]' % (minx, maxx, miny, maxy))
            print('Lawnmower hull area: %f' % (self.lawnmower_hull.area))

            self.XA = []
            self.YA = []
            self.VXA = []
            self.VYA = []

            self.LWNI = []
            self.LWNX = []
            self.LWNY = []

            for ia in np.arange(len(self.agents)):

                center = [0.5 * (self.X[0] + self.X[-1]), 0.5 * (self.Y[0] + self.Y[-1])]
                # direction = np.random.uniform(0, 2*np.pi)
                direction = np.arctan2(self.agents[ia]['vy'], self.agents[ia]['vx'])
                dx = np.cos(direction)
                dy = np.sin(direction)

                rot_hull = affinity.rotate(self.lawnmower_hull, direction, use_radians=True, origin=center)
                minx, miny, maxx, maxy = rot_hull.bounds

                # vert = np.abs(self.agents[ia]['vy']) > np.abs(self.agents[ia]['vx'])

                rm = self.agents[ia]['rm']

                DX = np.arange(minx + rm, maxx - rm + 1e-6, 2 * rm)
                DY = np.arange(miny + rm, maxy - rm + 1e-6, 2 * rm)
                # DX += 0.5 * (maxx - DX[-1])
                # DY += 0.5 * (maxy - DY[-1])

                DX += np.random.uniform() * (maxx - DX[-1] - rm)
                DY += np.random.uniform() * (maxy - DY[-1] - rm)

                lwn_x, lwn_y = [], []

                drc_up = np.random.uniform() > 0.5

                DDX = np.hstack([DX, DX[-2:0:-1]])
                # DDX = DX # Single pass
                # print(DX)
                # print(DDX)
                for dx in DDX:
                    line = LineString([(dx, miny), (dx, maxy)])
                    points = line.intersection(rot_hull)

                    lwn_x.append(dx)
                    lwn_x.append(dx)
                    if drc_up:
                        lwn_y.append(points.coords[0][1])
                        lwn_y.append(points.coords[1][1])
                    else:
                        lwn_y.append(points.coords[1][1])
                        lwn_y.append(points.coords[0][1])

                    drc_up = not drc_up

                for i in range(-1, np.size(lwn_y) - 1, 2):
                    y_mid = 0.5 * (lwn_y[i] + lwn_y[i + 1])
                    lwn_y[i] = y_mid
                    lwn_y[i + 1] = y_mid

                LWN = MultiPoint([(x, y) for x, y in zip(lwn_x, lwn_y)])
                rot_lwn = affinity.rotate(LWN, -direction, use_radians=True, origin=center)

                # nseg = len(rot_lwn)
                nseg = len(rot_lwn.geoms)
                lwn_x, lwn_y = [], []
                for seg in range(nseg):
                    x, y = rot_lwn.geoms[seg].coords[0]
                    lwn_x.append(x)
                    lwn_y.append(y)

                xa = lwn_x[0]
                ya = lwn_y[0]

                self.LWNI.append(1)
                self.LWNX.append(lwn_x)
                self.LWNY.append(lwn_y)

                vx = lwn_x[1] - lwn_x[0]
                vy = lwn_y[1] - lwn_y[0]
                vm = np.sqrt(vx ** 2 + vy ** 2)
                vx = vx / vm
                vy = vy / vm

                self.XA.append([xa])
                self.YA.append([ya])
                self.VXA.append([vx])
                self.VYA.append([vy])

                self.agents[ia]['x'] = xa
                self.agents[ia]['y'] = ya
                self.agents[ia]['vx'] = vx
                self.agents[ia]['vy'] = vy

                s = 0.0
                for i in range(1, len(lwn_x)):
                    s += np.sqrt((lwn_x[i] - lwn_x[i - 1]) ** 2 + (lwn_y[i] - lwn_y[i - 1]) ** 2)

                print('Agent %d: (%8.2f, %8.2f) (%8.2f, %8.2f) s:%f t_total:%f' % (
                ia, xa, ya, vx, vy, s, s / self.agents[ia]['va']))

            plt.figure(figsize=(20, 20))
            ax = plt.gca()
            ax.set_title('Goal density')
            ax.contourf(self.X, self.Y, self.m, np.linspace(0, self.plot_options['c_levels'][-1], 201),
                        cmap=plt.cm.viridis, alpha=0.1)
            ax.axis('image')
            plot_polygon(ax, self.lawnmower_hull)
            for ia in range(len(self.agents)):
                ax.plot(self.LWNX[ia], self.LWNY[ia], ls='--', lw=2, c='black')
                # ax.plot(self.LWNX[ia][self.LWNI[ia]], self.LWNY[ia][self.LWNI[ia]], 'gs')

            plt.savefig(self.results_dir + '/lawnmower.png')
            plt.close()

            # input('Press return to continue...')

        # Save agent parameters to file
        agent_params_file = open(self.results_dir + '/agent_parameters.txt', 'w')
        params_lines = []

        for agent in self.agents:
            params = ['%f' % agent[k] for k in keys]
            param_line = ' '.join(params)
            params_lines.append(param_line)

        agent_params_file.write('\n'.join(params_lines))
        agent_params_file.close()

        self.dc_levels = []
        nlvl = 50
        no = len(self.plot_options['dc_orders'])
        om = - 10.0 ** np.linspace(np.log10(self.plot_options['dc_orders'][0]),
                                   np.log10(self.plot_options['dc_orders'][-1]), (no - 1) * nlvl + 1)
        oz = np.linspace(-self.plot_options['dc_orders'][-1], self.plot_options['dc_orders'][-1], nlvl + 1)
        op = 10.0 ** np.linspace(np.log10(self.plot_options['dc_orders'][-1]),
                                 np.log10(self.plot_options['dc_orders'][0]), (no - 1) * nlvl + 1)
        for o in om:
            self.dc_levels.append(o)
        for o in oz[1:-1]:
            self.dc_levels.append(o)
        for o in op:
            self.dc_levels.append(o)
        # print(dc_levels)
        # vert = not vert

        if self.motion_model == 'cadubins':
            col_stats = open(self.results_dir + '/collision_avoidance_stats.txt', 'a')
            col_stats.write('Time ' + ''.join(['%12s' % ('Agent ' + str(ia)) for ia in range(len(self.agents))]))
            col_stats.close()

    def coverage(self, iAgent=None):

        dC = np.zeros([self.ny, self.nx])
        for ia, (XA, YA, VXA, VYA) in enumerate(zip(self.XA, self.YA, self.VXA, self.VYA)):

            if iAgent != None and iAgent != ia:
                continue

            mask = (self.Xmg - XA[-1]) ** 2 + (self.Ymg - YA[-1]) < self.support_radius ** 2

            for xa, ya, vxa, vya in zip(XA[-len(self.FDT):], YA[-len(self.FDT):], VXA[-len(self.FDT):],
                                        VYA[-len(self.FDT):]):
                mask = (self.Xmg - xa) ** 2 + (self.Ymg - ya) < self.support_radius ** 2

                theta = np.arctan2(vya, vxa)
                RX = (self.Xmg - xa) * np.cos(theta) + (self.Ymg - ya) * np.sin(theta)
                RY = (self.Xmg - xa) * np.sin(theta) - (self.Ymg - ya) * np.cos(theta)

                dC[mask] += self.spraying_pattern(RY[mask], RX[mask])
                # dC += self.spraying_pattern(RY, RX)

        return dC * self.dt / len(self.FDT)

    def convergence(self):

        # self.s = self.m/np.sum(self.m) - self.c/np.sum(self.c)
        # self.s[self.s < 0] = 0.0

        e = self.m - self.sc
        e1 = np.copy(e)
        e2 = -np.copy(e)
        e1[e1 < 0] = 0.0
        e2[e2 < 0] = 0.0

        E1 = np.sum(e1 * self.IW) / self.Vm
        E2 = np.sum(e2 * self.IW) / self.Vm
        E = E1 + E2
        # print(E1)
        # print(E2)
        # print(E)
        Vc = np.sum(self.sc * self.IW)

        self.E.append([E, E1, E2, self.Vm, Vc])
        # print(self.E)

        fajl = open(self.results_dir + '/convergence.txt', 'a')
        line = '%15.6e ' % self.T[self.it]
        line += ' '.join(['%15.6e' % e for e in self.E[-1]])
        fajl.write(line + '\n')
        fajl.close()

    def search(self):

        self.init()

        # print('prob: %f' % (1-np.exp(- self.detection_probability * self.dt)))
        self.write_info()
        self.plot_case_summary()

        fajl = open(self.results_dir + '/convergence.txt', 'w')
        fajl.write('%15s %15s %15s %15s %15s %15s\n' % ('t', 'Total', 'Undercoverage', 'Overcoverage', 'Vm', 'Vc'))
        fajl.close()

        if self.outputStep > 0:
            fajl = open(self.results_dir + '/agents.txt', 'w')
            fajl.write('%d\n' % len(self.XA))
            fajl.close()

        search_metod_step = None
        if self.method == 'hedac':
            print('HEDAC Search')
            search_metod_step = self.hedac_step


        elif self.method == 'smc':
            print('SMC Search')
            search_metod_step = self.smc_step


        elif self.method == 'lawnmower':
            search_metod_step = self.lawnmower_step


        else:
            print('ERROR\n' * 5)
            print('\n' * 3)
            print('Invalid coverage method')
            print('\n' * 3)

        self.sit = -1
        self.it = 0
        self.plot_solution()
        self.sit = 0

        """
        Main loop
        """
        for self.it, self.t in enumerate(self.T):

            # dc = np.zeros_like(self.c)
            for ia in range(len(self.agents)):
                dc = self.coverage(ia)
                self.c += dc

                if self.method == 'hedac':
                    point = Point(self.XA[ia][-1], self.YA[ia][-1])
                    # if (np.average((self.m - self.sc - dc)[dc > 0.0]) > 0 and self.lawnmower_hull.contains(point)) or not self.spray_switching:
                    if np.average((self.m - self.sc - dc)[dc > self.spray_threshold]) > 0 or not self.spray_switching:
                        self.sc += dc

                if self.method == 'lawnmower':
                    point = Point(self.XA[ia][-1], self.YA[ia][-1])
                    # if (np.average((self.m - self.sc - dc)[dc > 0.0]) > 0 and self.lawnmower_hull.contains(point)) or not self.spray_switching:
                    if np.average((self.m - self.sc - dc)[dc > self.spray_threshold]) > 0 or not self.spray_switching:
                        self.sc += dc

                if self.method == 'smc':
                    point = Point(self.XA[ia][-1], self.YA[ia][-1])
                    # if (np.average((self.m - self.sc - dc)[dc > 0.0]) > 0 and self.lawnmower_hull.contains(point)) or not self.spray_switching:
                    if np.average((self.m - self.sc - dc)[dc > self.spray_threshold]) > 0 or not self.spray_switching:
                        self.sc += dc

            self.s = self.m / np.sum(self.m) - self.c / np.sum(self.c)
            self.s[self.s < 0] = 0.0

            self.convergence()
            # self.save_positions()

            VAX, VAY = search_metod_step()

            self.agent_motion(VAX, VAY)

            # self.output()

            if self.it in self.it_state_save:
                self.save_state()

            print('%.5f %8.4e %8.4e %8.4e %8.4e' % (self.t, self.E[-1][0], self.E[-1][1], self.E[-1][2], self.c.max()))

            if self.outputStep > 0:
                if self.it % self.outputStep == 0:
                    self.plot_solution()
                    self.sit += 1

            if self.lawnmower_finished > 0:
                print('Lawnmower stop! %d' % self.lawnmower_finished)

        self.sit = -2
        self.plot_solution()
        # self.save_state()
        print('the end!')

    def hedac_step(self):

        i = 0
        for iY in range(self.ny):
            for iX in range(self.nx):
                self.B[i] = - self.s[iY, iX] / self.alpha / self.area  # + 1e5*ac[iY, iX]
                i += 1

        z = self.lu.solve(self.B)

        i = 0
        for iY in range(self.ny):
            for iX in range(self.nx):
                self.u[iY, iX] = z[i]
                i += 1

        self.uy, self.ux = np.gradient(self.u)

        vax = intrp.interp2d(self.X, self.Y, self.ux, kind='linear')
        vay = intrp.interp2d(self.X, self.Y, self.uy, kind='linear')

        VAX, VAY = [], []
        for ia, (xa, ya) in enumerate(zip(self.XA, self.YA)):
            va_x = vax(xa[-1], ya[-1])[0]
            va_y = vay(xa[-1], ya[-1])[0]
            van = np.sqrt(va_x ** 2 + va_y ** 2)
            if van < 1e-20:
                va_x = xa[-1] - xa[-2]
                va_y = ya[-1] - ya[-2]
                van = np.sqrt(va_x ** 2 + va_y ** 2)
                print('### zero gradient')
            va_x /= van
            va_y /= van
            VAX.append(va_x)
            VAY.append(va_y)

        return VAX, VAY

    def agent_motion(self, VAX, VAY):

        if self.motion_model == 'cadubins':
            VAXn, VAYn = np.copy(VAX), np.copy(VAY)
            collision_avoidance = False
            na = np.size(VAX)
            d_min = self.min_distance
            CAW = np.zeros(na)

            """
            plt.figure()
            plt.axis('equal')
            ax = plt.gca()
            """
            for ia1, (xa1, ya1, va_x1, va_y1) in enumerate(zip(self.XA, self.YA, VAX, VAY)):

                R = self.agents[ia1]['rm']
                x_1 = xa1[-1]
                y_1 = ya1[-1]

                DD = 0
                for ia2, (xa2, ya2, va_x2, va_y2) in enumerate(zip(self.XA, self.YA, VAX, VAY)):
                    if ia1 == ia2:
                        continue
                    x_2 = xa2[-1]
                    y_2 = ya2[-1]
                    dd = np.sqrt((x_1 - x_2) ** 2 + (y_1 - y_2) ** 2)
                    DD += d_min / dd

                RR = DD * self.CAWD
                # print('DD', DD, RR)

                _lx, _ly = self.VYA[ia1][-1], -self.VXA[ia1][-1]
                _rx, _ry = -self.VYA[ia1][-1], self.VXA[ia1][-1]
                lx, ly = x_1 + _lx * R, y_1 + _ly * R
                rx, ry = x_1 + _rx * R, y_1 + _ry * R

                plt.plot(x_1, y_1, 'k.')
                # plt.plot(x_1n, y_1n, 'b.', lw=1)
                _vx1 = self.VXA[ia1][-1]
                _vy1 = self.VYA[ia1][-1]
                _vm = np.sqrt(_vx1 ** 2 + _vy1 ** 2)
                _vx1 = _vx1 * self.agents[ia1]['va'] / _vm
                _vy1 = _vy1 * self.agents[ia1]['va'] / _vm
                """
                plt.plot([x_1, x_1 + _vx1 * self.dt*5],
                         [y_1, y_1 + _vy1 * self.dt*5], 'b-', lw=1)
                
                c1 = Point(lx, ly).buffer(d_min + R + RR)
                c2 = Point(rx, ry).buffer(d_min + R + RR)
                cc = c1.intersection(c2)
                patch = PolygonPatch(cc, fc='gray', ec='gray', alpha=0.1, zorder=1)
                ax.add_patch(patch)
                
                c = Point(x_1, y_1).buffer(d_min * 0.5)
                patch = PolygonPatch(c, fc='red', ec='k', alpha=0.2, zorder=1)
                ax.add_patch(patch)
                """

                VXYW = np.zeros([na, 3])
                VXYW[ia1, 0] = va_x1
                VXYW[ia1, 1] = va_y1
                VXYW[ia1, 2] = 1

                for ia2, (xa2, ya2, va_x2, va_y2) in enumerate(zip(self.XA, self.YA, VAX, VAY)):

                    if ia1 == ia2:
                        continue
                    x_2 = xa2[-1]
                    y_2 = ya2[-1]
                    # dl = np.sqrt((x_2 - lx)**2 + (y_2 - ly)**2)
                    # dr = np.sqrt((x_2 - rx)**2 + (y_2 - ry)**2)
                    dc = np.sqrt((x_2 - x_1) ** 2 + (y_2 - y_1) ** 2)

                    # if dl < d_min + R + RR or dr < d_min + R + RR:# or dc < d_min + R:
                    if dc < d_min + R + RR:
                        collision_avoidance = True
                        """
                        if dl < dr:
                            vx, vy = -_vy1, _vx1
                            w = (d_min + R + RR - dr) / (RR * 0.5)
                        else:
                            vx, vy = _vy1, -_vx1
                            w = (d_min + R + RR - dl) / (RR * 0.5)
                        """
                        vx, vy = x_1 - x_2, y_1 - y_2
                        w = (d_min + R + RR - dc) / (RR * 0.5)

                        if w > 1:
                            w = 1
                        elif w < 0:
                            w = 0
                        else:
                            w = w ** 2
                        vm = np.sqrt(vx ** 2 + vy ** 2)
                        vx /= vm
                        vy /= vm

                        VXYW[ia2, 0] = vx
                        VXYW[ia2, 1] = vy
                        VXYW[ia2, 2] = w

                        """
                        plt.text(x_1 +self.agents[ia1]['va'] * vx * self.dt*5, 
                                 y_1 + self.agents[ia1]['va'] * vy * self.dt*5, 
                                 '%.2f' % (w))
                        plt.plot([x_1, x_1 +self.agents[ia1]['va'] * vx * self.dt*5], [y_1, y_1 + self.agents[ia1]['va'] * vy * self.dt*5], 'g-', lw=1)
                        """

                if np.sum(VXYW[:, 2]) > 1.2:
                    VXYW[ia1, 2] = 0.01
                VXYW[:, 2] = VXYW[:, 2] / np.sum(VXYW[:, 2])
                VAXn[ia1] = np.dot(VXYW[:, 0], VXYW[:, 2])
                VAYn[ia1] = np.dot(VXYW[:, 1], VXYW[:, 2])
                vm = np.sqrt(VAXn[ia1] ** 2 + VAYn[ia1] ** 2)

                VXYW[ia1, 2] = 0.0
                CAW[ia1] = np.sum(VXYW[:, 2])

                VAXn[ia1] /= vm
                VAYn[ia1] /= vm

                """
                plt.plot([x_1, x_1 + self.agents[ia1]['va'] * VAXn[ia1] * self.dt*5],
                         [y_1, y_1 + self.agents[ia1]['va'] * VAYn[ia1] * self.dt*5], 'r-', lw=1)
                """

            """
            plt.title('t=%0.1f s' % self.t)
            ax.set_xlim(self.X[0], self.X[-1])
            ax.set_ylim(self.Y[0], self.Y[-1])
            plt.savefig(self.results_dir + '/col_%06d.png' % (self.sit))
            plt.close()
            """

            col_stats = open(self.results_dir + '/collision_avoidance_stats.txt', 'a')
            col_stats.write('\n%5.1f' % self.t + ''.join(['%12.5f' % caw for caw in CAW]))
            col_stats.close()

            if collision_avoidance:
                VAX = VAXn
                VAY = VAYn
                # input(' >> press return to continue')

        for ia, (xa, ya, va_x, va_y) in enumerate(zip(self.XA, self.YA, VAX, VAY)):

            if self.method == 'lawnmower':
                lwn_d = np.sqrt(va_x ** 2 + va_y ** 2)
                va_x /= lwn_d
                va_y /= lwn_d

            if self.motion_model == 'dubins' or self.motion_model == 'cadubins':
                vo_x = self.VXA[ia][-1]
                vo_y = self.VYA[ia][-1]
                dang = self.agents[ia]['va'] / self.agents[ia]['rm'] * self.dt

                if (va_x * vo_x + va_y * vo_y) < np.cos(dang):

                    v1 = np.array([vo_x - vo_y * np.sin(dang), vo_y + vo_x * np.sin(dang)])
                    v2 = np.array([vo_x + vo_y * np.sin(dang), vo_y - vo_x * np.sin(dang)])

                    v1 /= np.linalg.norm(v1)
                    v2 /= np.linalg.norm(v2)

                    if (va_x * v1[0] + va_y * v1[1]) > (va_x * v2[0] + va_y * v2[1]):
                        va_x, va_y = v1
                    else:
                        va_x, va_y = v2

            self.VXA[ia].append(va_x)
            self.VYA[ia].append(va_y)

            if self.method == 'lawnmower':
                # print('lwn: %02d %10.3f %10.3f' % (self.lwn_i, lwn_d, np.sqrt((self.dt * self.va * va_x)**2 + (self.dt * self.va * va_y)**2)))
                if np.sqrt((self.dt * self.agents[ia]['va'] * va_x) ** 2 + (
                        self.dt * self.agents[ia]['va'] * va_y) ** 2) >= lwn_d - 1e-10:
                    self.LWNI[ia] += 1
                    if self.LWNI[ia] >= len(self.LWNX[ia]):
                        self.LWNI[ia] = 1
                        xa.append(xa[0])
                        ya.append(ya[0])
                        self.lawnmower_finished += 1

            pos_eps = 0.0  # 1e-30
            last = len(xa)
            for fdt in self.FDT:  # fraction od dt
                _xa = xa[last - 1] + fdt * self.dt * self.agents[ia]['va'] * va_x
                _ya = ya[last - 1] + fdt * self.dt * self.agents[ia]['va'] * va_y
                if _xa < self.X[0]: _xa = self.X[0] + pos_eps
                if _xa > self.X[-1]: _xa = self.X[-1] - pos_eps
                if _ya < self.Y[0]: _ya = self.Y[0] + pos_eps
                if _ya > self.Y[-1]: _ya = self.Y[-1] - pos_eps

                xa.append(_xa)
                ya.append(_ya)

        d_min = 1e9
        for ia1, (xa1, ya1) in enumerate(zip(self.XA, self.YA)):
            for ia2, (xa2, ya2) in enumerate(zip(self.XA, self.YA)):
                if ia1 == ia2:
                    continue
                x_1 = xa1[-1]
                y_1 = ya1[-1]
                x_2 = xa2[-1]
                y_2 = ya2[-1]

                d = np.sqrt((x_1 - x_2) ** 2 + (y_1 - y_2) ** 2)
                if d < d_min:
                    d_min = d

        # print('d_min:', d_min)
        # if d_min < self.min_distance:
        #    pass
        #    #input(' >> press return to continue')

    def output(self):

        fajl = open(self.results_dir + '/agents.txt', 'a')
        fajl.write('%.5f\n' % self.t)
        for xa, ya in zip(self.XA, self.YA):
            linex = ' '.join(['%.5f' % xx for xx in xa])
            liney = ' '.join(['%.5f' % yy for yy in ya])
            fajl.write(linex + '\n')
            fajl.write(liney + '\n')
        fajl.close()

    def smc_step(self):

        if np.sum(self.c) > 0.0:
            # self.cs = c_cum / np.mean(c_cum)
            self.cs = self.c / np.sum(self.c)
        else:
            pass
            self.cs = np.zeros([self.ny, self.nx])
        # self.c = filters.gaussian_filter(self.c, self.sigma_c/self.dx, truncate=50)

        # print('SMC:', np.sum(self.cs), np.sum(self.ms))

        self.ck = DCT(self.cs)

        """
        SMC
        """

        ##Las[iK1][iK2]=1./pow(1.+iK1*iK1+iK2*iK2,1.5) * (_fft_ck1[iK2 * _fft_n + iK1] - _fft_muk1[iK2 * _fft_n + iK1]);
        ky, kx = np.meshgrid(np.arange(self.ny), np.arange(self.nx))

        self.Las = 1.0 / (1 + kx ** 2 + ky ** 2) ** 1.5 * (self.ck - self.mk).T

        self.u = -IDCT(self.Las).T
        self.uy, self.ux = np.gradient(self.u)

        vax = intrp.interp2d(self.X, self.Y, self.ux, kind='linear')
        vay = intrp.interp2d(self.X, self.Y, self.uy, kind='linear')

        VAX, VAY = [], []
        for ia, (xa, ya) in enumerate(zip(self.XA, self.YA)):
            va_x = vax(xa[-1], ya[-1])[0]
            va_y = vay(xa[-1], ya[-1])[0]
            van = np.sqrt(va_x ** 2 + va_y ** 2)
            if van < 1e-20:
                va_x = xa[-1] - xa[-2]
                va_y = ya[-1] - ya[-2]
                van = np.sqrt(va_x ** 2 + va_y ** 2)
                print('### zero gradient')
            va_x /= van
            va_y /= van
            VAX.append(va_x)
            VAY.append(va_y)

        return VAX, VAY

    def lawnmower_step(self):

        VAX, VAY = [], []
        for ia, (xa, ya, vxa, vya) in enumerate(zip(self.XA, self.YA, self.VXA, self.VYA)):

            va_x = self.LWNX[ia][self.LWNI[ia]] - xa[-1]
            va_y = self.LWNY[ia][self.LWNI[ia]] - ya[-1]

            van = np.sqrt(va_x ** 2 + va_y ** 2)
            if van < 1e-20:
                va_x = vxa[-1]
                va_y = vya[-1]
                van = np.sqrt(va_x ** 2 + va_y ** 2)
                print('### zero gradient')

            VAX.append(va_x)
            VAY.append(va_y)

        return VAX, VAY

    def plot_solution(self):

        formatter = tkr.ScalarFormatter(useMathText=True)
        formatter.set_scientific(True)
        formatter.set_powerlimits((-2, 2))
        formatter.set_useOffset(False)

        plt.figure(figsize=(self.plot_options['plot_width'], self.plot_options['plot_height']))

        gs1 = gridspec.GridSpec(3, 4)
        gs1.update(left=self.plot_options['left'],
                   right=self.plot_options['right'],
                   wspace=self.plot_options['wspace'],
                   hspace=self.plot_options['hspace'],
                   top=self.plot_options['top'],
                   bottom=self.plot_options['bottom'])
        ax1 = plt.subplot(gs1[0:3, 0:3])
        ax2 = plt.subplot(gs1[0, 3])
        ax3 = plt.subplot(gs1[1, 3])
        ax4 = plt.subplot(gs1[2, 3])

        if self.sit >= 0:
            plt.title('t=%.4f' % self.T[self.it])
        elif self.sit == -1:
            plt.title('t=%.4f' % self.T[0])
        elif self.sit == -2:
            plt.title('t=%.4f' % self.T[-1])

        cmap = plt.cm.YlGnBu
        # cmap = plt.cm.viridis

        ax2.set_title('Spraying density')
        cntr1 = ax1.contourf(self.X, self.Y, self.sc, np.linspace(0.0, self.plot_options['c_levels'][-1], 201),
                             cmap=cmap, extend='max')
        # cntr1 = ax1.contourf(self.X, self.Y, self.sc-self.m, np.linspace(self.plot_options['c_levels'][0], self.plot_options['c_levels'][-1], 201), cmap = plt.cm.RdBu)#, norm=MidpointNormalize(midpoint=0.))
        # cbar = plt.colorbar(cntr3, ax=ax1, shrink=0.68, pad = 0.01, aspect = 80, fraction=0.02, format=formatter, ticks=self.plot_options['c_levels'])
        cbar = plt.colorbar(cntr1, ax=ax1, shrink=0.38, pad=self.plot_options['cbar_pad'], aspect=80, fraction=0.01,
                            format=formatter, ticks=self.plot_options['c_levels'])

        cbar.formatter.set_powerlimits((0, 0))
        cbar.update_ticks()
        cbar.ax.get_yaxis().get_offset_text().set_visible(False)

        exp = np.floor(np.log10(self.plot_options['c_levels'][-1]))
        cbar.set_label('$c$ [lm$^{-2}s^{-1}$] $\cdot 10^{%d}$' % exp)

        for xa, ya in zip(self.XA, self.YA):
            ax1.plot(xa, ya, color='black', lw=1, alpha=0.5)
        for xa, ya in zip(self.XA, self.YA):
            ax1.plot(xa[-1], ya[-1], 'o', color='black', ms=8)
        ax1.set_xlim(self.X[0], self.X[-1])
        ax1.set_ylim(self.Y[0], self.Y[-1])
        ax1.axis('image')

        ax2.set_title('Goal density')
        cntr2 = ax2.contourf(self.X, self.Y, self.m, np.linspace(0, self.plot_options['c_levels'][-1], 201), cmap=cmap)
        cbar = plt.colorbar(cntr2, ax=ax2, shrink=0.78, pad=0.02, aspect=50, fraction=0.02,
                            ticks=self.plot_options['c_levels'])
        cbar.formatter.set_powerlimits((0, 0))
        cbar.update_ticks()
        cbar.ax.get_yaxis().get_offset_text().set_visible(False)
        exp = np.floor(np.log10(self.plot_options['c_levels'][-1]))
        cbar.set_label('$m$ [lm$^{-2}s^{-1}$] $\cdot 10^{%d}$' % exp)
        ax2.axis('image')

        ax3.set_title('Spraying density difference, $c-m$ [lm$^{-2}s^{-1}$] $\cdot 10^{%d}$' % exp)

        cntr3 = ax3.contourf(self.X, self.Y, self.sc - self.m, self.dc_levels,
                             vmin=-self.plot_options['c_levels'][-1], vmax=self.plot_options['c_levels'][-1],
                             cmap=plt.cm.RdBu, extend='both',
                             norm=colors.SymLogNorm(linthresh=self.plot_options['dc_orders'][-1],
                                                    linscale=1.0,
                                                    vmin=-self.plot_options['c_levels'][-1],
                                                    vmax=self.plot_options['c_levels'][-1]))
        # cntr3 = ax3.contourf(self.X, self.Y, self.sc, np.linspace(0, self.plot_options['c_levels'][-1], 201), cmap = plt.cm.viridis)

        dc_ticks = []
        for o in self.plot_options['dc_orders']:
            dc_ticks.append(-o)
        for o in self.plot_options['dc_orders'][::-1]:
            dc_ticks.append(o)
        # print(dc_ticks)
        cbar = plt.colorbar(cntr3, ax=ax3, shrink=0.78, pad=0.02, aspect=50, fraction=0.02, ticks=dc_ticks)

        ax3.axis('image')

        ax4.set_title('Coverage evaluation')
        if self.it > 0:
            l0 = ax4.plot(self.T[0:self.it + 1], np.array(self.E)[:, 0], c='black', ls='-', lw=2,
                          label='Total error, $E$')
            ax4.fill_between(self.T[0:self.it + 1], np.array(self.E)[:, 1], color='red', alpha=0.2)
            ax4.fill_between(self.T[0:self.it + 1], np.array(self.E)[:, 1], np.array(self.E)[:, 0], color='blue',
                             alpha=0.2)
        else:
            l0 = ax4.plot([], [], c='black', ls='-', lw=2, label='Total error, $E$')
            ax4.fill_between([], [], color='red', alpha=0.2)
            ax4.fill_between([], [], [], color='blue', alpha=0.2)

        # ax6.set_xlim(self.T[0], self.T[-1])
        ax4.set_xlim(self.T[0], self.T[-1])
        ax4.set_ylim(-0.05, 1.05)
        # ax4.set_ylim(self.plot_options['E_min'], 1.0)
        # ax6.set_ylabel(r'$E$')
        ax4.set_xlabel(r'$t$ [s]')
        # ax4.set_ylim(0, self.plot_options['c_levels'][-1])
        # ax4.set_yscale('log')
        # ax6.set_xscale('log')
        ax4.grid(which='minor')
        ax4.grid(which='major')

        # ax4.plot(self.T[0:self.it+1], np.array(self.D)/100.0 , c='red', ls=':', lw=2, label='Proportion of undetected targets, $1-D$')
        ax4.legend(loc='upper right')

        f1 = ax4.fill(np.NaN, np.NaN, 'red', alpha=0.2)
        f2 = ax4.fill(np.NaN, np.NaN, 'blue', alpha=0.2)
        # ax4.legend([l0[0], (l1[0], f1[0]), (l2[0], f2[0])], ['Total error, $E$', 'Subsprayed error, $E_1$', 'Oversprayed error, $E_2$'], loc='upper right')
        ax4.legend([l0[0], f1[0], f2[0]], ['Total error, $E$', 'Undersprayed error, $E^-$', 'Oversprayed error, $E^+$'],
                   loc='upper right')

        if self.sit >= 0:
            plt.savefig(self.results_dir + '/it_%06d.png' % self.sit)
        elif self.sit == -1:
            plt.savefig(self.results_dir + '/state_initial.png')
        elif self.sit == -2:
            plt.savefig(self.results_dir + '/state_final.png')

        plt.close()

        if self.sit % 100 == 0:
            # ffmpeg -r 15 -i iter_%05d.png -c:v libx264 -r 15 -pix_fmt yuv420p pipenetwork_optimization.mp4
            try:
                # print os.getcwd() + '/' + self.results_dir
                p = subprocess.Popen(
                    'ffmpeg -y -r 25 -i it_%06d.png -c:v libx264 -r 25 -pix_fmt yuv420p search.mp4'.split(),
                    cwd=os.getcwd() + '/' + self.results_dir,
                    stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                output, error = p.communicate()
                # p.wait()
                print(output)
                print(error)
            except:
                pass

    def plot_case_summary(self):

        print('Making case summary plot ...')

        xr, yr, v, axlbl_offset, x_arr, y_arr, left, right, bottom, top, h1, h2 = self.plot_options[
            'spray_pattern_plot']
        X = np.linspace(-xr, xr, 200)
        Y = np.linspace(-yr, yr, 200)
        Xm, Ym = np.meshgrid(X, Y)

        C = self.spraying_pattern(Xm, Ym)
        Cx = np.zeros_like(X)
        for ix, x in enumerate(X):
            Cx[ix] = np.sum(C[:, ix])
        Cx *= (Y[1] - Y[0]) / v

        self.spray_threshold = Cx.max() * self.phi_threshold_ratio

        subtitle_offset = 0.05
        cmap = plt.cm.gray_r
        fig = plt.figure(figsize=(10, (10 / xr * yr + 3) * 0.52))
        gs = gridspec.GridSpec(2, 2,
                               height_ratios=(10 / xr * yr, 3),
                               width_ratios=(1, 1),
                               hspace=0.4, wspace=0.32,
                               bottom=bottom, top=top,
                               left=left, right=right)
        ax1 = plt.subplot(gs[0, 1])

        cf = ax1.contourf(Xm, Ym, C, 100, cmap=cmap)
        ax1.contour(Xm, Ym, C, 8, colors='k', linewidths=0.1)
        # cbaxes = fig.add_axes([0.6, 0.6, 0.2, 0.02])
        cbar = plt.colorbar(cf, ax=ax1, shrink=0.78, pad=0.01, aspect=50, fraction=0.02,
                            ticks=self.plot_options['phi_levels'])
        # cbar = plt.colorbar(cf, ax=ax1, orientation="horizontal")

        cbar.formatter.set_powerlimits((0, 0))
        cbar.update_ticks()
        cbar.ax.get_yaxis().get_offset_text().set_visible(False)

        exp = np.floor(np.log10(self.plot_options['phi_levels'][-1]))
        cbar.set_label('$\phi$ [lm$^{-2}s^{-1}$] $\cdot 10^{%d}$' % exp)

        ax1.axis('image')
        ax1.grid(ls=':', zorder=-10)
        ax1.arrow(0, 0, 0, y_arr, fc='black', ec='black',
                  head_width=0.2, head_length=0.3, zorder=10)
        ax1.text(axlbl_offset, y_arr, '$\zeta$', fontsize=12, zorder=10)
        ax1.arrow(0, 0, x_arr, 0, fc='black', ec='black',
                  head_width=0.2, head_length=0.3, zorder=10)
        ax1.text(x_arr, axlbl_offset, '$\chi$', fontsize=12, zorder=10)
        # ax1.tick_params(direction='in', pad=-25)
        ax1.text(0.5 + subtitle_offset, h1, '(B) Spraying pattern', fontsize=12, ha='left', transform=fig.transFigure)

        ax2 = plt.subplot(gs[1, 1], sharex=ax1)
        # plt.colorbar(cf, ax=ax1)

        ax2.plot(X, Cx, c='k', lw=0.5)
        ax2.fill_between(X, Cx, color='gray', alpha=0.5)
        # ax2.tick_params(direction='in', pad=-25)
        ax2.set_ylim(0, None)
        # ax2.get_yaxis().get_major_formatter().set_scientific(True)
        ax2.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
        ax2.get_yaxis().get_offset_text().set_visible(False)
        exp = np.floor(np.log10(ax2.get_ylim()[1]))

        ax2.text(0.5 + subtitle_offset, h2, '(C) Perpendicular spraying density profile', fontsize=12, ha='left',
                 transform=fig.transFigure)
        ax2.set_ylabel('Spraying density\n[lm$^{-2}$] $\cdot 10^{%d}$' % exp)
        ax2.grid(ls=':')

        ax3 = plt.subplot(gs[:, 0])
        # ax3.contour(self.X, self.Y, self.m, 5, colors='k', linewidths=0.1)
        cntr2 = ax3.contourf(self.X, self.Y, self.m, np.linspace(0, self.plot_options['c_levels'][-1], 201),
                             cmap=plt.cm.gray_r,
                             norm=mpl.colors.Normalize(vmin=0., vmax=self.plot_options['c_levels'][-1] * 2))
        cbar = plt.colorbar(cntr2, ax=ax3, shrink=0.78, pad=-0.05, aspect=80, fraction=0.02,
                            ticks=self.plot_options['c_levels'])

        cbar.formatter.set_powerlimits((0, 0))
        cbar.update_ticks()
        cbar.ax.get_yaxis().get_offset_text().set_visible(False)

        exp = np.floor(np.log10(self.plot_options['c_levels'][-1]))
        cbar.set_label('$\phi$ [lm$^{-2}s^{-1}$] $\cdot 10^{%d}$' % exp)

        cbar.set_label('$m$ [lm$^{-2}$] $\cdot 10^{%d}$' % exp)

        ax3.axis('image')
        ax3.text(subtitle_offset, h1, '(A) Goal spraying density', fontsize=12, ha='left', transform=fig.transFigure)

        plt.savefig(self.results_dir + '/%s_definition.pdf' % self.case_name)
        plt.close()

    def save_state(self):

        np.savez(self.results_dir + '/state_%f.npz' % self.t,
                 X=self.X,
                 Y=self.Y,
                 t=self.T[self.it],
                 m=self.m,
                 XA=self.XA,
                 YA=self.YA,
                 c=self.c,
                 sc=self.sc,
                 s=self.s,
                 u=self.u)
        """
        if self.method == 'hedac':
            np.savez(self.results_dir + '/state_%f.npz' % self.t, 
                     X = self.X,
                     Y = self.Y,
                     t = self.T[self.it],
                     m = self.m,
                     XA = self.XA,
                     YA = self.YA,
                     c = self.c,
                     ss = self.sc,
                     s = self.s,
                     u = self.u)        

        elif self.method == 'smc':
            np.savez(self.results_dir + '/state_%f.npz' % self.t, 
                     X = self.X,
                     Y = self.Y,
                     t = self.T[self.it],
                     m = self.m,
                     #ms = self.ms,
                     c = self.c,
                     #cs = self.cs,
                     mk = self.mk,
                     ck = self.ck,
                     u = -IDCT(self.Las).T)  
        """

    def save_positions(self):
        posfile = open(self.results_dir + '/positions.txt', 'a')

        cols = [self.T[self.it]]
        for xa, ya in zip(self.XA, self.YA):
            cols.append(xa[-1])
            cols.append(ya[-1])

        posfile.write(','.join([str(c) for c in cols]) + '\n')

    def circles_intersections(self, x1, y1, r1, x2, y2, r2):
        print('circles_intersections: ', x1, y1, r1, x2, y2, r2)

        Dx = x2 - x1
        Dy = y2 - y1
        D = np.sqrt(Dx ** 2 + Dy ** 2)

        # Distance between circle centres
        if D > r1 + r2:
            print("The circles do not intersect")
            return None
        elif D < np.abs(r2 - r1):
            print("No Intersect - One circle is contained within the other")
            return None
        elif D == 0 and r1 == r2:
            print("No Intersect - The circles are equal and coincident")
            return None
        else:
            if D == r1 + r2 or D == r1 - r2:
                print("The circles intersect at a single point")
            else:
                print("The circles intersect at two points")

            chorddistance = (r1 ** 2 - r2 ** 2 + D ** 2) / (2 * D)
            # distance from 1st circle's centre to the chord between intersects
            halfchordlength = np.sqrt(r1 ** 2 - chorddistance ** 2)
            chordmidpointx = x1 + (chorddistance * Dx) / D
            chordmidpointy = y1 + (chorddistance * Dy) / D
            I1 = (chordmidpointx + (halfchordlength * Dy) / D,
                  chordmidpointy - (halfchordlength * Dx) / D)
            theta1 = np.rad2deg(np.arctan2(I1[1] - y1, I1[0] - x1))

            I2 = (chordmidpointx - (halfchordlength * Dy) / D,
                  chordmidpointy + (halfchordlength * Dx) / D)
            theta2 = np.rad2deg(np.arctan2(I2[1] - y1, I2[0] - x1))

            if theta2 > theta1:
                I1, I2 = I2, I1

            return I1, I2
