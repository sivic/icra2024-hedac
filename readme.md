# HEDAC ergodic control

Repository for the ICRA 2024 Tutorial on ergodic control (https://ergodiccontrol.github.io/)



Dependencies:
`numpy matplotlib scipy seaborn shapely descartes netgen ngsolve vtk pyvista meshio`

Use `conda install numpy matplotlib scipy seaborn shapely descartes netgen ngsolve vtk pyvista meshio`

or `mamba install numpy matplotlib scipy seaborn shapely descartes netgen ngsolve vtk pyvista meshio`

Note: `netgn`, `ngsolve` and `descartes` are a bit tricky to install via conda/mamba due version compatibility issues, thus try `pip install`. 


The codes are based on following HEDAC papers:
- `hedac_basic`: Ivić, Stefan, Bojan Crnković, and Igor Mezić. "Ergodicity-based cooperative multiagent area coverage via a potential field." IEEE transactions on cybernetics 47.8 (2016): 1983-1993.
- `hedac_spraying`: Ivić, Stefan, Aleksandr Andrejčuk, and Siniša Družeta. "Autonomous control for multi-agent non-uniform spraying." Applied Soft Computing 80 (2019): 742-760.
- `hedac_fem`: Ivić, Stefan, Ante Sikirica, and Bojan Crnković. "Constrained multi-agent ergodic area surveying control based on finite element approximation of the potential field." Engineering applications of artificial intelligence 116 (2022): 105441.
- `hedac_fem3d`: Ivić, Stefan, et al. "Multi-UAV trajectory planning for 3D visual inspection of complex structures." Automation in Construction 147 (2023): 104709.