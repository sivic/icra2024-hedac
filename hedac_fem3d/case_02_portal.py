from hedacfem3d import HedacFEM3D
import numpy as np
import matplotlib.pyplot as plt

test = HedacFEM3D('case_02_portal', delete_dir=False)
test.gmsh_filename = 'meshes/portal_domain.msh'

stdev = 2.0
dist = 5.0
def m0(x, y, z, ds, db):
    _m0 = np.exp(-0.5 * (ds - dist) ** 2 / stdev ** 2) / (stdev * np.sqrt(2 * np.pi))
    if db < 2:
        _m0 *= db/2
    return _m0

def phi(ia, r):
    # return 0.1 * np.exp(-(r ** 2) / (2 * 5 ** 2))
    sigma = 5
    return 200 / (sigma**3 * (2 * np.pi)**1.5) * np.exp(-(r ** 2) / (2 * sigma ** 2))

test.m0_fun = m0  # m0 is calculated from structure distance
na = 3  # Number of agents
test.AX0 = np.linspace(30, 60, na)  # m
test.AY0 = np.full(na, 40)  # m
test.AZ0 = np.full(na, 0.1)  # m
test.AV = np.full(na, 0.5)  # m/s
test.phi_fun = phi
test.AS = np.full(na, 20)  # m
test.FOV_H = 8 # m (height/distance of Field of View)
test.FOV_R = 5 # m (radius of Field of View)

test.alpha = 12.0  # HEDAC's conduction parameter (greater alpha => global/broader search)
test.beta = 1

test.ca_safety_distance = 2.0 # m, collision avoidance safety distance

test.T = np.linspace(0, 1500, 1501)  # Time discretization
test.animation_frame_steps = 1
test.state_save_steps = 100
test.fps = 30 # frames per second, animation video speed

test.plot_options['trajectory_tube_radius'] = 0.15
test.plot_options['window_size'] = [2500, 2600]
test.plot_options['focal_point'] = [45, 25, 38]
test.plot_options['camera_position'] = [155, 155, 100]
test.plot_options['camera_zoom'] = test.plot_options['camera_arrow_length'] = 5
test.plot_options['arrow_thickness'] = 0.01
test.plot_options['d_bar'] = 5.0
test.plot_options['sc_max'] = 20.0

# test.debug_plots = True

test.initialize(skip_a_inv_calculation=False)
if not test.load_state(test.T.size - 1):
    test.solve_trajecotries()
# test.solve_trajecotries()

# test.plot_3d(filename='portal_plot_test')
# test.plot_3d(plot_field='m0', filename=f'case_2_portal_mu0')
# test.plot_3d(filename=f'case_2_portal_inspection')
# test.plot_convergence(filename=f'case_2_portal_convergence.pdf')
# test.calculate_surface_coverage()