# -*- coding: utf-8 -*-
"""
Created on Sat Feb 14 17:25:16 2015

@author: stefan
"""

import logging
import itertools
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import os
import shutil
from matplotlib.colors import Normalize
import matplotlib.patheffects as pe
import matplotlib as mpl
import subprocess
import matplotlib.tri as tri
import time
import seaborn
from numpy.linalg import cond
from scipy.optimize import minimize

import netgen.read_gmsh as read_gsmh
import vtk
import colorcet as cc
import pyvista as pv
from scipy.optimize import linprog

import pickle

#distance point to triangle
from statsmodels.base.data import _nan_rows

from pointTriangleDistance import pointTriangleDistance
from scipy.spatial import cKDTree

# import shapely.geometry as shg
# import shapely.ops as sho
# from descartes import PolygonPatch

import ngsolve as ngs

ngs.ngsglobals.msg_level = 1


logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger("rothemain.rothe_utils")
logging.getLogger('UFL').setLevel(logging.WARNING)
logging.getLogger('FFC').setLevel(logging.WARNING)


class HedacFEM3D():

    def __init__(self, case_name, delete_dir=False):

        self.tic()

        self.case_name = case_name
        self.title('Initializing HEDAC-FEM3D instance')

        self.log('CWD: {}'.format(os.getcwd()))
        self.log('Script: {}'.format(__file__))
        self.log('Results dir: {}'.format(os.path.abspath(case_name)))
        if delete_dir:
            if os.path.exists(case_name):
                shutil.rmtree(case_name)
        if not os.path.exists(case_name):
            os.mkdir(case_name)
        if not os.path.exists(f'{case_name}/frames'):
            os.mkdir(f'{case_name}/frames')
        if not os.path.exists(f'{case_name}/debug'):
            os.mkdir(f'{case_name}/debug')
        if not os.path.exists(f'{case_name}/states'):
            os.mkdir(f'{case_name}/states')
        if not os.path.exists(f'{case_name}/surface_coverage'):
            os.mkdir(f'{case_name}/surface_coverage')
        self.results_dir = os.path.abspath(case_name) + '/'

        self.gmsh_filename = None
        self.mesh = None
        self.pv_structure_mesh = None

        self.d_s = None
        self.d_b = None
        self.gd_s = None
        self.gd_b = None
        self.m0 = None
        self.m = None
        self.u = None
        self.c = None
        self._dc = None
        self.sc = None

        # """Target is a sepearate mesh file in the same coordinate system which is used to define the area of interest."""
        # self.targetmsh_filename = None
        # """Distance is a npz file which contains dist2boundary, dist2target """
        # self.distance_filnename = None
        self.ca_safety_distance = None
        self.inspection_distance = 1.0

        self.AXYZ = None
        self.AX0 = None
        self.AY0 = None
        self.AD0 = None
        self.AV = None
        self.AS = None
        self.FOV_R = None
        self.FOV_H = None
        
        #this is used for inertial effects
        self.vxyz= None


        self.T = None
        self.it = 0
        self.m0_fun = None
        self.phi_fun = None

        self.plot_options = {}
        self.plot_options['sc_min'] = 1

        self.state_save_steps = 100  # After how many steps state is saved
        self.animation_frame_steps = 1  # After how many steps animation frame is made
        self.fps = 30
        self.frames = None
        self.plotter = None
        self.debug_plots = False
        self.execution_time = None

        self.toc()

    def title(self, title):
        """Prints a formatted title to the console."""

        print()
        print()
        print('═' * 100)
        print('  ' + title.ljust(96) + '  ')
        print('─' * 100)

    def log(self, msg):
        """Prints a formatted log message to the console."""

        print('    ' + msg)

    def tic(self):
        """Starts a timer."""

        self._time1 = time.time()

    def toc(self, msg=''):
        """Measures the elapsed time since the timer was started (using tic method).
        Prints the formatted message containing elapsed time."""

        s = time.time() - self._time1
        if s < 1:
            self.log('{}Elapsed time: {:.3f} miliseconds.'.format(
                '' if msg == '' else msg + '. ', s * 1e3))
        else:
            self.log('{}Elapsed time: {:.3f} seconds.'.format(
                '' if msg == '' else msg + '. ', s))
        return s

    def initialize(self, skip_a_inv_calculation=False):
        """Initialization of the solver. Should be called before running the simulation."""

        self.init_domain()
        self.init_agents()
        self.init_graphics()

        self.init_fem(skip_a_inv_calculation=skip_a_inv_calculation)

        self.save_state()
        self.plot_3d(plot_field='d_s', filename='d_s')
        self.plot_3d(plot_field='d_b', filename='d_b')
        self.plot_3d(plot_field='m0', filename='m0')
        # self.plot_3d(plot_field='u', filename=f'u_{self.it:06d}')
        # self.plot_3d(plot_field='m', filename=f'm_{self.it:06d}')
        # self.plot_3d(plot_field='c', filename=f'c_{self.it:06d}')
        # self.plot_3d(plot_field='dc', filename=f'dc_{self.it:06d}')
        self.plot_3d() # plot initial state

    def init_domain(self):
        """Domain, time, FEM mesh and Shapely geometric objects initialization for the domain nad obstacles."""

        self.tic()
        self.title('Initializing domain')

        # Time discretization
        self.tn = np.size(self.T)
        self.dt = self.T[1] - self.T[0]
        self.log('Time: {} to {}'.format(self.T[0], self.T[-1]))
        self.log('Time step: {}'.format(self.dt))
        self.eta_V = np.full(np.size(self.T), np.nan)
        self.eta_V[0] = 0.0
        self.eta_A = np.full(np.size(self.T), np.nan)
        self.eta_A[0] = 0.0
        self.execution_time = np.zeros([3, np.size(self.T)]) * np.nan

        # Nodes and elements from the mesh
        self.log(f'Reading mesh from {self.gmsh_filename}')
        self.mesh = read_gsmh.ReadGmsh(self.gmsh_filename)
        self.nodes = self.mesh.Points()  # Nodes
        self.nn = len(self.nodes)  # Number of nodes
        self.elements = self.mesh.Elements3D()  # Elements
        self.ne = len(self.elements)  # Number of elements
        self.Xn = np.zeros(self.nn)  # Nodes x coordinates
        self.Yn = np.zeros(self.nn)  # Nodes y coordinates
        self.Zn = np.zeros(self.nn)  # Nodes y coordinates
        for i, node in enumerate(self.nodes):
            self.Xn[i], self.Yn[i], self.Zn[i] = node

        # self.EL = np.zeros([self.ne, 4], dtype=np.int)  # Elements topology (connections)
        # for i, e in enumerate(self.elements):
        #     self.EL[i, :] = [v.nr - 1 for v in e.vertices]

        # Initializing ngsolve mesh
        self.fe_mesh = ngs.Mesh(self.mesh)

        # Domain bounds:
        x_min, x_max, y_min, y_max, z_min, z_max = \
            self.Xn.min(), self.Xn.max(), self.Yn.min(
            ), self.Yn.max(), self.Zn.min(), self.Zn.max()
        self.log(f'Number of mesh elements: {self.ne}')
        self.log(f'Number of mesh nodes: {self.nn}')
        self.log(
            f'Bounding box: [{x_min}, {x_max}] x [{y_min}, {y_max}] x [{z_min}, {z_max}]')
        self.plot_bbox = x_min, x_max, y_min, y_max

        self.toc()

    def init_agents(self):
        """Initializes agents positions and parameters."""

        self.tic()
        self.title('Initializing agents')

        assert np.size(self.AX0) == np.size(
            self.AY0), f'The size of agent parameters is not matching ({np.size(self.AX0)} vs {np.size(self.AY0)})'
        assert np.size(self.AX0) == np.size(
            self.AZ0), f'The size of agent parameters is not matching ({np.size(self.AX0)} vs {np.size(self.AZ0)})'
        assert np.size(self.AX0) == np.size(
            self.AV), f'The size of agent parameters is not matching ({np.size(self.AX0)} vs {np.size(self.AV)})'
        assert np.size(self.AX0) == np.size(
            self.AS), f'The size of agent parameters is not matching ({np.size(self.AX0)} vs {np.size(self.AS)})'

        self.an = np.size(self.AX0)

        self.AXYZ = np.full([self.an, self.tn, 3], np.nan)
        self.AXYZ[:, 0, 0] = self.AX0
        self.AXYZ[:, 0, 1] = self.AY0
        self.AXYZ[:, 0, 2] = self.AZ0
        
        #inertial effects
        self.vxyz=np.zeros([self.an, 3])

        # Monitoring execution time
        # self.execution_time = np.full([3, self.tn], 0.0)

        # Displaying the summary of agents' parameters in the log
        self.log(f'Number of agents: {self.an}')
        self.log(f'AX0: {self.AX0}')
        self.log(f'AY0: {self.AY0}')
        self.log(f'AZ0: {self.AZ0}')

        self.toc()

    def init_fem(self, skip_a_inv_calculation=False):
        """Initializing FEM for ngsolve."""

        self.tic()
        self.title('Initializing FEM')

        # H1-conforming finite element space
        # https://ngsolve.org/docu/nightly/i-tutorials/unit-2.3-hcurlhdiv/hcurlhdiv.html
        # self.fes = ngs.H1(self.fe_mesh, order=3, neuman=[1,2,3,4])
        self._fes = ngs.H1(self.fe_mesh, order=1)
        self.fes = ngs.H1(self.fe_mesh, order=2)
        # self.fes = ngs.H1(self.fe_mesh, order=3)
        # self.fes = ngs.HCurl(self.fe_mesh, order=2)
        # self.fes = ngs.HDiv(self.fe_mesh, order=2)

        # Define trial- and test-functions
        u = self.fes.TrialFunction()
        self.v = self.fes.TestFunction()

        # The bilinear-form
        self.a = ngs.BilinearForm(self.fes)
        self.a += ngs.SymbolicBFI(self.alpha * ngs.grad(u) * ngs.grad(self.v) + self.beta * u * self.v)
        
        self.m0 = ngs.GridFunction(self.fes)
        
        self.toc()

        # self.init_distance_fields()
        self.init_distance_fields_full_version()


        I_m0 = ngs.Integrate(self.m0, self.fe_mesh)  # Normalize
        for i, (x, y, z) in enumerate(zip(self.Xn, self.Yn, self.Zn)):
            self.m0.vec[i] /= I_m0

        self.c = ngs.GridFunction(self._fes)
        self.c.Set(0 * ngs.x)

        self._dc_nodal = np.zeros([self.nn])

        self.m = ngs.GridFunction(self.fes)
        self.m.Set(self.m0)
        self.accumulate_coverage()

        # The right hand side
        # self.f = ngs.LinearForm(self.fes)
        self.ff = ngs.GridFunction(self.fes)

        # Assemble
        self.a.Assemble()
        # f.Assemble()

        if skip_a_inv_calculation:
            self.log('Skipping calculation of A_inv')
        else:
            self.log(f'Calculating inverse of stiffness matrix...')
            self.a_inv = self.a.mat.Inverse(self.fes.FreeDofs(), inverse="sparsecholesky")
            self.log('Done!')

        # input(' >> Press return to continue.')

        self.u = np.zeros(self.nn)

        self.gfu = ngs.GridFunction(self.fes)

        self.f = ngs.LinearForm(self.fes)
        self.f += ngs.SymbolicLFI(self.m / np.average(self.m.vec[:self.nn]) * self.v)
        self.f.Assemble()
        # Compute the solution (using precalculated inverse of system matrix A)

        if not skip_a_inv_calculation:
            self.gfu.vec.data = self.a_inv * self.f.vec

        self.toc()
        

    def init_distance_fields(self):
        # self.title('Initializing distance fields')
        # self.tic()

        self.log(f'Domain nodes: {self.nn}')
        

        bdomain_nodes = []
        for face in self.mesh.Elements2D():
            bdomain_nodes.extend([v.nr for v in face.vertices])
        bdomain_nodes = np.unique(bdomain_nodes)
        self.log(f'Domain boundary nodes: {bdomain_nodes.size}')

        Xd, Yd, Zd = np.zeros(len(bdomain_nodes)),\
            np.zeros(len(bdomain_nodes)),\
            np.zeros(len(bdomain_nodes))
        nodes = self.mesh.Points()
        for i, j in enumerate(bdomain_nodes):
            Xd[i], Yd[i], Zd[i] = nodes[j]

        d_b = np.zeros([self.nn])
        for i in range(self.nn):
            d_b[i] =np.sqrt(np.min( (self.Xn[i] - Xd)**2 + (self.Yn[i] - Yd)**2 + (self.Zn[i] - Zd)**2))
        self.log(f'Boundary distance matrix shape: {d_b.shape}')
        self.log(f'd_b min: {d_b.min()}, d_b max: {d_b.max()}')

        d_s = None
        structure_mesh_filename = f'{self.gmsh_filename[:-11]}_structure.msh'
        if os.path.exists(structure_mesh_filename):
            self.log(f'Reading structure mesh ({structure_mesh_filename})')
            # if os.path.exists(structure_mesh_filename):
            structure_mesh = read_gsmh.ReadGmsh(structure_mesh_filename)

            structure_nodes = []
            for face in structure_mesh.Elements2D():
                structure_nodes.extend([v.nr for v in face.vertices])
            structure_nodes = np.unique(structure_nodes)
            self.log(f'Structure nodes: {structure_nodes.size}')

            Xs, Ys, Zs = np.zeros(len(structure_nodes)),\
                np.zeros(len(structure_nodes)),\
                np.zeros(len(structure_nodes))
            nodes = structure_mesh.Points()
            for i, j in enumerate(structure_nodes):
                Xs[i], Ys[i], Zs[i] = nodes[j]

            d_s = np.zeros([self.nn])
            for i in range(self.nn):
                d_s[i] =np.sqrt(np.min( (self.Xn[i] - Xs)**2 + (self.Yn[i] - Ys)**2 + (self.Zn[i] - Zs)**2))

            self.log(f'Structure distance matrix shape: {d_s.shape}')
            self.log(f'd_s min: {d_s.min()}, d_s max: {d_s.max()}')

        else:
            self.log(f'There is no structure mesh for this case.')

        _fes = ngs.H1(self.fe_mesh, order=1)
        self.log('Setting m0 field from specified function.')
        _m0 = ngs.GridFunction(_fes)
        if d_s is None:
            for i, (x, y, z, db) in enumerate(zip(self.Xn, self.Yn, self.Zn, d_b)):
                _m0.vec[i] = self.m0_fun(x, y, z, np.nan, db)
            self.m0.Set(_m0)
        else:

            self.log('Filling d_s GridFunction')
            _ds = ngs.GridFunction(_fes)
            for i, (x, y, z) in enumerate(zip(self.Xn, self.Yn, self.Zn)):
                _ds.vec[i] = d_s[i]

            self.d_s = ngs.GridFunction(self.fes)
            self.d_s.Set(_ds)
            self.log(f'd_s min/max: {np.min(self.d_s.vec)}, {np.max(self.d_s.vec)}')
            self.gd_s = ngs.grad(self.d_s)
            self.log('Setting m0 field based on the distance from structure.')
            for i, (x, y, z, ds, db) in enumerate(zip(self.Xn, self.Yn, self.Zn, d_s, d_b)):
                _m0.vec[i] = self.m0_fun(x, y, z, ds, db)
            self.m0.Set(_m0)
            self.log(f'm0 min/max: {np.min(self.m0.vec)}, {np.max(self.m0.vec)}')


        self.log('Filling d_b GridFunction')
        _db = ngs.GridFunction(_fes)
        for i, (x, y, z) in enumerate(zip(self.Xn, self.Yn, self.Zn)):
            #d = np.min(d_b[i, :])
            _db.vec[i] = d_b[i]
        self.d_b = ngs.GridFunction(self.fes)
        self.d_b.Set(_db)
        self.gd_b = ngs.grad(self.d_b)
        
        self.toc()
        
    def init_distance_fields_full_version(self):

        self.log(f'Domain nodes: {self.nn}')
        # make efficient search tree
        
        nodes = self.mesh.Points()
        point_cloud=np.column_stack((self.Xn, self.Yn, self.Zn))
        
        #index of the bdomain node
        bdomain_nodes = []
        #index of the bdomain node
        bdomain_nodes_inverse = {}
        #dict of the b triangles containing the node
        bdomain_Triangles_near_vertex = {}
        #triangles as a list of nodes
        bdomain_Triangles=[]
        for j,face in enumerate(self.mesh.Elements2D()):
            tri=[v.nr for v in face.vertices]
            bdomain_Triangles.append(tri)
            for v in face.vertices:
                if v.nr not in bdomain_nodes:
                    bdomain_nodes.append(v.nr)
                    bdomain_nodes_inverse[v.nr]=len(bdomain_nodes)-1
                    bdomain_Triangles_near_vertex[v.nr]=[j]                                
                else:
                    if j not in bdomain_Triangles_near_vertex[v.nr]:
                        bdomain_Triangles_near_vertex[v.nr].append(j)
                    
        self.log(f'Domain boundary nodes: {len(bdomain_nodes)}')
        d_b = np.zeros([self.nn])         


        #coordiantes of nodes
        Xd, Yd, Zd = np.zeros(len(bdomain_nodes)),\
            np.zeros(len(bdomain_nodes)),\
            np.zeros(len(bdomain_nodes))      
        for i, j in enumerate(bdomain_nodes):
            Xd[i], Yd[i], Zd[i] = nodes[j]
        dbpoints=np.column_stack((Xd, Yd, Zd)) 
   
        #fast point lookup
        dbtree = cKDTree(dbpoints)
        # get indices of closest three points to use as vetice to calculate distance to
        for i,point in enumerate(point_cloud):
            dist,idx_of_point_in_mesh = dbtree.query(point,3)# take 3 closest points
            key_node_index=bdomain_nodes[idx_of_point_in_mesh[0]]
            d=dist[0]#set distance to the closest one
            if d>1.e-10:
                for triangleID in bdomain_Triangles_near_vertex[bdomain_nodes[idx_of_point_in_mesh[0]]]:
                    triangle=bdomain_Triangles[triangleID]
                    if (bdomain_nodes[idx_of_point_in_mesh[1]] in triangle) and (bdomain_nodes[idx_of_point_in_mesh[2]] in triangle): #found one
                        TRI=np.array([dbpoints[idx_of_point_in_mesh[0]],dbpoints[idx_of_point_in_mesh[1]],dbpoints[idx_of_point_in_mesh[2]]])
                        d_calc,p_calc=pointTriangleDistance(TRI, point)
                        d = min(d, d_calc)
                        break
                else:#did not break, check all distances to all neighboring triangles#this is a sharp point case
                    for triangleID in bdomain_Triangles_near_vertex[bdomain_nodes[idx_of_point_in_mesh[0]]]:
                        triangle=bdomain_Triangles[triangleID]
                        TRI=np.array([dbpoints[bdomain_nodes_inverse[triangle[0]]],dbpoints[bdomain_nodes_inverse[triangle[1]]],dbpoints[bdomain_nodes_inverse[triangle[2]]]])
                        d_calc,p_calc=pointTriangleDistance(TRI, point)
                        d = min(d, d_calc)
            d_b[i]=d

        self.log(f'Boundary distance matrix shape: {d_b.shape}')
        self.log(f'd_b min: {d_b.min()}, d_b max: {d_b.max()}')

        structure_mesh_filename = f'{self.gmsh_filename[:-11]}_structure.msh'
        if os.path.exists(structure_mesh_filename):
            self.log(f'Reading structure mesh ({structure_mesh_filename})')
            # if os.path.exists(structure_mesh_filename):
            structure_mesh = read_gsmh.ReadGmsh(structure_mesh_filename)
            nodes = structure_mesh.Points()

            #index of the bdomain node
            sdomain_nodes = []
            #index of the bdomain node
            sdomain_nodes_inverse = {}
            #dict of the b triangles containing the node
            sdomain_Triangles_near_vertex = {}
            #triangles as a list of nodes
            sdomain_Triangles=[]
            for j,face in enumerate(structure_mesh.Elements2D()):
                tri=[v.nr for v in face.vertices]
                sdomain_Triangles.append(tri)
                for v in face.vertices:
                    if v.nr not in sdomain_nodes:
                        sdomain_nodes.append(v.nr)
                        sdomain_nodes_inverse[v.nr]=len(sdomain_nodes)-1
                        sdomain_Triangles_near_vertex[v.nr]=[j]                                
                    else:
                        if j not in sdomain_Triangles_near_vertex[v.nr]:
                            sdomain_Triangles_near_vertex[v.nr].append(j)
                        
            self.log(f'Domain boundary nodes: {len(sdomain_nodes)}')
            d_s = np.zeros([self.nn])         
    
    
            #coordiantes of nodes
            Xs, Ys, Zs = np.zeros(len(sdomain_nodes)),\
                np.zeros(len(sdomain_nodes)),\
                np.zeros(len(sdomain_nodes))      
            for i, j in enumerate(sdomain_nodes):
                Xs[i], Ys[i], Zs[i] = nodes[j]
            spoints=np.column_stack((Xs, Ys, Zs)) 
       
            #fast point lookup
            stree = cKDTree(spoints)
            # get indices of closest three points to use as vetice to calculate distance to
            for i,point in enumerate(point_cloud):
                dist,idx_of_point_in_mesh = stree.query(point,3)# take 3 closest points
                key_node_index=sdomain_nodes[idx_of_point_in_mesh[0]]
                d=dist[0]#set distance to the closest one
                if d>1.e-10:
                    for triangleID in sdomain_Triangles_near_vertex[sdomain_nodes[idx_of_point_in_mesh[0]]]:
                        triangle=sdomain_Triangles[triangleID]
                        if (sdomain_nodes[idx_of_point_in_mesh[1]] in triangle) and (sdomain_nodes[idx_of_point_in_mesh[2]] in triangle): #found one
                            TRI=np.array([spoints[idx_of_point_in_mesh[0]],spoints[idx_of_point_in_mesh[1]],spoints[idx_of_point_in_mesh[2]]])
                            d_calc,p_calc=pointTriangleDistance(TRI, point)
                            d = min(d, d_calc)
                            break
                    else:#did not break, check all distances to all neighboring triangles#this is a sharp point case
                        for triangleID in sdomain_Triangles_near_vertex[sdomain_nodes[idx_of_point_in_mesh[0]]]:
                            triangle=sdomain_Triangles[triangleID]
                            TRI=np.array([spoints[sdomain_nodes_inverse[triangle[0]]],spoints[sdomain_nodes_inverse[triangle[1]]],spoints[sdomain_nodes_inverse[triangle[2]]]])
                            d_calc,p_calc=pointTriangleDistance(TRI, point)
                            d = min(d, d_calc)
                d_s[i]=d

            self.log(f'Structure distance matrix shape: {d_s.shape}')
            self.log(f'd_s min: {d_s.min()}, d_s max: {d_s.max()}')

        else:
            self.log('There is no structure mesh for this case.')
            d_s = None

        self.log('Setting m0 field from specified function.')
        _m0 = ngs.GridFunction(self._fes)
        if d_s is None:
            for i, (x, y, z, db) in enumerate(zip(self.Xn, self.Yn, self.Zn, d_b)):
                _m0.vec[i] = self.m0_fun(x, y, z, np.nan, db)
            self.m0.Set(_m0)
        else:

            self.log('Filling d_s GridFunction')
            _ds = ngs.GridFunction(self._fes)
            for i, (x, y, z) in enumerate(zip(self.Xn, self.Yn, self.Zn)):
                _ds.vec[i] = d_s[i]

            self.d_s = ngs.GridFunction(self.fes)
            self.d_s.Set(_ds)
            print(f'd_s min/max: {np.min(self.d_s.vec)}, {np.max(self.d_s.vec)}')
            self.gd_s = ngs.grad(self.d_s)
            self.log('Setting m0 field based on the distance from structure.')
            for i, (x, y, z, ds, db) in enumerate(zip(self.Xn, self.Yn, self.Zn, d_s, d_b)):
                _m0.vec[i] = self.m0_fun(x, y, z, ds, db)
            self.m0.Set(_m0)
            print(f'm0 min/max: {np.min(self.m0.vec)}, {np.max(self.m0.vec)}')

        self.log('Filling d_b GridFunction')
        _db = ngs.GridFunction(self._fes)
        for i, (x, y, z) in enumerate(zip(self.Xn, self.Yn, self.Zn)):
            #d = np.min(d_b[i, :])
            _db.vec[i] = d_b[i]
        self.d_b = ngs.GridFunction(self.fes)
        self.d_b.Set(_db)
        self.gd_b = ngs.grad(self.d_b)

    def init_graphics(self):
        """Initializing plotting stuff."""

        self.tic()
        self.title('Initializing graphics')
        # pv.set_plot_theme('document')

        self.log(f'Reading domain mesh ({self.gmsh_filename})')
        self.pv_domain_mesh = pv.read(self.gmsh_filename)

        structure_mesh_filename = f'{self.gmsh_filename[:-11]}_structure.msh'
        self.log(f'Reading structure mesh ({structure_mesh_filename})')
        if os.path.exists(structure_mesh_filename):
            self.pv_structure_mesh = pv.read(structure_mesh_filename)
            self.log(f' - Number of structure nodes: {self.pv_structure_mesh.number_of_points}')
            self.pv_surf = self.pv_structure_mesh.extract_surface()
            # number of observable surface nodes
            self.nosn = self.pv_surf.points.shape[0] - \
                        np.sum(np.isclose(self.pv_surf.points[:, 2], np.min(self.pv_domain_mesh.points[:, 2])))
            self.log(f' - Number of surface nodes: {self.pv_surf.points.shape[0]}')
            self.log(f' - Number of observable surface nodes: {self.nosn}')
            self.sc = np.zeros(self.pv_surf.number_of_points)
            self.log(' - mesh loaded')
        else:
            self.log(' - no mesh file')

        # Preparing indices for animation frames
        self.frames = - np.ones(self.T.size, dtype=int)
        fr = 0
        for it in range(self.T.size):
            if it % self.animation_frame_steps == 0:
                self.frames[it] = fr
                fr += 1

        self.toc()


    def solve_trajecotries(self):
        """Main time loop for solving trajectories and their control."""
        self.title('Simulating coverage control')

        if self.it == 0:
            # Runs simulation from the beginning
            self.log('Starting new simulation')
            # self.plot_frame()
        else:
            # Continues simulation from given iteration.
            self.log(f'Starting simulation from t:{self.T[self.it]} (it:{self.it})')

        first_it = self.it

        # Making debug figures
        if self.debug_plots:
            for ia in range(self.an):
                self.plot_3d(plot_field='u', filename=f'debug/u_ia{ia}_{0:06d}', agent=ia)
                self.plot_3d(plot_field='m', filename=f'debug/m_ia{ia}_{0:06d}', agent=ia)
                self.plot_3d(plot_field='c', filename=f'debug/c_ia{ia}_{0:06d}', agent=ia)

        for self.it, t in enumerate(self.T):
            if self.it <= first_it:
                continue

            # Obtaining angular velocity from HEDAC potential field.
            vxyz = self.hedac_control()
            
            #linear combiantion of directional vectors to smooth out trajectories
            vxyz=self.inertial_effect(vxyz)

            # Correcting agent direction to avoid collisions with agents and the boundary
            vxyz=self.collision_avoidance(vxyz)
            
            #stash the directions
            self.vxyz=np.copy(vxyz)

            # Moving the agents
            for ia in range(self.an):
                self.AXYZ[ia, self.it, :] = self.AXYZ[ia, self.it -
                                                      1, :] + vxyz[ia, :] * self.AV[ia] * self.dt

            # Calculate coverage
            self.accumulate_coverage()

            # Calculate surface coverage
            self.calculate_surface_coverage()

            # Making animation frame
            if self.frames[self.it] >= 0:
                self.plot_3d()

            # Making debug figures
            if self.debug_plots:
                for ia in range(self.an):
                    self.plot_3d(plot_field='u', filename=f'debug/u_ia{ia}_{self.it:06d}', agent=ia)
                    self.plot_3d(plot_field='m', filename=f'debug/m_ia{ia}_{self.it:06d}', agent=ia)
                    self.plot_3d(plot_field='c', filename=f'debug/c_ia{ia}_{self.it:06d}', agent=ia)
                    # self.plot_3d(plot_field='dc', filename=f'debug/dc_ia{ia}_{self.it:06d}', agent=ia)

            # Save state and make animation (each state_save_steps)
            if self.it % self.state_save_steps == 0:
                self.save_state()
                self.plot_convergence(self.it)
                self.make_animation()

    def accumulate_coverage(self):

        self.tic()
        self.title(f'Accumulate coverage {self.it}/{self.tn - 1} t={self.T[self.it]:.3f}')
        # Calculates increase of coverage

        self._dc_nodal *= 0
        for ia, (ax, ay, az) in enumerate(self.AXYZ[:, self.it, :]):
            mask = (self.Xn - ax)**2 + (self.Yn - ay)**2 < self.AS[ia] ** 2

            x = self.Xn[mask]
            y = self.Yn[mask]
            z = self.Zn[mask]
            r = np.sqrt((x - ax)**2 + (y - ay)**2 + (z - az)**2)

            _phi = self.phi_fun(ia, r)
            self._dc_nodal[mask] += _phi
            # self._dc.vec[mask] += _phi

        self._dc_nodal *= self.dt
        for i in range(self.nn):
            self.c.vec[i] += self._dc_nodal[i]
        # mask = np.where(self._dc_nodal >= 1e-20)[0]
        # for i in mask:
        #     self.c.vec[i] = self._dc_nodal[i]


        # Accumulates increase to coverage c
        # self.c_new.Set(self.c + self._dc * self.dt)
        # self.c.Set(self.c_new)

        # Calculates the "uncovered" field which is the source in the heat equation.
        self.m.Set(self.m0 * ngs.exp(-self.c))

        self.eta_V[self.it] = 1 - ngs.Integrate(self.m, self.fe_mesh)
        # print(self.eta[:self.it+1])
        # input('?')

        # print(np.shape(self._dc.vec))
        # print(np.shape(self.dc.vec))
        # print(np.min(self._dc_nodal), np.max(self._dc_nodal))
        # print(np.min(self.c.vec), np.max(self.c.vec))
        # input(' >> Press return to continue.')
        # Measuring time
        self.execution_time[0, self.it] = self.toc()

    def hedac_control(self):
        """Set ups right hand terms for HEDAC PDE and calculates potential field u.
        Returns vector of angular velocities corresponding to directions determined by grad(u)."""

        self.tic()
        self.title(f'HEDAC step {self.it}/{self.tn - 1} t={self.T[self.it]:.3f}')

        # Set the right-hand side of the heat equation
        self.f = ngs.LinearForm(self.fes)
        self.f += ngs.SymbolicLFI(self.m
                                  # / np.average(self.m.vec[:self.nn])
                                  * self.v)
        self.f.Assemble()

        # Compute the solution (using precalculated inverse of system matrix A)
        self.gfu.vec.data = self.a_inv * self.f.vec

        # Obtaining the gradient function
        self.ggfu = ngs.grad(self.gfu)

        # Calculating the gradient at each agents' location
        # omega = np.zeros([self.an])
        VXYZ = np.zeros([self.an, 3])

        for ia in range(self.an):
            self.log(f'Agent {ia}')
            self.log(
                f' x: [{self.AXYZ[ia, self.it - 1, 0]}, {self.AXYZ[ia, self.it - 1, 1]}, {self.AXYZ[ia, self.it - 1, 2]}]')
            ax, ay, az = self.AXYZ[ia, self.it - 1, :]
            # print(self.AXYZ[ia, self.it - 1, :])
            aloc = self.fe_mesh(ax, ay, az)
            # print(ax, ay, az, aloc)
            vxyz = self.ggfu(aloc)
            self.log(f'{vxyz}')
            vxyz /= np.linalg.norm(vxyz)  # Direction is a unit vector
            VXYZ[ia, :] = vxyz
            self.log(f' v: [{vxyz[0]}, {vxyz[1]}, {vxyz[2]}]')

        # Obtaining the potential in all nodes and scaling the potential field to [0, 1]
        U = np.zeros(self.nn)
        for i, node in enumerate(self.nodes):
            U[i] = self.gfu.vec[i]
        self.u = U.real / np.max(U.real)

        # Measuring time
        self.execution_time[1, self.it] = self.toc()

        return VXYZ
    
    def inertial_effect(self, VXYZ):
        for ia in range(self.an):
            alpha=np.dot(VXYZ[ia, :],self.vxyz[ia, :])# alpha \in [-1,1]
            beta=0.1*(1-alpha)/2-1*(-1-alpha)/2
            v_lin_comb=beta*VXYZ[ia, :]+(1-beta)*self.vxyz[ia, :]
            #if we do not normalize, the agent will slow down for a sharp turn
            v_lin_comb/=np.linalg.norm(v_lin_comb)
            VXYZ[ia, :]=v_lin_comb
        return VXYZ

    def collision_avoidance(self, VXYZ):
        """
        Simple collision avoidance which must intersect 
        all half spaces defined by the normal vectors. """

        self.tic()
        self.title(f'Collision avoidance {self.it}/{self.tn - 1} t={self.T[self.it]:.3f}')

        # calculate distances for Colision avoidance
        tmpdistance = np.zeros((self.an, self.an))
        for ia in range(self.an):
            veca = np.array(self.AXYZ[ia, self.it - 1, :])
            tmpdistance[ia, ia] = np.inf
            for ib in range(ia+1, self.an):
                vecb = np.array(self.AXYZ[ib, self.it - 1, :])
                tmpdistance[ia, ib] = np.linalg.norm(veca-vecb)
        # make simetric
        distancmatrix = np.triu(tmpdistance) + np.triu(tmpdistance, 1).T
        for ia in range(self.an):

            ax, ay, az = self.AXYZ[ia, self.it - 1, :]
            #print(self.AXYZ[ia, self.it - 1, :])
            aloc = self.fe_mesh(ax, ay, az)
            print('Agent:',ia)
            print('location:',ax, ay, az)
            vxyz=np.copy(VXYZ[ia, :])
            vxyz/=np.linalg.norm(vxyz)
            CA=[]
            
            mindist=self.ca_safety_distance
            d2b=self.d_b(aloc)
            stepDistance=self.AV[ia]*self.dt
            if d2b<=self.ca_safety_distance+stepDistance:
                #tmp=self.gd_b(aloc)
                gdb=np.array(self.gd_b(aloc))
                gdb/=np.linalg.norm(gdb)
                CA.append(gdb)
                mindist=min(mindist,d2b)
            for ib in range(self.an):
                if distancmatrix[ia,ib]<=self.ca_safety_distance+stepDistance:
                    veca =np.array(self.AXYZ[ia, self.it - 1, :])
                    vecb =np.array(self.AXYZ[ib, self.it - 1, :])
                    vec=veca-vecb
                    vec/=np.linalg.norm(vec)
                    CA.append(vec)
                    mindist=min(mindist,distancmatrix[ia,ib])
    
            if len(CA)>0:
                CA.append(vxyz)
                rez=self.collision_avoidance_single(CA)
                if rez['success']:
                    print(f'Collision avoided for agent {ia} dir:',vxyz,' New dir:',rez['x']) 
                    vxyzCA=np.array(rez['x'])
                    vxyzCA/=np.linalg.norm(vxyzCA)                   
                    if d2b<=self.ca_safety_distance/2:    
                        print('worning collision. Less than half the safety distance ')
                        #print('press key')
                else:
                    print('Collision avoidance no solution. Set to 0')
                    #print('STOP in the name of love. Press key:')
                    vxyzCA=np.array([0,0,0])
                if(mindist<=self.ca_safety_distance/2+stepDistance/2):                 
                    # if mindist<=self.ca_safety_distance/4:
                    #     print('CA:',CA)
                    #     print('vxyzCA:',vxyzCA)       
                    #     print('vxyz:',vxyz)
                    #     print('This is not OK. Agent is very close to impact!')
                    VXYZ[ia, :]=vxyzCA#vxyz # Direction is a unit vector
                else:
                    alpha=2*(self.ca_safety_distance+stepDistance-mindist)/(self.ca_safety_distance+stepDistance)
                    VXYZ[ia, :]=alpha*vxyzCA+(1-alpha)*vxyz
            #end colision avoidance

        # Measuring time
        self.execution_time[2, self.it] = self.toc()
        return VXYZ
            
    def collision_avoidance_single(self,constraintvectorlist):
        """Simple collision avoidance which must intersect all half spaces defined by the normal vectors """
        A = -np.array(constraintvectorlist)           
        goalvector=np.sum(A, 0)#-vxyz
        goalvector=goalvector/np.linalg.norm(goalvector)
        b = np.zeros(len(constraintvectorlist))
        resL = linprog(goalvector, A_ub=A, b_ub=b, bounds=[-1, 1])
        def f(x):
            return np.dot(np.array(x),goalvector)
        def jac(x):
            return goalvector
        def hess(x):
            return np.zeros((np.size(x),np.size(x)))
        def ogran1(x):
            return np.dot(A,x)-b
         
        def ogran2(x):
            return x[0]**2+x[1]**2+x[2]**2-1
        def cons_J(x):
            return 2*x
        def cons_H(x, v):
            return 2*np.eye(np.size(x))

        x0=np.array(resL['x'])
        x0/=np.linalg.norm(x0)
        gr=(-1,1)
        granice = (gr, gr, gr)
        
        from scipy.optimize import LinearConstraint
        linear_constraint = LinearConstraint(A, -np.inf*np.ones(len(constraintvectorlist)), b)
        from scipy.optimize import NonlinearConstraint
        nonlinear_constraint = NonlinearConstraint(ogran2, -np.inf, 0., jac=cons_J, hess=cons_H)
        
        rjesenje =minimize(f, x0, method='trust-constr', jac=jac, hess=hess,
                       constraints=[linear_constraint, nonlinear_constraint],
                       options={'verbose': 0}, bounds=granice)
        if rjesenje['success']:    
            return rjesenje
        else: 
            return resL

    def make_animation(self):
        """Calling ffmpeg for making video files from created frames."""
        self.tic()
        self.title('Making animation')
        self.log(f'CWD: {self.results_dir}')

        self.log(f'Animation: hedac-fem_{self.case_name}_animation.mp4')
        p = subprocess.Popen(
            f'/usr/bin/ffmpeg -r {self.fps:d} -i frames/exploration_%06d.png -c:v libx264 -r 30 -pix_fmt yuv420p hedac-fem3d_{self.case_name}_animation.mp4 -y'.split(),
            #windows c:\ffmpeg\bin\ffmpeg.exe
            # f'ffmpeg -r {self.fps:d} -i exploration_%06d.png -vf "crop=trunc(iw/2)*2:trunc(ih/2)*2" -c:v libx264 -r 30 -pix_fmt yuv420p hedac-fem3d_{self.case_name}_animation.mp4 -y'.split(),
            cwd=self.results_dir,
            stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.communicate()
        self.log(str(p.stdout))
        self.log(str(p.stderr))

        self.toc()

    def plot_3d(self, plot_field=None, filename=None, agent=None, silent=False):

        self.tic()
        if not silent:
            self.title('Plotting 3D')
        # print(f'plot_field={plot_field}, filename={filename}')

        sargs = dict(height=0.02 * self.plot_options['window_size'][0] / self.plot_options['window_size'][1],
                     width=0.3,
                     vertical=False,
                     position_x=0.05,
                     position_y=0.95,
                     title='Number of observations\n',
                     title_font_size=30,
                     label_font_size=24, )

        pv.set_plot_theme("document")
        plotter = pv.Plotter(
            window_size=self.plot_options['window_size'],
            # Plots off screen when True. Helpful for saving screenshots without a window popping up.
            off_screen=True,
            line_smoothing=True,
            point_smoothing=True,
            polygon_smoothing=True,
        )
        # self.plotter.set_background('dff0f3', top='white')
        plotter.set_background('95acb0', top='white')

        if self.pv_structure_mesh:
            plotter.add_mesh(self.pv_structure_mesh,
                                 color='lightgrey',
                                 style='surface',
                                 # opacity=0.9,
                                 show_edges=True,
                                 show_scalar_bar=False)

            print(np.min(self.sc), np.max(self.sc))
            print(np.sum(self.sc > 0), np.size(self.sc))
            self.pv_surf['coverage'] = self.sc
            self.pv_surf.set_active_scalars('coverage')
            plotter.add_mesh(self.pv_surf,
                             scalars='coverage',
                             color='lightgray',
                             style='surface',
                             opacity='linear',
                             show_edges=True,
                             show_scalar_bar=True,
                             cmap=plt.cm.summer,
                             clim=[self.plot_options['sc_min'] , self.plot_options['sc_max']],
                             below_color='lightgray',
                             above_color=plt.cm.summer(255),
                             scalar_bar_args=sargs,
                             )
        # plotter.add_bounding_box(line_width=0.5, color='gray')
        else:
            plotter.add_mesh(self.pv_domain_mesh,
                         style='wireframe',
                         color='white',
                         # opacity=0.05 if plot_field else 0.03,
                         opacity=0.08,
                         show_edges=True,
                         show_scalar_bar=False)

        def lines_from_points(points):
            """Given an array of points, make a line set"""
            poly = pv.PolyData()
            poly.points = points
            cells = np.full((len(points) - 1, 3), 2, dtype=np.int_)
            cells[:, 1] = np.arange(0, len(points) - 1, dtype=np.int_)
            cells[:, 2] = np.arange(1, len(points), dtype=np.int_)
            poly.lines = cells
            return poly

        # Plotting fields
        for label, field in zip('d_s d_b m0 u m c dc'.split(), [self.d_s, self.d_b, self.m0, self.gfu, self.m, self.c, self._dc]):
            if plot_field == label and field:
                if not silent:
                    self.log(f'Plotting field {label}')
                self.pv_domain_mesh[label] = field.vec[:self.nn]
                self.pv_domain_mesh.set_active_scalars(label)
                if 'slice_planes' in self.plot_options:
                    slices = self.pv_domain_mesh.slice_orthogonal(x=self.plot_options['slice_planes'][0],
                                                                  y=self.plot_options['slice_planes'][1],
                                                                  z=self.plot_options['slice_planes'][2])
                    plotter.add_mesh(slices, scalars=label, scalar_bar_args=sargs, cmap='CET_L17')
                    # print(a)

                else:
                    # print(a)
                    if agent is not None:
                        slices = self.pv_domain_mesh.slice_orthogonal(x=self.AXYZ[agent, self.it, 0],
                                                                      y=self.AXYZ[agent, self.it, 1],
                                                                      z=self.AXYZ[agent, self.it, 2])
                    else:
                        slices = self.pv_domain_mesh.slice_orthogonal()
                    plotter.add_mesh(slices, scalars=label, scalar_bar_args=sargs, cmap='CET_L17')

                if not silent:
                    self.log(f'{label} min/max: {np.min(field.vec)}, {np.max(field.vec)}')

        agents = [agent] if agent is not None else list(range(self.an))
        if not plot_field or agent:
            for ia in agents:

                # Plot trajectory
                if self.it > 0:
                    line = lines_from_points(self.AXYZ[ia, :self.it + 1, :])
                    line["scalars"] = np.arange(line.n_points)
                    tube = line.tube(
                        radius=self.plot_options['trajectory_tube_radius'])
                    plotter.add_mesh(tube,
                                         # show_edges=True,
                                         # smooth_shading=True,
                                         show_scalar_bar=False,
                                        cmap=plt.cm.gist_heat,
                                         )

                # Plot current position
                sphere = pv.Sphere(
                    radius=3 * self.plot_options['trajectory_tube_radius'], center=self.AXYZ[ia, self.it, :])
                plotter.add_mesh(sphere,
                                     show_edges=False,
                                     smooth_shading=True,
                                     show_scalar_bar=False,
                                     color='blue',
                                     )

                # Plot camera directions
                if self.gd_s:
                    s = self.plot_options['arrow_thickness'] # size
                    for i in range(0, self.it, 3):
                        xyz = self.AXYZ[ia, i]
                        aloc = self.fe_mesh(xyz[0], xyz[1], xyz[2])
                        c_xyz = -np.array(self.gd_s(aloc))
                        c_xyz = c_xyz / np.linalg.norm(c_xyz)
                        arrow = pv.Arrow(start=xyz,
                                         direction=c_xyz * self.plot_options['camera_arrow_length'],
                                         tip_length=4*s,
                                         tip_radius=2*s,
                                         tip_resolution=20,
                                         shaft_radius=s,
                                         shaft_resolution=10,
                                         # scale=self.plot_options['camera_arrow_length']-s,
                                         scale='auto',
                                         )

                        plotter.add_mesh(arrow,
                                         # color='b52d07',
                                         # color='ff3399',
                                         color='003319',
                                         )

            # chart = pv.Chart2D(loc=(0.8, 0.8), size=(0.2, 0.2))
            # # _ = chart.scatter(x, y)
            # _ = chart.line(self.T, self.eta, 'black')


        # self.plotter.enable_eye_dome_lighting()
        plotter.camera.position = tuple(self.plot_options['camera_position'])
        plotter.camera.focal_point = tuple(self.plot_options['focal_point'])
        # plotter.camera.zoom(self.plot_options['camera_zoom'])

        _filename = f'{self.results_dir}{filename}.png' if filename else f'{self.results_dir}frames/exploration_{self.frames[self.it]:06d}.png'
        self.log(f'Saving figure {_filename}')
        # plotter.update()
        plotter.screenshot(_filename, window_size=self.plot_options['window_size'])
        plotter.close()

        self.toc()
        # input(' >> Press return to continue.')

    def save_state(self):
        """Saves the state of the search (everything needed for continuing the simulation)."""
        self.tic()
        self.title('Saving state')

        filename = f'{self.results_dir}states/state_{self.it:06d}.npz'
        self.log(f'it: {self.it}')
        self.log(f'State filename: {filename}')
        np.savez_compressed(filename,
                            it=self.it,
                            time=self.T,
                            AXYZ=self.AXYZ,
                            # AD=self.AD,
                            # AVXYZ=self.AVXYZ,
                            c=self.c.vec,
                            gfu=self.gfu.vec,
                            # ggfu=self.ggfu.vec,
                            sc=self.sc,
                            execution_time=self.execution_time,
                            eta_V=self.eta_V,
                            eta_A=self.eta_A,
                            )
        self.toc()
        # self.trajectory_analysis()

    def load_state(self, it):
        """Loads the state of the search (everything needed for continuing the simulation)."""
        self.tic()
        self.title('Loading state')
        filename = f'{self.results_dir}states/state_{it:06d}.npz'
        self.log(f'State filename: {filename}')
        if os.path.exists(filename):
            npz = np.load(filename, allow_pickle=True)
            self.log(f'Variables: {npz.files}')
        else:
            return False

        self.it = npz['it']
        self.log(f'it: {self.it}')
        self.AXYZ[:, :self.it + 1, :] = npz['AXYZ'][:, :self.it + 1, :]

        for i, c in enumerate(npz['c']):
            self.c.vec[i] = c
        for i, u in enumerate(npz['gfu']):
            self.gfu.vec[i] = u

        Z = np.zeros(self.nn)
        for i, node in enumerate(self.nodes):
            Z[i] = self.gfu.vec[i]
        self.u = Z.real / np.max(Z.real)
        # Calculating the gradient function
        self.ggfu = ngs.grad(self.gfu)
        self.sc = npz['sc']

        self.execution_time[:, :self.it + 1] = npz['execution_time'][:, :self.it + 1]
        self.eta_V[:self.it + 1] = npz['eta_V'][:self.it + 1]
        self.eta_A[:self.it + 1] = npz['eta_A'][:self.it + 1]

        npz.close()
        self.m.Set(self.m0 * ngs.exp(-self.c))

        self.toc()
        return True

    def plot_convergence(self, it=None, filename=None):
        self.title('Plotting convergence')
        if not it:
            it = self.it + 1

        # fig, axes = plt.subplots(figsize=[10, 5], nrows=2, constrained_layout=True, sharex=True)
        fig, axes = plt.subplots(figsize=[6, 4], nrows=2, constrained_layout=True, sharex=True)
        ax1, ax2 = axes

        ax1.plot(self.T, self.eta_V, label=r'$\eta_V$')
        ax1.plot(self.T, self.eta_A, label=r'$\eta_A$')
        ax1.set_xlabel(r'Time, $t$ [s]')
        ax1.set_ylabel(r'$\eta_V$, $\eta_A$ [-]')
        ax1.set_ylim(0, 1)
        ax1.set_xlim(self.T[0], self.T[-1])
        ax1.grid(lw=0.2, c='k')
        ax1.legend()

        Ds = np.full([self.an, self.tn], np.nan)
        if self.d_s:
            for ia in range(self.an):
                # self.AXYZ = np.full([self.an, self.tn, 3], np.nan)
                for _it in range(it):
                    ax, ay, az = self.AXYZ[ia, _it, :]
                    Ds[ia, _it] = self.d_s(self.fe_mesh(ax, ay, az))

                ax2.plot(self.T, Ds[ia, :], lw=1, label=f'Agent {ia + 1}')

        ax2.set_ylabel(r'Distance to structure [m]')
        ax2.axhline(self.plot_options['d_bar'], c='k', lw=1, ls='--', label=r'Inspection distance $\bar{d}$')
        ax2.axhline(self.ca_safety_distance*0.5, c='k', lw=1, ls='-', label=r'Safety distance $\epsilon$')
        # ax2.set_yscale('log')
        ax2.grid(lw=0.2, c='k')
        ax2.legend(ncol=3, fontsize=8)
        fig.align_ylabels()

        npz_filename = f'{self.results_dir}/{self.case_name}_distances.npz'
        np.savez_compressed(npz_filename,
                            T=self.T,
                            eta_V=self.eta_V,
                            eta_A=self.eta_A,
                            Ds=Ds,
                            AXYZ=self.AXYZ,
                            d_bar=self.plot_options['d_bar'],
                            sft_d=self.ca_safety_distance*0.5,
                            )

        if filename:
            filename = f'{self.results_dir}{filename}'
        else:
            # filename = f'{self.results_dir}/convergence_{it:06d}.png' if it else f'{self.results_dir}/convergence.png'
            filename = f'{self.results_dir}/convergence.png'
        self.log(filename)
        plt.savefig(filename)

        return fig, ax1

    def node_in_cone(self, agent, dir, node):
        dist = node - agent
        h_dist = np.dot(dist, dir)
        if not (0 <= h_dist <= self.FOV_H):
            return 0

        cone_radius = (h_dist / self.FOV_H) * self.FOV_R
        r_dist = np.linalg.norm(dist - h_dist * dir)

        # print(f'{axyz=}, {node=}, {dir=}')
        # print(f'{h_dist=}, {r_dist=}, {cone_radius=}')

        if r_dist < cone_radius:
            # Ray trace
            points, cell = self.pv_surf.ray_trace(agent, node, plot=False)

            if points.shape[0] > 1:
                if np.allclose(points[:, 0], node[0], atol=1e-3) and \
                        np.allclose(points[:, 1], node[1],atol=1e-3) and \
                        np.allclose(points[:, 2], node[2], atol=1e-3):
                    return 2
                else:
                    return 1
            else:
                return 2

        return 0

    def calculate_surface_coverage(self, make_frames=False):

        self.title('Surface coverage calculation')
        self.tic()

        self.log(f'Number of surface nodes: {self.pv_surf.number_of_points}')
        self.log(f'Number of observable surface nodes: {self.nosn}')

        self.log(f'Calculating surface coverage for t={self.T[self.it]} ({self.it})')
        for ia in range(self.an):
            axyz = self.AXYZ[ia, self.it, :]
            ngs_point = self.fe_mesh(*axyz)
            dir = -np.array(self.gd_s(ngs_point))
            dir /= np.linalg.norm(dir)
            for i, node in enumerate(self.pv_surf.points):
                # Calculate if node is inside of a cone
                self.sc[i] += (self.node_in_cone(axyz, dir, node) == 2)

        #np.savez_compressed(f'{self.results_dir}/surface_coverage/sc_{self.it:06d}.npz', sc=sc)
        self.eta_A[self.it] = np.sum(self.sc >= 1) / self.nosn

        # self.log(f' - Surface coverage max: {self.sc.max()}')
        # self.log(f' - Surface coverage std dev: {self.sc.std()}')
        self.log(f' - Surface coverage measure: {self.eta_A[self.it]}')
        self.toc()

    def recalculate_surface_coverage(self, make_frames=False):

        self.title('Surface coverage calculation')

        if not os.path.exists(f'{self.results_dir}/surface_coverage'):
            os.mkdir(f'{self.results_dir}/surface_coverage')

        self.log(f'Number of surface nodes: {self.pv_surf.number_of_points}')
        sc = np.zeros(self.pv_surf.number_of_points)

        self.surf_coverage = np.zeros(self.it + 1)

        for it in range(self.it + 1):
            self.log(f'Calculating surface coverage for t={self.T[it]} ({it}/{self.it})')
            self.tic()
            for ia in range(self.an):
                axyz = self.AXYZ[ia, it, :]
                ngs_point = self.fe_mesh(*axyz)
                dir = -np.array(self.gd_s(ngs_point))
                dir /= np.linalg.norm(dir)
                for i, node in enumerate(self.pv_surf.points):
                    # Calculate if node is inside of a cone
                    sc[i] += (self.node_in_cone(axyz, dir, node) == 2)

            np.savez_compressed(f'{self.results_dir}/surface_coverage/sc_{it:06d}.npz', sc=sc)
            self.surf_coverage[it] = np.sum(sc >= 1) / sc.size

            self.log(f' - Surface coverage max: {sc.max()}')
            self.log(f' - Surface coverage std dev: {sc.std()}')
            self.log(f' - Surface coverage measure: {np.sum(sc >= 1) / sc.size}')
            self.toc()

            if make_frames:
                self.tic()
                self.pv_surf['coverage'] = sc
                self.pv_surf.set_active_scalars('coverage')

                sargs = dict(height=0.02 * self.plot_options['window_size'][0] / self.plot_options['window_size'][1],
                             width=0.3,
                             vertical=False,
                             position_x=0.05,
                             position_y=0.97,
                             title_font_size=30,
                             label_font_size=24, )

                pv.set_plot_theme("document")
                plotter = pv.Plotter(
                    window_size=self.plot_options['window_size'],
                    # Plots off screen when True. Helpful for saving screenshots without a window popping up.
                    off_screen=True,
                    line_smoothing=True,
                    point_smoothing=True,
                    polygon_smoothing=True,
                )
                # self.plotter.set_background('dff0f3', top='white')
                plotter.set_background('95acb0', top='white')

                # self.plotter.enable_eye_dome_lighting()
                plotter.camera.position = tuple(self.plot_options['camera_position'])
                plotter.camera.focal_point = tuple(self.plot_options['focal_point'])
                # plotter.camera.zoom(self.plot_options['camera_zoom'])


                plotter.add_mesh(self.pv_structure_mesh,
                                     color='lightgrey',
                                     style='surface',
                                     # opacity=0.9,
                                     show_edges=True,
                                     show_scalar_bar=False)

                plotter.add_mesh(self.pv_surf,
                                     #color='lightgrey',
                                     style='surface',
                                     # opacity=0.9,
                                     #show_edges=True,
                                     show_scalar_bar=True)
                # pv_surf.plot()


                # def lines_from_points(points):
                #     """Given an array of points, make a line set"""
                #     poly = pv.PolyData()
                #     poly.points = points
                #     cells = np.full((len(points) - 1, 3), 2, dtype=np.int_)
                #     cells[:, 1] = np.arange(0, len(points) - 1, dtype=np.int_)
                #     cells[:, 2] = np.arange(1, len(points), dtype=np.int_)
                #     poly.lines = cells
                #     return poly

                for ia in range(self.an):

                    # # Plot trajectory
                    # if it > 0:
                    #     line = lines_from_points(self.AXYZ[ia, :it + 1, :])
                    #     #line["scalars"] = np.arange(line.n_points)
                    #     tube = line.tube(
                    #         radius=self.plot_options['trajectory_tube_radius'] * 0.6)
                    #     plotter.add_mesh(tube,
                    #                          # show_edges=True,
                    #                          # smooth_shading=True,
                    #                          show_scalar_bar=False,
                    #                          color='grey',
                    #                          )

                    # Plot current position
                    sphere = pv.Sphere(
                        radius=3 * self.plot_options['trajectory_tube_radius'], center=self.AXYZ[ia, it, :])
                    plotter.add_mesh(sphere,
                                         show_edges=False,
                                         smooth_shading=True,
                                         show_scalar_bar=False,
                                         color='blue',
                                         )


                    # Plot camera directions
                    if self.gd_s:
                        xyz = self.AXYZ[ia, it, :]
                        aloc = self.fe_mesh(xyz[0], xyz[1], xyz[2])
                        dir = -np.array(self.gd_s(aloc))
                        dir = dir / np.linalg.norm(dir)

                        plotter.add_mesh(pv.Cone(center=xyz + 0.5 * dir * self.FOV_H,
                                                 direction=-dir,
                                                 height=self.FOV_H,
                                                 radius=self.FOV_R,
                                                 resolution=36),
                                         opacity=0.2,
                                         color='yellow')

                    # chart = pv.Chart2D(loc=(0.8, 0.8), size=(0.2, 0.2))
                    # # _ = chart.scatter(x, y)
                    # _ = chart.line(self.T, self.eta, 'black')

                filename = f'{self.results_dir}/surface_coverage/surf_coverage_{it:06d}.png'
                self.log(f'Saving figure {filename}')
                plotter.screenshot(filename, window_size=self.plot_options['window_size'])
                plotter.close()

                self.toc()

        fig, ax = plt.subplots(figsize=[6, 3], nrows=1, constrained_layout=True, sharex=True)
        ax.plot(self.T[:self.it + 1], self.surf_coverage[:self.it + 1] * 100)
        ax.set_xlabel('Time [s]')
        ax.set_ylabel('Surface coverage [%]')
        plt.savefig(f'{self.results_dir}/{self.case_name}_surface_coverage.png')
        plt.close()


    def make_surface_coverage_figure(self):

        self.title('Creating surface coverage figure')

        if not os.path.exists(f'{self.results_dir}/surface_coverage'):
            os.mkdir(f'{self.results_dir}/surface_coverage')

        pv.set_plot_theme("document")
        plotter = pv.Plotter(
            window_size=self.plot_options['window_size'],
            # Plots off screen when True. Helpful for saving screenshots without a window popping up.
            off_screen=True,
            line_smoothing=True,
            point_smoothing=True,
            polygon_smoothing=True,
        )
        # self.plotter.set_background('dff0f3', top='white')
        plotter.set_background('95acb0', top='white')
        pv.global_theme.edge_color = 'white'

        # self.plotter.enable_eye_dome_lighting()
        plotter.camera.position = tuple(self.plot_options['camera_position'])
        plotter.camera.focal_point = tuple(self.plot_options['focal_point'])
        # plotter.camera.zoom(self.plot_options['camera_zoom'])

        self.log(f'Number of surface nodes: {self.pv_surf.number_of_points}')
        sc = np.zeros(self.pv_surf.number_of_points)

        self.log(f'Calculating surface coverage for t={self.T[self.it]} ({self.it})')
        self.tic()
        for ia in range(self.an):
            axyz = self.AXYZ[ia, self.it, :]
            ngs_point = self.fe_mesh(*axyz)
            dir = -np.array(self.gd_s(ngs_point))
            dir /= np.linalg.norm(dir)

            ray = pv.Line(axyz, axyz + dir * self.FOV_H)
            plotter.add_mesh(ray, color='blue', line_width=20)

            for i, node in enumerate(self.pv_surf.points):
                # Calculate if node is inside of a cone
                in_cone = self.node_in_cone(axyz, dir, node)

                sc[i] += (in_cone == 2)

                if in_cone > 0:
                    ray = pv.Line(axyz, node)
                    plotter.add_mesh(ray, color='orange', line_width=3)

                    sphere = pv.Sphere(center=node,
                                       radius=3 * self.plot_options['trajectory_tube_radius'],
                                       )
                    plotter.add_mesh(sphere,
                                     show_edges=False,
                                     smooth_shading=True,
                                     show_scalar_bar=False,
                                     color='green' if in_cone == 2 else 'red',
                                     )

        self.log(f' - Surface coverage max: {sc.max()}')
        self.log(f' - Surface coverage std dev: {sc.std()}')
        self.log(f' - Surface coverage measure: {np.sum(sc >= 1) / sc.size}')

        self.pv_surf['coverage'] = sc
        self.pv_surf.set_active_scalars('coverage')

        plotter.add_mesh(self.pv_surf,
                         # color='lightgrey',
                         #style='surface',
                         # opacity=0.9,
                         show_edges=True,
                         edge_color='white',
                         show_scalar_bar=False)

        for ia in range(self.an):

            # Plot current position
            sphere = pv.Sphere(
                radius=3 * self.plot_options['trajectory_tube_radius'], center=self.AXYZ[ia, self.it, :])
            plotter.add_mesh(sphere,
                                 show_edges=False,
                                 smooth_shading=True,
                                 show_scalar_bar=False,
                                 color='blue',
                                 )

            # Plot camera directions
            if self.gd_s:
                xyz = self.AXYZ[ia, self.it, :]
                aloc = self.fe_mesh(xyz[0], xyz[1], xyz[2])
                dir = -np.array(self.gd_s(aloc))
                dir = dir / np.linalg.norm(dir)

                plotter.add_mesh(pv.Cone(center=xyz + 0.5 * dir * self.FOV_H,
                                         direction=-dir,
                                         height=self.FOV_H,
                                         radius=self.FOV_R,
                                         resolution=36),
                                 opacity=0.2,
                                 color='yellow')

        plotter.store_image = True
        filename = f'{self.results_dir}/surf_coverage_fig.png'
        self.log(f'Saving figure {filename}')
        plotter.screenshot(filename, window_size=self.plot_options['window_size'])
        plotter.close()
        self.toc()


