from hedacfem3d import HedacFEM3D
import numpy as np
import matplotlib.pyplot as plt

test = HedacFEM3D('case_01_unit_cube', delete_dir=False)
test.gmsh_filename = 'meshes/unit_cube_domain.msh'


def m0(x, y, z, ds, db):
    if z<=0.45:
        if db > 0.1:
            return 1.0
        elif 0.05< db < 0.1:
            return 1.0 *(db-0.05)/0.05
        else:
            return 0.
    else:
        return 0.


def phi(ia, r):
    return 1.e-2 * np.exp(-(r ** 2) / (2 * 0.05 ** 2))
    #return 1.5 * np.exp(-(r ** 2) / (2 * 0.15 ** 2))


test.m0_fun = m0
na = 100  # Number of agents

# test.AX0 = np.linspace(0.1, 0.9, na)
# test.AY0 = np.full(na, 0.1)
# test.AZ0 = np.linspace(0.1, 0.2, na)

test.AX0 = np.random.rand(na)*0.5+0.25
test.AY0 = np.random.rand(na)*0.5+0.25
test.AZ0 = np.random.rand(na)*0.5+0.25

test.AD0 = np.deg2rad([-90, -90])
test.AV = np.ones(na) * 0.01 # m/s
test.phi_fun = phi
test.AS = np.full(na, 120)

test.alpha = 0.2  # HEDAC's conduction parameter (greater alpha => global/broader search)
test.beta = 0.1

test.ca_safety_distance = 0.05  # m, collision avoidance safety distance
test.inspection_distance = 0.05  # m, mean distance from structure faces from which inspection is conducted

test.T = np.linspace(0, 1000, 2001)  # Time discretization
test.animation_frame_steps = 100
test.state_save_steps = 100
test.fps = 20 # frames per second, animation video speed

test.plot_options['trajectory_tube_radius'] = 0.003
test.plot_options['window_size'] = [2500, 2500]
test.plot_options['focal_point'] = [0.5, 0.5, 0.4]
test.plot_options['camera_position'] = [2.5, 2.5, 2.0]
test.plot_options['camera_zoom'] = 1.3
test.plot_options['camera_arrow_length'] = 1.5
test.plot_options['slice_planes'] = [0, -5.7, 0]
test.plot_options['arrow_thickness'] = 0.02
test.plot_options['d_bar'] = 1.5

test.debug_plots = False
# test.initialize(skip_a_inv_calculation=True)
#test.load_state(200)
test.initialize()
test.solve_trajecotries()
