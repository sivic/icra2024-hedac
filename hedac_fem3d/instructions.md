Instructions for installing anaconda environment and all needed modules for running **HEDAC-FEM3D** code

All commands should be executed in Anaconda prompt (on Windows) or in terminal with initialized conda paths or with full-path conda commands `/opt/anaconda/bin/conda` (Linux). 

Create new environment (use whatever you like instead of `hedac-fem3d` for environment name) based on Python 3.8:

`conda create -n hedac-fem3d python=3.8`

Activate newly created environment:

`conda activate hedac-fem3d`

Install **ngsolve** module:

`conda install -c ngsolve ngsolve`

Install **pyvista** module:

`conda install -c conda-forge pyvista pyvistaqt`

Install all other modules:

`conda install -c conda-forge numpy scipy matplotlib spyder`

That's it!



Whenever you want to run a code or start an IDE you should activate `hedac-fem3d` environemnt in your Terminl:

 `conda activate hedac-fem3d`

and then start Spyder which uses all packages in this environment with

`spyder` or `spyder &` (to "disconnect" it from terminal)

or if you want to run code directly:

`python test_hedac_fem3d.py`



