from hedacfem3d import HedacFEM3D
import numpy as np
import matplotlib.pyplot as plt

test = HedacFEM3D('case_03_wind_turbine', delete_dir=False)
test.gmsh_filename = 'meshes/windturbine_domain.msh'

stdev = 1
dist = 6.0
def m0(x, y, z, ds, db):
    _m0 = np.exp(-0.5 * (ds - dist) ** 2 / stdev ** 2) / (stdev * np.sqrt(2 * np.pi))
    # if db < 0.1:
    #     _m0 *= db/0.1
    return _m0

sigma = 3
def phi(ia, r):
    # return 4 * np.exp(-(r ** 2) / (2 * 3.0 ** 2))
    return 400 * 1 / (sigma**3 * (2 * np.pi)**1.5) * np.exp(-(r ** 2) / (2 * sigma ** 2))


test.m0_fun = m0
test.AX0 = np.array([10, 10])
test.AY0 = np.array([-20, 20])
test.AZ0 = np.array([0, 0])
test.AV = np.ones(2) * 1.2 # m/s
test.phi_fun = phi
test.AS = np.full(2, 120)
test.FOV_R = 3.5 # m (radius of Field of View)
test.FOV_H = 8 # m (height/distance of Field of View)

test.alpha = 15  # HEDAC's conduction parameter (greater alpha => global/broader search)
test.beta = 1

test.ca_safety_distance = 2  # m, collision avoidance safety distance
# test.inspection_distance = 5.0  # m, mean distance from structure faces from which inspection is conducted

test.T = np.linspace(0, 1200, 2401)  # Time discretization
test.animation_frame_steps = 1
test.state_save_steps = 100
test.fps = 30 # frames per second, animation video speed

test.plot_options['trajectory_tube_radius'] = 0.2
test.plot_options['window_size'] = [2500, 4000]
test.plot_options['focal_point'] = [0, 10, 108]
test.plot_options['camera_position'] = [300, 300, 150]
test.plot_options['camera_zoom'] = 1.5
# test.plot_options['slice_planes'] = [test.AX0[0], test.AY0[0], test.AZ0[0]]
test.plot_options['camera_arrow_length'] = 6
test.plot_options['arrow_thickness'] = 0.015
test.plot_options['d_bar'] = 6.0
test.plot_options['sc_max'] = 20.0

test.initialize(skip_a_inv_calculation=False)
if not test.load_state(test.T.size - 1):
    # test.initialize()
    test.solve_trajecotries()

# test.calculate_surface_coverage()

test.plot_3d(plot_field='m0', filename=f'case_3_wind_turbine_mu0')
test.plot_3d(filename=f'case_3_wind_turbine_inspection')
test.plot_convergence(filename=f'case_3_wind_turbine_convergence.pdf')