from hedacfem3d import HedacFEM3D
import numpy as np
import matplotlib.pyplot as plt

test = HedacFEM3D('case_04_bridge_b', delete_dir=False)
test.gmsh_filename = 'meshes/bridge_domain.msh'



def m0(x, y, z, ds, db):
    stdev = 0.3
    dist = 1.5
    _m0 = np.exp(-0.5 * (ds - dist) ** 2 / stdev ** 2) / (stdev * np.sqrt(2 * np.pi))
    # if db < 0.05:
    #     _m0 *= db/0.05
    return _m0

if test.case_name[-2:] == '_a':
    na = 5  # Number of agents
    test.FOV_R = 1.5 # m (radius of Field of View)
    test.FOV_H = 2 # m (height/distance of Field of View)
elif test.case_name[-2:] == '_b':
    na = 1  # Number of agents
    test.FOV_R = 8  # m (radius of Field of View)
    test.FOV_H = 10  # m (height/distance of Field of View)

if test.case_name[-2:] == '_a':
    sigma = 1.0
    def phi(ia, r):
        # return 3.0 / na * np.exp(-(r ** 2) / (2 * 1.0 ** 2))
        return 100 / na / (sigma**3 * (2 * np.pi)**1.5) * np.exp(-(r ** 2) / (2 * sigma ** 2))
elif test.case_name[-2:] == '_b':
    sigma = 1.0
    def phi(ia, r):
        #return 10.0 * np.exp(-(r ** 2) / (2 * 1.0 ** 2))
        return 20 / (sigma**3 * (2 * np.pi)**1.5) * np.exp(-(r ** 2) / (2 * sigma ** 2))



test.m0_fun = m0
test.AX0 = np.full(na, 28)  # m
test.AY0 = np.array([0]) if test.case_name[-2:] == '_b' else np.linspace(-9, 9, na)  # m
test.AZ0 = np.full(na, 5)  # m
test.AV = np.full(na, 0.5)  # m/s
test.phi_fun = phi
test.AS = np.full(na, 6)  # m

test.alpha = 20  # HEDAC's conduction parameter (greater alpha => global/broader search)
test.beta = 1

test.ca_safety_distance = 1.0  # m, collision avoidance safety distance

if test.case_name[-2:] == '_a':
    test.T = np.linspace(0, 10*60, 2*10*60+1)  # Time discretization
    test.animation_frame_steps = 2
    test.state_save_steps = 60*2*5
    test.fps = 30  # frames per second, animation video speed
elif test.case_name[-2:] == '_b':
    test.T = np.linspace(0, 50*60, 2*50*60+1)  # Time discretization
    test.animation_frame_steps = 2
    test.state_save_steps = 60*2*5
    test.fps = 30  # frames per second, animation video speed

test.plot_options['trajectory_tube_radius'] = 0.05
test.plot_options['window_size'] = [5000, 3200]
test.plot_options['focal_point'] = [6, 0, 1]
test.plot_options['camera_position'] = [38, 38, 34]
test.plot_options['camera_zoom'] = 1.0
test.plot_options['camera_arrow_length'] = 1.5
test.plot_options['slice_planes'] = [0, -5.7, 0]
test.plot_options['arrow_thickness'] = 0.02
test.plot_options['d_bar'] = 1.5
test.plot_options['sc_max'] = 20.0

test.initialize(skip_a_inv_calculation=False)
if not test.load_state(test.T.size - 1):
    # test.initialize()
    test.solve_trajecotries()

# test.load_state(600)
# test.solve_trajecotries()
# test.calculate_surface_coverage()

test.plot_convergence(filename=f'case_4_bridge_{test.case_name[-1]}_convergence.pdf')
test.plot_3d(plot_field='m0', filename=f'case_4_bridge_mu0')
if test.case_name[-2:] == '_a':
    test.plot_3d(filename=f'case_4_bridge_a_inspection')
elif test.case_name[-2:] == '_b':
    test.plot_3d(filename=f'case_4_bridge_b_inspection')
