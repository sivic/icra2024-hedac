from hedacfem import HedacFem
import numpy as np
import netgen.read_gmsh as read_gsmh
import matplotlib.pyplot as plt

test = HedacFem('case_03_archipelago', delete_dir=False)

test.mesh = read_gsmh.ReadGmsh('archipelago/archipelago_gmsh_45m.msh')
test.boundary_offset = 0.0

def m0(x, y):
    return np.exp((-x**2 - y**2)/(2 * 1800**2))

k = np.tan(np.deg2rad(60))
def phi(ia, x, y):
    # x is the heading direction
    # y is lateral direction
    r = np.sqrt(x**2 + y**2)
    mask = np.logical_and(np.logical_and(y > -k*x, y < k*x), np.logical_and(x > 0, r < 120))
    # mask = np.logical_and(r < 120, x > 0)
    # mask = r < 120
    _phi = np.zeros_like(x)
    _phi[mask] = 120 - r[mask]
    return _phi * 4e-4

test.m0_fun = m0
na = 10  # Number of agents
test.AX0 = np.array([  800,  1200,  1600, -2000, -2300, -2600, 2800, 2800, 2800, 2800])
test.AY0 = np.array([-2750, -2750, -2750, -2750, -2750, -2750, -900, -300,  300,  900])
# test.AD0 = np.deg2rad(np.linspace(0, 360, na, endpoint=False))
test.AD0 = np.deg2rad([ 90, 90, 90, 90, 90, 90, -180, -180, -180, -180])
test.AV = np.ones(na) * 3.0 # m/s #20čv
test.AR = np.ones(na) * 13.22  # m
test.AC = np.ones(na) * 6  # m
# test.AA = np.ones(na) * 0.1
# test.AS = np.ones(na) * 10  # m
test.phi_fun = phi
test.AS = np.full(na, 120)

test.alpha = 1e4  # HEDAC's conduction parameter (greater alpha => global/broader search)
test.beta = 0.05

test.T = np.linspace(0, 3600*3, 3601)  # Time discretization
# test.T = np.linspace(0, 100*3, 101)  # Time discretization

test.plot_options['scale_xy'] = -2300, 2500
test.plot_options['scale_w'] = 500
test.plot_options['scale_units'] = 'm', 2.25
test.plot_options['ms'] = 16  # Size of the yellow arrow marking agent position and direction in the visualizations
test.plot_options['case_title']  = 'Test case 3: Archipelago'
test.state_save_steps = 1

test.initialize()
if not test.load_state(np.size(test.T) - 1):
    test.solve_trajecotries()
    test.plot_field(test.m, f'm_{test.it}.png')
    test.make_animation()

test.trajectory_analysis()

fig, ax = plt.subplots(figsize=(10.1, 10.0))
plt.subplots_adjust(left=0,
                    right=1,
                    bottom=0,
                    top=1)
fig.patch.set_facecolor('none')
test.plot_situation(ax, gradient_scale=0.9)
fig.savefig(f'{test.case_name}/{test.case_name}.png', facecolor=fig.get_facecolor())
plt.close(fig)
