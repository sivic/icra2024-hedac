# -*- coding: utf-8 -*-
"""
Created on Sat Feb 14 17:25:16 2015

@author: stefan
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import os
import shutil
from matplotlib.colors import Normalize
import matplotlib.patheffects as pe
import matplotlib as mpl
import subprocess
import matplotlib.tri as tri
import time
import seaborn
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from matplotlib.collections import PatchCollection

import vtk
import colorcet as cc
import pyvista as pv

import shapely.geometry as shg
import shapely.ops as sho

import ngsolve as ngs

ngs.ngsglobals.msg_level = 1

import itertools

import logging

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger("rothemain.rothe_utils")
logging.getLogger('UFL').setLevel(logging.WARNING)
logging.getLogger('FFC').setLevel(logging.WARNING)


# set the colormap and centre the colorbar
class MidpointNormalize(mpl.colors.Normalize):
    """
    Normalise the colorbar so that diverging bars work there way either side from a prescribed midpoint value)

    e.g. im=ax1.imshow(array, norm=MidpointNormalize(midpoint=0.,vmin=-100, vmax=100))
    """

    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        mpl.colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y), np.isnan(value))


# Plots a Polygon to pyplot `ax`
def plot_polygon(ax, poly, **kwargs):
    path = Path.make_compound_path(
        Path(np.asarray(poly.exterior.coords)[:, :2]),
        *[Path(np.asarray(ring.coords)[:, :2]) for ring in poly.interiors])

    patch = PathPatch(path, **kwargs)
    collection = PatchCollection([patch], **kwargs)

    ax.add_collection(collection, autolim=True)
    ax.autoscale_view()
    return collection

class HedacFem():

    def __init__(self, case_name, delete_dir=False):

        self.tic()

        self.case_name = case_name
        self.title('Initializing HEDAC-FEM instance')

        self.log('CWD: {}'.format(os.getcwd()))
        self.log('Script: {}'.format(__file__))
        self.log('Results dir: {}'.format(os.path.abspath(case_name)))
        if delete_dir:
            if os.path.exists(case_name):
                shutil.rmtree(case_name)
        if not os.path.exists(case_name):
            os.mkdir(case_name)
        self.results_dir = os.path.abspath(case_name) + '/'

        self.obstacles = None
        self.mesh = None
        self.AX0 = None
        self.AY0 = None
        self.AD0 = None
        self.AV = None
        self.AR = None
        self.AS = None
        self.AC = None
        self.T = None
        self.it = 0
        self.m0_fun = None
        self.phi_fun = None
        self.u = None

        self.plot_options = {}
        self.state_save_steps = 100  # After how many steps state is saved

        # Dubins rotation matrices
        self.DRl = None
        self.DRr = None

        self.toc()

    def title(self, title):
        """Prints a formatted title to the console."""

        print()
        print()
        print('═' * 100)
        print('  ' + title.ljust(96) + '  ')
        print('─' * 100)

    def log(self, msg):
        """Prints a formatted log message to the console."""

        print('    ' + msg)

    def tic(self):
        """Starts a timer."""

        self._time1 = time.time()

    def toc(self, msg=''):
        """Measures the elapsed time since the timer was started (using tic method).
        Prints the formatted message containing elapsed time."""

        s = time.time() - self._time1
        if s < 1:
            self.log('{}Elapsed time: {:.3f} miliseconds.'.format('' if msg == '' else msg + '. ', s * 1e3))
        else:
            self.log('{}Elapsed time: {:.3f} seconds.'.format('' if msg == '' else msg + '. ', s))
        return s

    def initialize(self):
        """Initialization of the solver. Should be called before running the simulation."""

        self.init_domain()
        self.init_agents()
        self.init_fem()
        self.init_graphics()

    def init_domain(self):
        """Domain, time, FEM mesh and Shapely geometric objects initialization for the domain nad obstacles."""

        self.tic()
        self.title('Initializing domain')

        # Time discretization
        self.tn = np.size(self.T)
        self.dt = self.T[1] - self.T[0]
        self.log('Time: {} to {}'.format(self.T[0], self.T[-1]))
        self.log('Time step: {}'.format(self.dt))
        self.eta = np.full(np.size(self.T), np.nan)
        self.eta[0] = 0.0

        # Nodes and elements from the mesh
        self.nodes = self.mesh.Points()  # Nodes
        self.nn = len(self.nodes)  # Number of nodes
        self.elements = self.mesh.Elements2D()  # Elements
        self.ne = len(self.elements)  # Number of elements
        self.Xn = np.zeros(self.nn)  # Nodes x coordinates
        self.Yn = np.zeros(self.nn)  # Nodes y coordinates
        for i, node in enumerate(self.nodes):
            self.Xn[i], self.Yn[i], _ = node

        self.EL = np.zeros([self.ne, 3], dtype=int)  # Elements topology (connections)
        for i, e in enumerate(self.elements):
            self.EL[i, :] = [v.nr - 1 for v in e.vertices]

        # Initializing ngsolve mesh
        self.fe_mesh = ngs.Mesh(self.mesh)

        # Domain bounds:
        x_min, x_max, y_min, y_max = self.Xn.min(), self.Xn.max(), self.Yn.min(), self.Yn.max()
        self.log(f'Number of mesh elements: {self.ne}')
        self.log(f'Number of mesh nodes: {self.nn}')
        self.log(f'Bounding box: [{x_min}, {x_max}] x [{y_min}, {y_max}]')
        self.plot_bbox = x_min, x_max, y_min, y_max
        # self.domain_scale = np.min([x_max - x_min, y_max - y_min]) * 0.05
        # self.log(f'Domain scale: {self.domain_scale}')

        # Creating domain Shapely objects
        self.log('Extracting boundary polygons')
        TOPOLOGY = np.zeros([len(self.mesh.Elements1D()), 3], dtype=int)
        for i, e in enumerate(self.mesh.Elements1D()):
            n1, n2 = [v.nr - 1 for v in e.vertices]
            TOPOLOGY[i, :] = [n1, n2, 1]

        polygons = [[TOPOLOGY[0][0], TOPOLOGY[0][1]]]
        TOPOLOGY[0, 2] = 0

        while np.sum(TOPOLOGY[:, 2]) > 0:
            for ie in range(np.shape(TOPOLOGY)[0]):
                n1, n2, s = TOPOLOGY[ie, :]
                if s == 0:
                    continue
                for ipoly in range(len(polygons)):
                    if n1 == polygons[ipoly][-1]:
                        polygons[ipoly].append(n2)
                        TOPOLOGY[ie, 2] = 0
                        break
                    if n2 == polygons[ipoly][-1]:
                        polygons[ipoly].append(n1)
                        TOPOLOGY[ie, 2] = 0
                        break
                else:
                    continue
                break
            else:
                for ie in range(np.shape(TOPOLOGY)[0]):
                    n1, n2, s = TOPOLOGY[ie, :]
                    if s != 0:
                        polygons.append([n1, n2])
                        TOPOLOGY[ie, 2] = 0
                        break

        self.log(f'Number of boundaries: {len(polygons)}')

        # Separating each domain boundary
        sh_polygons, areas = [], []
        for ipoly, poly in enumerate(polygons):
            xy = zip(self.Xn[poly], self.Yn[poly])
            sh_poly = shg.Polygon(xy)
            sh_polygons.append(sh_poly)
            areas.append(sh_poly.area)

        i_exterior = np.argmax(areas)
        self.domain = sh_polygons[i_exterior]
        self.log(f'Domain area: {self.domain.area}')
        # this is probably usles because
        # self.domain=self.domain.difference(sh_poly)
        for ipoly, sh_poly in enumerate(sh_polygons):

            self.log(f'Poylgon {ipoly + 1}, {"exterior" if ipoly == i_exterior else "interior"},area: {sh_poly.area}')
            if ipoly == i_exterior:
                continue
            assert self.domain.intersection(sh_poly).area > 0.0, 'ERROR! The domain should be fully connected!'
            self.domain = self.domain.difference(sh_poly)

        self.holes = sh_polygons

        # Creating a domain plot using Shapely objects
        self.log('Saving mesh figure')
        x_span = x_max - x_min
        y_span = y_max - y_min
        figsize = 10
        if x_span > y_span:
            y_span = y_span / x_span * figsize
            x_span = figsize
        else:
            x_span = x_span / y_span * figsize
            y_span = figsize
        self.figsize = [x_span, y_span]

        # Plotting the domain polygons
        fig, ax = plt.subplots(figsize=self.figsize)
        for ipoly, poly in enumerate(polygons):
            plt.plot(self.Xn[poly], self.Yn[poly], c=f'C{ipoly}', lw=0.8)
        # domain_patch = PolygonPatch(self.domain, alpha=0.1)
        # ax.add_patch(domain_patch)
        plot_polygon(ax, self.domain, alpha=0.1)
        tr = tri.Triangulation(self.Xn, self.Yn, triangles=self.EL)
        plt.triplot(tr, lw=0.3, c='grey')
        ax.plot(self.AX0, self.AY0, 'k+')
        fig.savefig(self.results_dir + 'mesh.png', dpi=300)
        plt.close(fig)

        # Calculating distances/clearances between polygons
        if np.sum(self.AR) > 0 and np.sum(self.AC) > 0 and len(sh_polygons) > 1:
            self.log('Checking obstacle polygons clearances')
            distance = np.full([len(sh_polygons), len(sh_polygons)], np.nan)
            d_min = np.min(2 * self.AR + 2 * self.AC)
            for ipoly, poly in enumerate(sh_polygons):
                for ipoly2, poly2 in enumerate( sh_polygons):
                    if ipoly2 <= ipoly:
                        continue
                    d = poly.exterior.distance(poly2.exterior)
                    distance[ipoly, ipoly2] = d
                    distance[ipoly2, ipoly] = d

                    if d < d_min:
                        self.log(f' - Critical clearance between obstacle polygons {ipoly} and {ipoly2} ({d=})')

            d_max = np.nanmax(distance)
            d_max = np.max([d_max, 2*d_min])
            self.log(f'{d_min=}, {np.nanmin(distance)=}, {np.nanmax(distance)=}')
            plt.figure(figsize=(15, 15), constrained_layout=True)
            plt.title(f'Minimal distance: {np.nanmin(distance):.3f}')
            plt.imshow(distance, cmap=plt.cm.RdBu,
                       norm=mpl.colors.TwoSlopeNorm(vcenter=d_min,
                                                    vmin=0,
                                                    vmax=d_max))
            plt.colorbar()
            plt.savefig(self.results_dir + 'distances.png', dpi=300)
            if np.nanmin(distance) < d_min:
                self.log('Obstacles clearances are too small! Please check agent settings and the domain geometry!')
                self.log(f'The minimal distance between obstacles: {np.nanmin(distance):.3f}, maximal clearance required by agents: {d_min:.3f}')
                input(' >> Press return to continue')

        self.toc()

    def init_agents(self):
        """Initializes agents positions and parameters."""

        self.tic()
        self.title('Initializing agents')

        assert np.size(self.AX0) == np.size(
            self.AY0), f'The size of agent parameters is not matching ({np.size(self.AX0)} vs {np.size(self.AY0)})'
        assert np.size(self.AX0) == np.size(
            self.AV), f'The size of agent parameters is not matching ({np.size(self.AX0)} vs {np.size(self.AV)})'
        assert np.size(self.AX0) == np.size(
            self.AR), f'The size of agent parameters is not matching ({np.size(self.AX0)} vs {np.size(self.AR)})'
        assert np.size(self.AX0) == np.size(
            self.AD0), f'The size of agent parameters is not matching ({np.size(self.AX0)} vs {np.size(self.AD0)})'
        assert np.size(self.AX0) == np.size(
            self.AC), f'The size of agent parameters is not matching ({np.size(self.AX0)} vs {np.size(self.AC)})'
        assert np.size(self.AX0) == np.size(
            self.AS), f'The size of agent parameters is not matching ({np.size(self.AX0)} vs {np.size(self.AS)})'

        self.an = np.size(self.AX0)

        self.AXY = np.full([self.an, self.tn, 2], np.nan)
        self.AXY[:, 0, 0] = self.AX0
        self.AXY[:, 0, 1] = self.AY0

        self.AD = np.full([self.an, self.tn], np.nan)
        self.AD[:, 0] = self.AD0

        self.AVXY = np.full([self.an, self.tn, 2], np.nan)
        self.AVXY[:, 0, 0] = np.cos(self.AD0) * self.AV
        self.AVXY[:, 0, 1] = np.sin(self.AD0) * self.AV

        self.omega_max = np.zeros(self.an)
        self.omega_max[self.AR > 0] = self.AV[self.AR > 0] / self.AR[self.AR > 0]
        self.omega_max[self.AR == 0] = np.pi / self.dt

        self.col_avoid_plt = [{'boundary_avoidance': False, 'interaction': False} for ia in range(self.an)]

        # Monitoring execution time
        self.execution_time = np.full([3, self.tn], 0.0)

        # Displaying the summary of agents' parameters in the log
        self.log(f'Number of agents: {self.an}')
        self.log(f'AX0: {self.AX0}')
        self.log(f'AY0: {self.AY0}')
        self.log(f'AV: {self.AV}')
        self.log(f'AD0: {self.AD0}')
        self.log(f'AR: {self.AR}')
        self.log(f'AC: {self.AC}')
        self.log(f'AVX0: {self.AVXY[:, 0, 0]}')
        self.log(f'AVY0: {self.AVXY[:, 0, 1]}')
        self.log(f'omega_max: {self.omega_max}')

        self.toc()

    @staticmethod
    def rotation_matrix(angle):
        return np.array([[np.cos(angle), -np.sin(angle)],
                         [np.sin(angle), np.cos(angle)]])

    def init_fem(self):
        """Initializing FEM for ngsolve."""

        self.tic()
        self.title('Initializing FEM')

        # H1-conforming finite element space
        # https://ngsolve.org/docu/nightly/i-tutorials/unit-2.3-hcurlhdiv/hcurlhdiv.html
        # self.fes = ngs.H1(self.fe_mesh, order=3, neuman=[1,2,3,4])
        self.fes = ngs.H1(self.fe_mesh, order=2)
        # self.fes = ngs.H1(self.fe_mesh, order=3)
        # self.fes = ngs.HCurl(self.fe_mesh, order=2)
        # self.fes = ngs.HDiv(self.fe_mesh, order=2)

        # Define trial- and test-functions
        u = self.fes.TrialFunction()
        self.v = self.fes.TestFunction()

        # The bilinear-form 
        self.a = ngs.BilinearForm(self.fes)
        self.a += ngs.SymbolicBFI(self.alpha * ngs.grad(u) * ngs.grad(self.v) + self.beta * u * self.v)

        self.m0 = ngs.GridFunction(self.fes)
        # self.m0.Set(1 + 0 * ngs.x)
        # self.m0.Set(ngs.x**2 + ngs.y**2)
        for i, (x, y) in enumerate(zip(self.Xn, self.Yn)):
            self.m0.vec[i] = self.m0_fun(x, y)
        I_m0 = ngs.Integrate(self.m0, self.fe_mesh) # Normalize
        for i, (x, y) in enumerate(zip(self.Xn, self.Yn)):
            self.m0.vec[i] /= I_m0

        #self.oldc = ngs.GridFunction(self.fes)
        self.c = ngs.GridFunction(self.fes)
        self.c.Set(0 * ngs.x)

        self.dc = ngs.GridFunction(self.fes)


        self.m = ngs.GridFunction(self.fes)
        self.m.Set(self.m0)
        self.accumulate_coverage()
        self.plot_field(self.c, 'c0.png')
        self.plot_field(self.m0, 'm0.png')

        # The right hand side
        # self.f = ngs.LinearForm(self.fes)
        self.ff = ngs.GridFunction(self.fes)

        # Assemble
        self.a.Assemble()
        # f.Assemble()
        self.a_inv = self.a.mat.Inverse(self.fes.FreeDofs())
        self.u = np.zeros(self.nn)

        self.gfu = ngs.GridFunction(self.fes)

        self.f = ngs.LinearForm(self.fes)
        self.f += ngs.SymbolicLFI(self.m / np.average(self.m.vec[:self.nn]) * self.v)
        self.f.Assemble()
        # Compute the solution (using precalculated inverse of system matrix A)
        self.gfu.vec.data = self.a_inv * self.f.vec

        self.save_state(f'{self.results_dir}state_{self.it:06d}.npz')

        self.toc()

    def init_graphics(self):
        """Initializing plotting stuff."""
        self.tic()
        self.title('Initializing graphics')
        # self.plotter = BackgroundPlotter(
        self.plotter = pv.Plotter(
            window_size=[3840, 2160],
            off_screen=True,  # Plots off screen when True. Helpful for saving screenshots without a window popping up.
            line_smoothing=True,
            point_smoothing=True,
            polygon_smoothing=True,
        )
        x_span = self.Xn.max() - self.Xn.min()
        y_span = self.Yn.max() - self.Yn.min()
        # tuple: camera location, focus point, viewup vector
        self.plotter.camera_position = [(self.Xn.min() - 1 * x_span, self.Yn.min() - 1 * y_span, 3.5),
                                        (np.mean(self.Xn), np.mean(self.Yn), 0.8),
                                        (0, 0, 1)]

        # Get the points as a 2D NumPy array (N by 3)
        points = np.c_[self.Xn.reshape(-1), self.Yn.reshape(-1), np.zeros(self.nn)]

        self.pv_mesh1 = pv.UnstructuredGrid({vtk.VTK_TRIANGLE: self.EL}, points)
        self.pv_mesh1['values'] = np.zeros(self.nn)  # self.u.reshape(-1)
        self.plotter.add_mesh(self.pv_mesh1, cmap=cc.cm.fire, show_scalar_bar=False)

        self.pv_mesh2 = self.pv_mesh1.copy()
        self.pv_mesh2.points[:, 2] = 1.6
        self.plotter.add_mesh(self.pv_mesh2, cmap=plt.cm.Purples,
                              opacity=[0.2, 1.0],
                              show_scalar_bar=False)

        self.pv_contours2 = None
        self.pv_path2 = [None] * self.an

        self.plotter.enable_eye_dome_lighting()
        # self.plotter.show()
        # self.plotter.view_isometric()

        self.toc()

        # Plot options
        # self.plot_options = {}
        # self.plot_options['E_min'] = 1e-3
        # self.plot_options['s_orders'] = 5
        # self.plot_options['qiver_size'] = 50

        # self.plot_options['plot_width'] = 19.20
        # self.plot_options['plot_height'] = 10.80
        # self.plot_options['left'] = 0.02
        # self.plot_options['right'] = 0.97
        # self.plot_options['wspace'] = 0.2
        # self.plot_options['hspace'] = 0.1
        # self.plot_options['top'] = 0.97
        # self.plot_options['bottom'] = 0.05

        self.plot_tri = tri.Triangulation(self.Xn, self.Yn, triangles=self.EL)
        # self.plot_frame()

    def solve_trajecotries(self):
        """Main time loop for solving trajectories and their control."""
        self.title('Simulating coverage control')

        if self.it == 0:
            # Runs simulation from the beginning
            self.log(f'Starting new simulation')
            # self.plot_frame()
        else:
            # Continues simulation from given iteration.
            self.log(f'Starting simulation from t:{self.T[self.it]} (it:{self.it})')

        first_it = self.it

        for self.it, t in enumerate(self.T):
            if self.it <= first_it:
                continue

            # Obtaining angular velocity from HEDAC potential field.
            omega = self.hedac_control()

            # Correcting omega to avoid collisions
            self.collision_avoidance_maneuver(omega)

            # Calculate coverage
            self.accumulate_coverage()

            # # Making figures
            # if self.it % 1 == 0:
            #     self.log('Making frame')
            #     self.create_frame()

            # Save state and make animation (each state_save_steps)
            if self.it % self.state_save_steps == 0:
                self.save_state(f'{self.results_dir}state_{self.it:06d}.npz')
                #self.trajectory_analysis()
                self.create_frame()
                # self.plot_field(self.m, f'm_{self.it}.png')
                #self.make_animation()

            # input(' >> Press return to continue.')

    def accumulate_coverage(self):

        self.tic()
        # Calculates increase of coverage
        #c_increase = ngs.GridFunction(self.fes)

        dc = np.zeros([self.nn])
        for ia, (ax, ay) in enumerate(self.AXY[:, self.it, :]):
            # c_increase += self.AA[ia] * ngs.exp(- ((ngs.x - ax) ** 2 + (ngs.y - ay) ** 2) / (2 * self.AS[ia] ** 2))

            mask = (self.Xn - ax)**2 + (self.Yn - ay)**2 < self.AS[ia] ** 2

            x = self.Xn[mask]
            y = self.Yn[mask]
            theta = self.AD[ia, self.it]
            # print(f'{theta=}')
            #theta = np.arctan2(self.AVXY[ia, self.it, 0], -self.AVXY[ia, self.it, 0])
            rx = (x - ax) * np.cos(theta) + (y - ay) * np.sin(theta)
            ry = (x - ax) * np.sin(theta) - (y - ay) * np.cos(theta)

            _phi = self.phi_fun(ia, rx, ry)
            dc[mask] += _phi
        # input('?')

        dc *= self.dt

        for i in range(self.nn):
            self.c.vec[i] += dc[i]


        # Accumulates increase to coverage c
        # self.dc.Set(c_increase)
        # self.oldc.Set(self.c)
        # self.c.Set(self.oldc + self.dc * self.dt)

        # Calculates the "uncovered" field which is the source in the heat equation.
        self.m.Set(self.m0 * ngs.exp(-self.c))

        self.eta[self.it] = 1 - ngs.Integrate(self.m, self.fe_mesh)
        #print(self.eta[:self.it+1])
        # input('?')

        # Measuring time
        self.execution_time[0, self.it] += self.toc()


    def hedac_control(self):
        """Set ups right hand terms for HEDAC PDE and calculates potential field u.
        Returns vector of angular velocities corresponding to directions determined by grad(u)."""

        self.tic()
        self.title(f'HEDAC step {self.it}/{self.tn - 1} t={self.T[self.it]:.3f}')

        # Set the right-hand side of the heat equation
        self.f = ngs.LinearForm(self.fes)
        self.f += ngs.SymbolicLFI(self.m / np.average(self.m.vec[:self.nn]) * self.v)
        self.f.Assemble()

        # Compute the solution (using precalculated inverse of system matrix A)
        self.gfu.vec.data = self.a_inv * self.f.vec

        # print(np.min(self.m0.vec), np.max(self.m0.vec))
        # print(np.min(self.m.vec), np.max(self.m.vec))
        # print(np.min(self.c.vec), np.max(self.c.vec))
        # print(np.min(self.gfu.vec), np.max(self.gfu.vec))

        # Obtaining the gradient function
        self.ggfu = ngs.grad(self.gfu)

        # Calculating the gradient at each agents' location
        omega = np.zeros([self.an])
        for ia in range(self.an):
            self.log(f'Agent {ia}')
            self.log(f' x: [{self.AXY[ia, self.it - 1, 0]}, {self.AXY[ia, self.it - 1, 1]}]')
            vxy = self.ggfu(self.fe_mesh(self.AXY[ia, self.it - 1, 0], self.AXY[ia, self.it - 1, 1]))
            vxy /= np.linalg.norm(vxy)  # Direction is a unit vector
            self.log(f' v: [{vxy[0]}, {vxy[1]}]')
            # Calculating angular velocity which corresponds to HEADC's direction
            omega[ia] = (np.arctan2(vxy[1], vxy[0]) - self.AD[ia, self.it - 1])
            # self.log(f' omega: {omega[ia]}')

        omega = np.mod(omega, 2 * np.pi)
        omega[omega > np.pi] = omega[omega > np.pi] - 2 * np.pi

        omega /= self.dt
        self.log(f' omega: {omega}')

        # Dubins constraint
        # Correcting omega to be within interval [-omega_max, omega_max]
        omega = np.clip(omega, -self.omega_max, self.omega_max)

        # Obtaining the potential in all nodes and scaling the potential field to [0, 1]
        Z = np.zeros(self.nn)
        for i, node in enumerate(self.nodes):
            Z[i] = self.gfu.vec[i]
        self.u = Z.real / np.max(Z.real)

        # Measuring time
        self.execution_time[1, self.it] += self.toc()

        return omega

    def swarm_partitioning(self):
        """Partitions the swarm of agent to independent non-interacting groups. Returns a list of lists representing the
        agent indices grouped by distances between agents."""

        partitioning = {'groups': []}
        agents = list(range(self.an))
        ig = 0
        while len(agents):
            ia1 = agents[0]
            group = [ia1]
            agents.remove(ia1)
            partitioning[ia1] = ig

            current = 0
            while True:
                if current > len(group) - 1:
                    break
                ia1 = group[current]
                for ia2 in np.copy(agents):
                    d = np.linalg.norm(self.AXY[ia1, self.it - 1, :] - self.AXY[ia2, self.it - 1, :])
                    if d <= 2 * self.AR[ia1] + self.AC[ia1] + self.AV[ia1] * self.dt * 1 + \
                            2 * self.AR[ia2] + self.AC[ia2] + self.AV[ia2] * self.dt * 1:
                        group.append(ia2)
                        agents.remove(ia2)
                        partitioning[ia2] = ig
                        # break
                current += 1
            partitioning['groups'].append(group)
            ig += 1

        return partitioning

    def collision_area(self, group, omega, update_state=False, make_plots=False):
        """Calculates the area for all collision which would happen in the next time step (dt) if turning based on
        omega is applied. Returns new coordinates for given turning angular velocity and the vector containing
        collision areas for each agent. The function can update the state of the search (agents positions and
        directions) if argument update_state is set to True. Argument make_plots can visualize the collisions and it is
        mainly used for debugging."""

        assert np.size(group) == np.size(omega), 'group and omega vectors size mismatch!'
        R = np.zeros_like(group, dtype=float)
        XY0 = np.copy(self.AXY[group, self.it - 1, :])  # Current position
        XY = np.copy(self.AXY[group, self.it - 1, :])  # New position (to be calculated)
        THETA = self.AD[group, self.it - 1] + omega * self.dt  # New heading angle
        VXY = np.array([np.cos(THETA), np.sin(THETA)]).T  # New heading (unit) direction
        if make_plots:
            print(f'theta: {THETA}, vxy: {VXY}')
            print(f'{omega=}')

        for i, ia in enumerate(group):

            if np.abs(omega[i]) < 1e-6:
                R[i] = 1e9 * self.AR[ia]
            else:
                R[i] = self.AV[ia] / np.abs(omega[i])

            # print(f'{ia=} {omega[i]=} {R[i]=}')
            if R[i] < 100 * self.AR[ia]:
                # If curvature radius is small enough (trajectory is not straight)
                # Old (tangential) direction component
                vxy_t0 = self.AVXY[ia, self.it - 1, :] / np.linalg.norm(self.AVXY[ia, self.it - 1, :])
                vxy_t = np.array([np.cos(self.AD[ia, self.it - 1]), np.sin(self.AD[ia, self.it - 1])])  # * self.AV[ia]

                # Normal (to current) direction component
                vxy_n = np.matmul(self.rotation_matrix(np.deg2rad(90 * np.sign(omega[i]))), vxy_t)

                # Weights
                w_t = R[i] * np.sin(np.abs(omega[i]) * self.dt)
                w_n = R[i] * (1 - np.cos(omega[i] * self.dt))
                # New position
                vxy = vxy_t * w_t + vxy_n * w_n
                XY[i, :] += vxy
                if make_plots:
                    print(f'{R=}')
                    print(f'{vxy_t0=} {vxy_t=}')
                    print(f'{vxy_n=}')
                    print(f'{w_t=} {w_n=}')
                    print(f'{vxy=}')
                    print(f'{VXY[i]=}')

            else:
                # Trajectory is a straight line
                # New position
                XY[i, :] += VXY[i, :] * self.AV[ia] * self.dt

        # Updating the state of agents with calculated new positions, directions and velocities.
        if update_state:
            self.AD[group, self.it] = np.copy(THETA)
            self.AXY[group, self.it, :] = np.copy(XY)

            self.log('\nUpdating agent positions')
            for i, ia in enumerate(group):
                self.AVXY[ia, self.it, :] = [self.AV[ia] * VXY[i, 0],
                                             self.AV[ia] * VXY[i, 1]]

                self.log(f'Agent {ia}')
                self.log(f' x: [{self.AXY[ia, self.it - 1, 0]}, {self.AXY[ia, self.it - 1, 1]}]')
                self.log(f' v: [{self.AVXY[ia, self.it, 0]}, {self.AVXY[ia, self.it, 1]}]')

            # print(f'{R=}')
            # input(' >> Press return to continue.')

        if self.AR[group].sum() + self.AC[group].sum() == 0:
            # Dubins constraint is not used
            Ac = np.zeros_like(group, dtype=np.float)
            for i, ia in enumerate(group):
                step_line = shg.LineString([XY0[i, :], XY[i, :]])
                Ac[i] = step_line.difference(self.domain).length
            return XY, Ac

        # Calculate collision circles
        circles_a = []
        circles_b = []
        for i, ia in enumerate(group):
            vxy_a = np.matmul(self.rotation_matrix(np.deg2rad(-90)), VXY[i, :])
            vxy_b = np.matmul(self.rotation_matrix(np.deg2rad(90)), VXY[i, :])
            xya = XY[i, :] + vxy_a * self.AR[ia]
            xyb = XY[i, :] + vxy_b * self.AR[ia]

            # Collision circles
            circles_a.append(shg.Point(xya).buffer(self.AR[ia] + self.AC[ia]))
            circles_b.append(shg.Point(xyb).buffer(self.AR[ia] + self.AC[ia]))

        # Area of collisions with the boundary (for + and - escape circles of all agents)
        Ab = np.zeros(np.size(group) * 2)
        # Area of collisions between all agents' escape circles
        Aa = np.zeros([np.size(group) * 2, np.size(group) * 2])

        for i, ia in enumerate(group):
            # Collision area with the border
            Ab[i * 2 + 0] += circles_a[i].difference(self.domain).area
            Ab[i * 2 + 1] += circles_b[i].difference(self.domain).area

        for i, ia in enumerate(group):
            # Collision area with other agents
            for i2, ia2 in enumerate(group):
                if ia == ia2:
                    continue
                Aa[i * 2 + 0, i2 * 2 + 0] += circles_a[i].intersection(circles_a[i2]).area
                Aa[i * 2 + 0, i2 * 2 + 1] += circles_a[i].intersection(circles_b[i2]).area
                Aa[i * 2 + 1, i2 * 2 + 0] += circles_b[i].intersection(circles_a[i2]).area
                Aa[i * 2 + 1, i2 * 2 + 1] += circles_b[i].intersection(circles_b[i2]).area

        # All combinations of possible escape routes
        combinations = list(itertools.product([0, 1], repeat=np.size(group)))
        nc = len(combinations)
        ab = np.zeros([nc, np.size(group)])  # Area of boundary collision for each combination and agent
        aa = np.zeros([nc, np.size(group)])  # Area of collision between agents for each combination and agent

        for ic, c in enumerate(combinations):
            # Indices of the combination c for accessing Aa and Ab matrices
            ix = np.arange(np.size(group)) * 2 + np.array(c)
            #print(f'{ix.shape=}')
            ab[ic, :] = Ab[ix]
            # aaa = Aa[np.meshgrid(ix, ix)] # this worked for old version of numpy
            aaa = Aa[ix, :][:, ix]
            #print(f'{np.meshgrid(ix, ix).shape=}')
            aa[ic, :] += np.sum(aaa, 1)

        # Total collision area is sum of the areas of boundary collision and collisions between agents.
        ac = ab + aa
        # The best possible combination of escape routes is the one with minimal collision area
        best = np.argmin(np.sum(ac, 1))
        Ac = ac[best]

        if make_plots:

            # Making matrix plots for matrices aa, ab, ac and Aa
            for m, name in zip([aa, ab, ac, Aa], 'aa ab ac Aa'.split()):
                fig, ax = plt.subplots(figsize=(10, 10))
                ax.imshow(m)
                fig.savefig(f'{self.results_dir}dubins_{name}_it_{self.it:06d}_{group}.png')
                plt.close(fig)

            for i1, ia1 in enumerate(group):
                fig, ax = plt.subplots(figsize=(15, 15))

                # Plot domain boundaries
                domain_patch = PolygonPatch(self.domain, fc='grey', alpha=0.1, lw=2, ec='k', antialiased=True)
                ax.axis('equal')
                ax.add_patch(domain_patch)

                for i2, (ia2, xy0, xy, th, clr) in enumerate(zip(group, XY0, XY, THETA, 'r b g'.split())):
                    vxy0 = self.AVXY[ia2, self.it - 1, :]
                    vxy = np.array([np.cos(th), np.sin(th)])
                    # Unit direction towards escape circles
                    vxy0_a = np.matmul(self.rotation_matrix(np.deg2rad(90)), vxy0)
                    vxy0_b = np.matmul(self.rotation_matrix(np.deg2rad(-90)), vxy0)
                    vxy_a = np.matmul(self.rotation_matrix(np.deg2rad(90)), vxy)
                    vxy_b = np.matmul(self.rotation_matrix(np.deg2rad(-90)), vxy)

                    # Collision radius
                    cr = (self.AR[ia2] + self.AC[ia2])
                    # Centers of escape circles
                    xy0_a = xy0 + vxy0_a * self.AR[ia2]
                    xy0_b = xy0 + vxy0_b * self.AR[ia2]
                    xy_a = xy + vxy_a * self.AR[ia2]
                    xy_b = xy + vxy_b * self.AR[ia2]

                    ax.plot(xy0[0], xy0[1], '.', c=clr)  # Old position
                    ax.plot(xy[0], xy[1], 'o', c=clr)  # New position
                    ax.plot(xy0_a[0], xy0_a[1], 'x', c=clr)  # Center of escape circle a
                    ax.plot(xy0_b[0], xy0_b[1], 'x', c=clr)  # Center of escape circle b
                    ax.plot(xy_a[0], xy_a[1], '+', c=clr)  # Center of escape circle a
                    ax.plot(xy_b[0], xy_b[1], '+', c=clr)  # Center of escape circle b
                    ax.text(xy[0], xy[1], f'{ia2}')  # Adding agent index to the plot

                    # Plotting escape circles
                    circ = plt.Circle((xy0_a[0], xy0_a[1]), cr, fc=clr, ls='--', lw=0.5, ec=clr, alpha=0.1)
                    ax.add_artist(circ)
                    circ = plt.Circle((xy0_b[0], xy0_b[1]), cr, fc=clr, ls='--', lw=0.5, ec=clr, alpha=0.1)
                    ax.add_artist(circ)

                    circ = plt.Circle((xy_a[0], xy_a[1]), cr, fc=clr, ls='--', lw=0.5, ec=clr, alpha=0.1)
                    ax.add_artist(circ)
                    circ = plt.Circle((xy_b[0], xy_b[1]), cr, fc=clr, ls='--', lw=0.5, ec=clr, alpha=0.1)
                    ax.add_artist(circ)

                    # Plotting old direction vector
                    ax.plot([xy0[0], xy0[0] + vxy0[0]],
                            [xy0[1], xy0[1] + vxy0[1]],
                            c=clr, ls=':', lw=0.5)

                    # Plotting the new direction vector
                    ax.plot([xy[0], xy[0] + vxy[0]],
                            [xy[1], xy[1] + vxy[1]],
                            c=clr, ls='-', lw=0.5)

                    ax.plot(self.AXY[ia1, :self.it, 0], self.AXY[ia1, :self.it, 1], c='grey', lw=0.5)

                # Setting the plot boundaries
                sc = 2
                ax.set_xlim(XY[i1, 0] - sc * (self.AR[ia1] + self.AC[ia1]),
                            XY[i1, 0] + sc * (self.AR[ia1] + self.AC[ia1]))
                ax.set_ylim(XY[i1, 1] - sc * (self.AR[ia1] + self.AC[ia1]),
                            XY[i1, 1] + sc * (self.AR[ia1] + self.AC[ia1]))

                # Saving and closing the figure
                fig.savefig(f'{self.results_dir}dubins_ia{ia1}_it_{self.it:06d}.png')
                plt.close(fig)

        return XY, Ac

    def collision_avoidance_maneuver(self, omega_u):
        """Correcting the angular velocities omega_u via the optimization."""

        self.tic()
        self.partitioning = self.swarm_partitioning()
        print(f'\nSwarm partitioning: {self.partitioning["groups"]}')
        # if len(partitions) != self.an:
        #    input(' >> Press return to continue.')

        for group in self.partitioning['groups']:
            print(f'\nGroup: {group}')
            # Calculates the collision area for initial turning velocities omega_u.
            XY, Ac = self.collision_area(group, omega_u[group], make_plots=False)
            print(omega_u, Ac)

            if np.sum(Ac) <= 0:
                # There is no need for the correction if there is no collision
                self.collision_area(group, omega_u[group], update_state=True, make_plots=False)
                # Setting flags for marking collision avoidance maneuver in visualizations to False
                for i, ia in enumerate(group):
                    self.col_avoid_plt[ia]['boundary_avoidance'] = False
                # All angular velocities in group remain unmodified
                continue

            # Setting flags for marking collision avoidance maneuver in visualizations
            for i, ia in enumerate(group):
                self.col_avoid_plt[ia]['boundary_avoidance'] = Ac[i] > 0

            """
            Solve optimization problem:
            - find omegas
            - which minimizes (omegas - omegas_hedac)^2
            - with constraint sum(A_collsion) <= 0
            """

            def eval_fun(_omega):
                # Evaluation function which returns objective (to be minimized)
                # and constraint (which needs to be less or equal to zero)
                _XY, _Ac = self.collision_area(group, _omega)
                omega_diff = ((omega_u[group] - _omega) / self.omega_max[group]) ** 2
                o1 = omega_diff.sum()
                c1 = np.sum(_Ac)
                return o1, c1

            # Check the bounds (which area escape routes) if constraints are violated.
            # At least one bound should be collision free.
            omegas_corrected = omega_u[group].copy()
            # agents_to_optimize = np.full(np.size(group), True, dtype=np.bool)
            print(f'Omega bounds check')
            print(f'{omegas_corrected=}')
            combinations = list(itertools.product([-1, 1], repeat=np.size(group)))
            print(f'Checking {len(combinations)} combinations of escape routes')
            best_obj = 0
            best_constr = np.inf
            for c in combinations:
                _omega = np.array(c) * self.omega_max[group]
                obj, constr = eval_fun(_omega)
                self.log(f' omega: {_omega}, obj: {obj}, constr: {constr}')
                if constr < best_constr or (constr == 0 and obj < best_obj):
                    omegas_corrected = _omega
                    best_obj = obj
                    best_constr = constr
            # Check if some direction can remain the same as the one obtained by the potential field
            # agents_to_optimize = np.full(np.size(group), True, dtype=np.bool)
            for i, ia in enumerate(group):
                _omega = omegas_corrected.copy()
                _omega[i] = omega_u[ia]
                obj, constr = eval_fun(_omega)
                if constr <= 0:
                    omegas_corrected[i] = omega_u[ia]
                    best_obj = obj
                    best_constr = constr
            print(f'{omegas_corrected=}')
            print(f'{best_obj=}, {best_constr=}')
            if best_constr > 0:
                self.collision_area(group, omegas_corrected, update_state=False, make_plots=True)
                input(' >> Press return to continue.')

            # Plot of objectives and constraints (1D function which is to be minimized)
            # O = np.linspace(-self.omega_max[ia], self.omega_max[ia], 1001)
            # F = np.zeros_like(O)
            # C1 = np.zeros_like(O)
            # for ia in range(self.an):
            #     if Ac[ia] == 0:
            #         continue
            #     print(f'Plot of objectives and constraints for single agent {ia=}')
            #     omega_plt = omegas_corrected.copy()
            #     for i, o in enumerate(O):
            #         omega_plt[ia] = o
            #         F[i], C1[i] = eval_fun(omega_plt, ia)
            #         # print(f'{ia=} {o=} {omega_plt} o:{F[i]} c:{C1[i]}')
            #     plt.figure(constrained_layout=True)
            #     plt.plot(O, F, label='Objective', lw=1)
            #     plt.plot(O, C1, label='Constraint 1', lw=1)
            #     plt.axhline(c='k', lw=0.5)
            #     plt.axvline(omega_u[ia], c='b', lw=0.5)
            #     # plt.plot(O, C2, label='Constraint 2')
            #     plt.xlabel(r'$\omega$')
            #     plt.yscale('symlog', linthresh=1e-5)
            #     plt.legend()
            #     plt.gca().set_facecolor('aliceblue')
            #     plt.savefig(f'{self.results_dir}optimization_{self.it:06d}_{ia}.png', transparent=False)
            #     plt.close()
            #     # input(' >> Press return to continue')

            # Golden section search procedure; repetitive for each agent (search dimension)
            rho = (3 - np.sqrt(5)) / 2
            max_iter = 20
            total_iter = 40
            while np.sum(Ac) > 0 and max_iter <= total_iter:

                XY, Ac = self.collision_area(group, omegas_corrected)
                for i, ia in enumerate(group):

                    print(f'\nGolden search for agent {ia}')
                    omegas = omegas_corrected.copy()

                    # Left and right bounds
                    a, b = -self.omega_max[ia], self.omega_max[ia]
                    omegas[i] = a
                    fa, ca = eval_fun(omegas)
                    omegas[i] = b
                    fb, cb = eval_fun(omegas)

                    # Two center points
                    c = a + rho * (b - a)
                    omegas[i] = c
                    fc, cc = eval_fun(omegas)
                    d = b - rho * (b - a)
                    omegas[i] = d
                    fd, cd = eval_fun(omegas)

                    for opt_it in range(max_iter):

                        if cc < cd or (((cc <= 0 and cd <= 0) or cc == cd) and fc < fd):
                            # Discarding point b (point a remains the same)
                            b = d
                            d = c
                            c = a + rho * (b - a)
                            fb, fd = fd, fc
                            cb, cd = cd, cc
                            omegas[i] = c
                            fc, cc = eval_fun(omegas)

                        else:
                            # Discarding point a (point b remains the same)
                            a = c
                            c = d
                            d = b - rho * (b - a)
                            fa, fc = fc, fd
                            ca, cc = cc, cd
                            omegas[i] = d
                            fd, cd = eval_fun(omegas)

                        # Updating the optimum based on central points
                        x_min = c if cc < cd or (cc == 0 and cd == 0 and fc < fd) else d
                        f_min = fc if cc < cd or (cc == 0 and cd == 0 and fc < fd) else fd
                        c_min = cc if cc < cd or (cc == 0 and cd == 0 and fc < fd) else cd

                    # Check if the bounds are better than found optimum
                    a, b = -self.omega_max[ia], self.omega_max[ia]
                    omegas[i] = a
                    fa, ca = eval_fun(omegas)
                    omegas[i] = b
                    fb, cb = eval_fun(omegas)
                    self.log(f'Checking bounds {a}, {fa}, {ca}')
                    if ca < c_min or (ca == 0 and c_min == 0 and fa < f_min):
                        x_min = a
                        f_min = fa
                        c_min = ca
                    self.log(f'Checking bounds {b}, {fb}, {cb}')
                    if cb < c_min or (cb == 0 and c_min == 0 and fb < f_min):
                        x_min = b
                        f_min = fb
                        c_min = cb

                    # Setting the angular velocity of ia-th agent to the found optimum
                    omegas_corrected[i] = x_min
                    XY, Ac = self.collision_area(group, omegas_corrected)
                    print(f'Best solution found after {opt_it} iterations ia:{ia} x:{x_min} f:{f_min} c:{c_min}')
                    print(f'{omegas_corrected=}')
                    # input(' >> Press return to continue.')

                max_iter += 10  # If constraints are not satisfied raise number of golden section search iterations
                XY, Ac = self.collision_area(group, omegas_corrected)
                print(f'it:{self.it} i:{opt_it} x:{x_min} f:{f_min} c:{c_min}')
                print(f'{Ac=}')

            print('\nOptimization done!')
            print(f'i:{opt_it} x:{x_min} f:{f_min} c:{c_min}')
            print(f'{omega_u=}')
            print(f'{omegas_corrected=}')
            print(f'{Ac=}')
            if Ac.sum() > 0:
                input(' >> Press return to continue.')

            # Updating the state of agents based on new angular velocities
            self.collision_area(group, omegas_corrected, update_state=True, make_plots=False)

            self.execution_time[2, self.it] += self.toc()

    def plot_field(self, F, filename, ia=None):
        """Utility function for plotting ngsolve fields."""
        plt.figure(figsize=self.figsize)
        Z = np.zeros(self.nn)
        for i, node in enumerate(self.nodes):
            Z[i] = F.vec[i]

        tr = tri.Triangulation(self.Xn, self.Yn, triangles=self.EL)
        # plt.tricontourf(tr, Z.real, levels=np.linspace(0, np.max(Z.real), 51))
        plt.tricontourf(tr, Z.real, 51)
        plt.colorbar()

        plt.axis('equal')
        plt.axis('off')
        plt.savefig(self.results_dir + filename)
        plt.close()

    def make_animation(self):
        """Calling ffmpeg for making video files from created frames."""
        self.tic()
        self.title('Making animation')
        self.log(f'CWD: {self.results_dir}')

        self.log(f'Animation: hedac-fem_{self.case_name}_animation.mp4')
        p = subprocess.Popen(
            f'/usr/bin/ffmpeg -r 30 -i search_%06d.png -c:v libx264 -r 30 -pix_fmt yuv420p hedac-fem_{self.case_name}_animation.mp4 -y'.split(),
            cwd=self.results_dir,
            stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.communicate()
        self.log(str(p.stdout))
        self.log(str(p.stderr))

        self.log(f'Animation: hedac-fem_{self.case_name}_animation.mp4')
        p = subprocess.Popen(
            f'/usr/bin/ffmpeg -r 30 -i frame_%06d.png -c:v libx264 -r 30 -pix_fmt yuv420p hedac-fem_{self.case_name}_surveying_animation.mp4 -y'.split(),
            cwd=self.results_dir,
            stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.communicate()
        self.log(str(p.stdout))
        self.log(str(p.stderr))

        self.toc()

    def recreate_frames(self):

        self.title('Recreating animation frames')

        for self.it, t in enumerate(self.T):
            self.load_state()
            self.create_frame()

        self.make_animation()

    def create_frame(self):

        dpi=160
        # Ultra-high-definition television (UHDTV): 4K UHD (3840 x 2160 progressive scan)
        fig = plt.figure(figsize=(3840 / dpi, 2160 / dpi), dpi=dpi)
        minx, miny, maxx, maxy = self.domain.bounds
        spanx = maxx - minx
        spany = maxy - miny
        # print(f'{spanx=}, {spany=}')
        # input(' >> Press return to continue.')

        w = np.min([2160 / spany * spanx, 3840 * 0.65])
        h = w / spany * spanx
        wr = w / 3840 + 0.05
        #print(w, h, wr)
        # input(' >> Press return to continue.')

        gs = gridspec.GridSpec(4, 2,
                               width_ratios=[wr, 1 - wr],
                               height_ratios=[1, 1, 2, 1],
                               left=0.02,
                               right=0.98,
                               bottom=0.04,
                               top=0.90,
                               hspace=0.1,
                               wspace=0.15,
                               )

        ax1 = plt.subplot(gs[:, 0])
        ax2 = plt.subplot(gs[0, 1])
        ax3 = plt.subplot(gs[1, 1], sharex=ax2)
        ax4 = plt.subplot(gs[2, 1], sharex=ax2)
        ax5 = plt.subplot(gs[3, 1], sharex=ax2)

        self.plot_situation(ax1)
        self.trajectory_analysis([ax2, ax3, ax4, ax5])

        fig.align_labels()
        plt.figtext(0.01, 0.96, 'Constrained multi-agent ergodic area surveying control based on finite element approximation of the potential field\n' +
                    'S. Ivić, B. Crnković, A. Sikirica, (2021)',
                    fontsize=16)
        plt.figtext(0.01, 0.93, f'{self.plot_options["case_title"]}',
                    fontsize=16,
                    weight='bold',
                    )

        plt.savefig(f'{self.results_dir}frame_{self.it:06d}.png')  # , dpi=100)
        plt.close(fig)

    def plot_situation(self, axes=None, gradient_scale=None, quiver_n=None, plot_scale=True):

        self.title('Plotting frame')

        if axes:
            ax1 = axes
        else:
            fig = plt.figure(
                figsize=(self.plot_options['plot_width'], self.plot_options['plot_height']))  # Full HD resolution

            # plt.title('t=%.4f' % self.T[self.it])

            gs1 = gridspec.GridSpec(3, 4)
            gs1.update(left=self.plot_options['left'],
                       right=self.plot_options['right'],
                       wspace=self.plot_options['wspace'],
                       hspace=self.plot_options['hspace'],
                       top=self.plot_options['top'],
                       bottom=self.plot_options['bottom'])
            ax1 = plt.subplot(gs1[0:3, 0:3])

        # Contour plot of potential u
        # ax1.tricontourf(self.plot_tri, self.u, 50, cmap='coolwarm')
        # ax1.tricontourf(self.plot_tri, self.u, 50, cmap=plt.cm.YlOrBr)

        m = np.zeros(self.nn)
        for i, node in enumerate(self.nodes):
            m[i] = self.m.vec[i]
        cntr = ax1.tricontourf(self.plot_tri, m, 50, cmap=plt.cm.summer_r)

        # Draw a quiver plot of the potential gradient
        if gradient_scale:
            x_min, x_max, y_min, y_max = self.plot_bbox
            x_span = x_max - x_min
            y_span = y_max - y_min
            if quiver_n:
                n = quiver_n
            else:
                n = 120
            nx, ny = int(np.min([n, n * x_span / y_span])), int(np.min([n, n * y_span / x_span]))
            x = np.linspace(x_min, x_max, nx)
            y = np.linspace(y_min, y_max, ny)
            x, y = np.meshgrid(x, y)
            ux = np.zeros_like(x) * np.nan
            uy = np.zeros_like(x) * np.nan
            # print(nx, ny)
            domain_scale = np.min([x_max - x_min, y_max - y_min])
            for ix in range(nx):
                for iy in range(ny):
                    # print(ix, iy)
                    _x, _y = x[iy, ix], y[iy, ix]
                    if self.domain.contains(shg.Point(_x, _y)):
                        ux[iy, ix], uy[iy, ix] = self.ggfu(self.fe_mesh(_x, _y))
            um = np.sqrt(ux ** 2 + uy ** 2)
            # print(np.nanmin(ux), np.nanmax(ux),
            #       np.nanmin(uy), np.nanmax(uy),
            #       np.nanmin(um), np.nanmax(um))
            scale = 0.5
            ux /= um ** scale
            uy /= um ** scale
            qn = 1 #int(np.floor(np.size(x) / 50))
            ax1.quiver(x[::qn, ::qn], y[::qn, ::qn],
                       ux[::qn, ::qn], uy[::qn, ::qn],
                       units='inches',
                       scale=gradient_scale,
                       scale_units='inches',
                       minlength=0.2, headwidth=2)

        # Plot boundaries
        plot_polygon(ax1, self.domain, fc='none', alpha=1, lw=2, ec='#4f1319ff', antialiased=True)
        # domain_patch = PolygonPatch(self.domain, fc='none', alpha=1, lw=2, ec='#4f1319ff', antialiased=True)
        # ax1.add_patch(domain_patch)
        if self.obstacles:
            plot_polygon(ax1, self.obstacles, fc='#c8beb7ff', alpha=1, lw=1, ec='#4f1319ff',
                                           antialiased=True)
            # obstacles_patch = PolygonPatch(self.obstacles, fc='#c8beb7ff', alpha=1, lw=1, ec='#4f1319ff',
            #                                antialiased=True)
            # ax1.add_patch(obstacles_patch)

        # Agent marker local coordinates
        amxy = np.array([[0, 0],
                         [-1, -2],
                         [4, 0],
                         [-1, 2]]) * self.plot_options['ms']

        for ia in range(self.an):
            # create line plot including an outline (stroke) using path_effects
            ax1.plot(self.AXY[ia, :self.it + 1, 0], self.AXY[ia, :self.it + 1, 1],
                     c='darkblue', lw=1, zorder=1.5,
                     path_effects=[pe.Stroke(linewidth=1.2, foreground='k'), pe.Normal()])

        omega = (self.AD[:, self.it] - self.AD[:, self.it - 1]) / self.dt
        AXY1 = self.collision_area(np.arange(self.an), omega)
        for ia in range(self.an):
            xy0 = self.AXY[ia, self.it - 1, :]
            xy1 = self.AXY[ia, self.it, :]
            theta = self.AD[ia, self.it]
            vxy = np.array([np.cos(theta), np.sin(theta)])
            vxy_a = np.matmul(self.rotation_matrix(np.deg2rad(90)), vxy)
            vxy_b = np.matmul(self.rotation_matrix(np.deg2rad(-90)), vxy)
            # Collision radius
            # cr = (ar + vm * self.dt) * 1.1
            cr = (self.AR[ia] + self.AC[ia])
            xya = xy1 + vxy_a * self.AR[ia]
            xyb = xy1 + vxy_b * self.AR[ia]

            circ = plt.Circle((xya[0], xya[1]), cr, fc='w', ls='-', lw=0.5, alpha=0.4,
                              ec='red' if self.col_avoid_plt[ia]['boundary_avoidance'] else 'grey')
            ax1.add_artist(circ)
            circ = plt.Circle((xyb[0], xyb[1]), cr, fc='w', ls='-', lw=0.5, alpha=0.4,
                              ec='red' if self.col_avoid_plt[ia]['boundary_avoidance'] else 'grey')
            ax1.add_artist(circ)
            circ = plt.Circle(self.AXY[ia, self.it, :], self.AC[ia], fc='none',
                              # alpha=0.1,
                              ls='-',
                              lw=1 if self.col_avoid_plt[ia]['interaction'] else 0.5,
                              ec='red' if self.col_avoid_plt[ia]['interaction'] else 'k')
            ax1.add_artist(circ)

            # Plot agent marker
            theta = self.AD[ia, self.it]  # np.arctan2(self.bavoid[ia]['vxy'][1], self.bavoid[ia]['vxy'][0])
            amxy_r = np.matmul(self.rotation_matrix(theta), amxy.T)
            amarker = shg.polygon.Polygon(self.AXY[ia, self.it, :] + amxy_r.T)
            plot_polygon(ax1, amarker, fc='steelblue', alpha=1, lw=0.6, ec='k',
                                       antialiased=True, zorder=2)
            # ax1.add_patch(PolygonPatch(amarker, fc='steelblue', alpha=1, lw=0.6, ec='k',
            #                            antialiased=True, zorder=2))
            # ax1.text(self.AXY[ia, self.it, 0], self.AXY[ia, self.it, 1], f'{ia}', c='lightblue')

        # Swarm partitioning
        for group in self.partitioning['groups']:
            for i1, ia1 in enumerate(group):
                for ia2 in group[i1 + 1:]:
                    ax1.plot([self.AXY[ia1, self.it, 0], self.AXY[ia2, self.it, 0]],
                             [self.AXY[ia1, self.it, 1], self.AXY[ia2, self.it, 1]],
                             color='red',
                             ls=':',
                             lw=2,
                             )

        x_min, x_max, y_min, y_max = self.Xn.min(), self.Xn.max(), self.Yn.min(), self.Yn.max()
        hx = (x_max - x_min) * 0.005
        hy = (y_max - y_min) * 0.005
        ax1.axis('equal')
        ax1.set_xlim(x_min - hx, x_max + hx)
        ax1.set_ylim(y_min - hy, y_max + hy)
        # ax1.autoscale(enable=False)
        ax1.axis('off')

        # Draw scale
        if plot_scale:
            for i, fc in enumerate(['k', 'w', 'none']):
                x = self.plot_options['scale_xy'][0] + i * self.plot_options['scale_w'] #+ 10 * hx
                y = self.plot_options['scale_xy'][1] + 6 * hy
                ax1.text(x,
                         y - 2.5 * hy,
                         f"{i * self.plot_options['scale_w']}",
                         ha='center', va='top')
                ax1.plot([x]*2, [y, y - 1.5 * hy], c='k', lw=0.5)
                if i < 2:
                    scale = plt.Rectangle((x, y),
                                          self.plot_options['scale_w'], 2 * hy,
                                          fc=fc, ls='-', lw=0.5, ec='k', zorder=1000)
                    ax1.add_patch(scale)
                # print(x, y, self.plot_options['scale_w'], hy)

            ax1.text(self.plot_options['scale_xy'][0] + self.plot_options['scale_units'][1] * self.plot_options['scale_w'],
                     y - 2.5 * hy,
                     self.plot_options['scale_units'][0],
                     ha='left', va='top')

        if not axes:
            plt.savefig(f'{self.results_dir}search_{self.it:06d}.png')  # , dpi=100)
            plt.close()

    def plot_3d(self):

        import vtk
        import colorcet as cc
        import pyvista as pv
        from pyvistaqt import BackgroundPlotter
        # import cmocean

        self.tic()
        self.title('Plotting 3D')

        x_span = self.Xn.max() - self.Xn.min()
        y_span = self.Yn.max() - self.Yn.min()

        # Get the points as a 2D NumPy array (N by 3)
        points = np.c_[self.Xn.reshape(-1), self.Yn.reshape(-1), np.zeros(self.nn)]

        u = self.u.reshape(-1)
        m0 = np.array(self.m0.vec[:self.nn])
        c = np.array(self.c.vec[:self.nn])
        print(f'{np.min(c)=} {np.max(c)=}')
        m = np.array(self.m.vec[:self.nn])
        u = u / np.max(u)
        m0 = m0 / np.max(m0)
        m = m0 * np.exp(-c)
        c = c / np.max(c)
        print(np.min(m), np.max(m))
        print(np.min(u), np.max(u))

        pv_mesh_u = pv.UnstructuredGrid({vtk.VTK_TRIANGLE: self.EL}, points)
        pv_mesh_u.points[:, 2] = u
        pv_mesh_u['values'] = u
        pv_mesh_m0 = pv_mesh_u.copy()
        pv_mesh_m0.points[:, 2] = m0
        pv_mesh_m0['values'] = m0
        pv_mesh_c = pv_mesh_u.copy()
        pv_mesh_c.points[:, 2] = 0.3 * c
        pv_mesh_c['values'] = c
        pv_mesh_m = pv_mesh_u.copy()
        pv_mesh_m.points[:, 2] = m
        pv_mesh_m['values'] = m
        pv_mesh_0 = pv_mesh_u.copy()
        pv_mesh_0.points[:, 2] *= 0
        pv_mesh_0['values'] *= 0
        pv_mesh_1 = pv_mesh_u.copy()
        pv_mesh_1.points[:, 2] *= 0
        pv_mesh_1.points[:, 2] += 1
        pv_mesh_1['values'] *= 0
        xmin, ymin, xmax, ymax = self.domain.bounds
        bounds = [xmin, xmax, ymin, ymax, 0, 1]


        pv.set_plot_theme('default')

        for subplot in 'ABCD':
            plotter = pv.Plotter(
                # shape=(2, 2),
                window_size=[2000, 2000],
                off_screen=True,  # Plots off screen when True. Helpful for saving screenshots without a window popping up.
                line_smoothing=True,
                point_smoothing=True,
                polygon_smoothing=True,
                border=False,
            )
            plotter.set_background('white')

            if subplot == 'A':
                """
                m_0 field plot
                """
                plotter.add_mesh(pv_mesh_m0,
                                 cmap=cc.cm.CET_R3,
                                 # show_edges=True,
                                 clim=[0, 1],
                                 show_scalar_bar=False)
                plotter.show_bounds(all_edges=True, bold=False,
                                    show_xaxis=False, show_yaxis=False, show_zaxis=False,
                                    show_xlabels=False, show_ylabels=False, show_zlabels=False,
                                    color='grey')

            if subplot == 'B':
                """
                c field plot
                """
                plotter.add_mesh(pv_mesh_c,
                                 cmap=cc.cm.CET_R3,
                                 # show_edges=True,
                                 show_scalar_bar=False)
                plotter.show_bounds(all_edges=True, bold=False,
                                    show_xaxis=False, show_yaxis=False, show_zaxis=False,
                                    show_xlabels=False, show_ylabels=False, show_zlabels=False,
                                    color='grey')

                plotter.add_mesh(pv_mesh_1,
                                 # cmap=plt.cm.Purples,
                                 opacity=0.5,
                                 # show_edges=True,
                                 color=True,
                                 # show_edges=True,
                                 show_scalar_bar=False)

                axyz = np.zeros([self.it + 1, 3])
                axyz[:, 2] = 1
                ac = np.full([self.it, 3], fill_value=2, dtype=np.int_)
                ac[:, 1] = np.arange(0, self.it, dtype=np.int_)
                ac[:, 2] = np.arange(1, self.it + 1, dtype=np.int_)
                ac = ac.reshape(-1)
                for ia in range(self.an):
                    axyz[:, :2] = self.AXY[ia, :self.it + 1, :]
                    path = pv.PolyData()
                    path.points = axyz
                    path.lines = ac
                    tube = path.tube(radius=0.01)
                    # tube.plot(smooth_shading=True)
                    if self.pv_path2[ia]:
                        plotter.remove_actor(self.pv_path2[ia])

                    self.pv_path2[ia] = plotter.add_mesh(tube, smooth_shading=True, color='gray')

                    for i in range(np.shape(axyz)[0]):
                        bxyz = np.copy(axyz[i, :])
                        bxyz[2] = 0
                        line = pv.Line(axyz[i, :], bxyz)
                        plotter.add_mesh(line, color="white")

            if subplot == 'C':
                """
                m field plot
                """
                plotter.add_mesh(pv_mesh_m,
                                 cmap=cc.cm.CET_R3,
                                 clim=[0, 1],
                                 # show_edges=True,
                                 show_scalar_bar=False)
                plotter.add_mesh(pv_mesh_1,
                                 # cmap=plt.cm.Purples,
                                 opacity=0.,
                                 # show_edges=True,
                                 color=True,
                                 # show_edges=True,
                                 show_scalar_bar=False)
                plotter.show_bounds(all_edges=True, bold=False,
                                    show_xaxis=False, show_yaxis=False, show_zaxis=False,
                                    show_xlabels=False, show_ylabels=False, show_zlabels=False,
                                    color='grey')

            if subplot == 'D':
                """
                u field plot
                """
                plotter.add_mesh(pv_mesh_0,
                                 # cmap=plt.cm.Purples,
                                 # opacity=[0.2, 1.0],
                                 # show_edges=True,
                                 color=True,
                                 show_edges=True,
                                 show_scalar_bar=False)
                plotter.add_mesh(pv_mesh_u,
                                 cmap=cc.cm.fire,
                                 # show_edges=True,
                                 show_scalar_bar=False)
                plotter.show_bounds(all_edges=True, bold=False,
                                    show_xaxis=False, show_yaxis=False, show_zaxis=False,
                                    show_xlabels=False, show_ylabels=False, show_zlabels=False,
                                    color='grey')

            plotter.enable_eye_dome_lighting()
            plotter.reset_camera(bounds=bounds)
            plotter.link_views()

            # # tuple: camera location, focus point, view-up vector
            plotter.camera_position = [(self.Xn.min() - 0.5 * x_span, self.Yn.min() - 3 * y_span, 3),
                                       (np.mean(self.Xn), np.mean(self.Yn), 0.5),
                                       (0, 0, 1)]

            # plotter.view_isometric()
            plotter.enable_parallel_projection()
            # plotter.show()
            plotter.screenshot(f'{self.results_dir}fem_3d_{self.it:06d}_{subplot}.png')
            plotter.close()

        # points = np.zeros([0, 3])
        # cells = np.zeros([0], dtype=np.int_)



        # self.pv_mesh.plot()
        # self.plotter.update()

        # mesh2 = mesh.copy()
        # print(mesh2.points.shape)
        # mesh2.points[:,2] = 2
        # self.plotter.add_mesh(mesh2, cmap=plt.cm.Purples)
        # contours = mesh2.contour()
        # self.plotter.add_mesh(contours, color='black', line_width=3,
        #                 show_scalar_bar=False)

        # Plot the gradient of u# Make a geometric object to use as the glyph
        # geom = pv.Arrow()  # This could be any dataset
        # Perform the glyph
        # arrows = mesh2.glyph(factor=0.1)
        # plotter.add_mesh(arrows, color="black")

        # self.plotter.show(screenshot=f'{self.results_dir}search3d_{self.it:06d}.png')

        self.toc()

    def save_state(self, filename):
        """Saves the state of the search (everything needed for continuing the simulation)."""
        self.tic()
        self.title('Saving state')
        self.log(f'it: {self.it}')
        self.log(f'State filename: {filename}')
        np.savez_compressed(filename,
                            it=self.it,
                            AXY=self.AXY,
                            AD=self.AD,
                            AVXY=self.AVXY,
                            c=self.c.vec,
                            gfu=self.gfu.vec,
                            # ggfu=self.ggfu.vec,
                            execution_time=self.execution_time,
                            eta=self.eta,
                            )
        self.toc()
        # self.trajectory_analysis()

    def load_state(self, it):
        """Loads the state of the search (everything needed for continuing the simulation)."""
        self.tic()
        self.title('Loading state')
        filename = f'{self.results_dir}/state_{it:06d}.npz'
        self.log(f'State filename: {filename}')
        if os.path.exists(filename):
            npz = np.load(filename, allow_pickle=True)
            self.log(f'Variables: {npz.files}')
        else:
            return False

        self.it = npz['it']
        self.log(f'it: {self.it}')
        _, nt, _ = np.shape(npz['AXY'])
        self.AXY[:, :nt, :] = npz['AXY']
        self.AD[:, :nt] = npz['AD']
        self.AVXY[:, :nt, :] = npz['AVXY']

        for i, c in enumerate(npz['c']):
            self.c.vec[i] = c
        for i, u in enumerate(npz['gfu']):
            self.gfu.vec[i] = u
        # print(np.nanmin(npz['gfu']), np.nanmax(npz['gfu']))
        Z = np.zeros(self.nn)
        for i, node in enumerate(self.nodes):
            Z[i] = self.gfu.vec[i]
        self.u = Z.real / np.max(Z.real)
        # Calculating the gradient function
        self.ggfu = ngs.grad(self.gfu)

        self.execution_time = npz['execution_time']
        self.eta = npz['eta']

        npz.close()
        self.m.Set(self.m0 * ngs.exp(-self.c))

        self.toc()

        self.partitioning = self.swarm_partitioning()
        # self.trajectory_analysis()
        return True

    def trajectory_analysis(self, axes=None):
        """Utility function for analyzing the produced trajectories. Called after each HedacFem.state_save_steps"""

        if self.AC.sum() == 0:
            # The analysis is not flexible enough to analyze cases where clearence(s) are not required
            return

        self.tic()
        self.title('Trajectory analysis')

        #seaborn.set_theme()
        N = 100 # Number of iteration included in time plots
        colors = seaborn.color_palette('deep')

        if axes:
            ax_eta, ax_kappa, ax_cl, ax_cmptime = axes
        else:
            fig = plt.figure(figsize=(10.1, 8.5),
                             # constrained_layout=True
                             )
            #fig.patch.set_facecolor('ivory')

            gs = gridspec.GridSpec(4, 1, height_ratios=[1, 1, 2, 1],)
            gs.update(left=0.08,
                      right=0.98,
                      wspace=0.1,
                      hspace=0.28,
                      top=0.975,
                      bottom=0.049)
            ax_eta = plt.subplot(gs[0, 0])
            ax_kappa = plt.subplot(gs[1, 0], sharex=ax_eta)
            ax_cl = plt.subplot(gs[2, 0], sharex=ax_eta)
            ax_cmptime = plt.subplot(gs[3, 0], sharex=ax_eta)

            subplot_label_pos = [0, 1.005]
            for ax, lbl in zip([ax_eta, ax_kappa, ax_cl, ax_cmptime], 'A B C D'.split()):
                ax.text(subplot_label_pos[0], subplot_label_pos[1], lbl,
                        # weight='bold',
                        fontsize=14,
                        horizontalalignment='left',
                        verticalalignment='bottom',
                        transform=ax.transAxes)

        if axes:
            if self.it < N:
                i1 = 0
                i2 = self.it + 1
                ax_eta.set_xlim(0, self.T[N])
            else:
                i1 = self.it - N
                i2 = self.it + 1
                ax_eta.set_xlim(self.T[i1], self.T[i2 - 1])
            ax_eta.fill_between(self.T[i1:i2], self.eta[i1:i2], color='C0', alpha=0.5)
            ax_eta.plot(self.T[i1:i2], self.eta[i1:i2], c='C0', lw=1.2)
        else:
            ax_eta.fill_between(self.T[:self.it + 1], self.eta[:self.it + 1], color='C0', alpha=0.5)
            ax_eta.plot(self.T[:self.it + 1], self.eta[:self.it + 1], c='C0', lw=1.2)
            ax_eta.set_xlim(0, self.T[self.it])
        ax_eta.set_ylabel('Survey accomplishment\n' + r'$\eta$ [-]')
        ax_eta.spines['top'].set_visible(False)
        ax_eta.spines['right'].set_visible(False)
        ax_eta.set_facecolor('whitesmoke')
        ax_eta.grid(axis='y', c='grey', alpha=0.3, lw=0.5, which='both')
        # ax_eta.set_yscale('log')
        ax_eta.set_ylim(0, 1)


        for ia in range(self.an):
            # kappa = np.abs(self.AD[ia, 1:self.it + 1] - self.AD[ia, :self.it]) / np.max(self.AV * self.dt / self.AR)
            # ax_kappa.plot(self.T[1:self.it + 1], kappa, lw=0.7, c=colors[2], ls='-')
            r = np.max(self.AV * self.dt / self.AR) / np.abs(self.AD[ia, 1:self.it + 1] - self.AD[ia, :self.it])
            ax_kappa.plot(self.T[1:self.it + 1], r, lw=0.7, c=colors[2], ls='-')
        ax_kappa.plot([], [], lw=0.7, c=colors[2], ls='-')
        # ax_kappa.plot(self.T[:self.it + 1], np.ones(self.it+1), lw=1.2, color=colors[0], ls='--',
        #          label='Minimal trajectory curvature')
        ax_kappa.axhline(1.0, lw=1.2, color=colors[3], label='Minimal turning radius')

        ax_kappa.set_ylabel('Normed radius\n' + r'$r / R$ [-]')
        ax_kappa.spines['top'].set_visible(False)
        ax_kappa.spines['right'].set_visible(False)
        ax_kappa.set_facecolor('whitesmoke')
        ax_kappa.grid(axis='y', c='grey', alpha=0.3, lw=0.5, which='both')
        ax_kappa.set_yscale('log')
        ax_kappa.plot([], [], lw=0.7, c=colors[2], ls='-', label='Trajectory radii')
        if axes:
            legend = ax_kappa.legend(loc='upper left', ncol=1)
            legend.get_frame().set_edgecolor('none')
            legend.get_frame().set_linewidth(0)
            legend.get_frame().set_alpha(0.3)
            ax_kappa.set_ylim(0.7, 1000)
        else:
            legend = ax_kappa.legend(loc='upper center', ncol=4)
            legend.get_frame().set_edgecolor('k')
            legend.get_frame().set_linewidth(0.5)
            legend.get_frame().set_alpha(0.8)
            ax_kappa.set_ylim(0.7, 1000)

        D = np.zeros([self.an, self.an + 1, self.it + 1]) * np.nan
        for ia1 in range(self.an):
            xy1 = self.AXY[ia1, :self.it + 1, :]
            for ia2 in range(ia1):
                if ia1 == ia2:
                    continue
                xy2 = self.AXY[ia2, :self.it + 1, :]
                d = np.linalg.norm(xy1 - xy2, axis=1) / self.AC[ia1]
                D[ia1, ia2, :] = d
                ax_cl.plot(self.T[:self.it + 1], d, lw=0.7, c=colors[5], ls='-')

            d = np.array(
                [self.domain.boundary.distance(shg.Point(xy1[i, 0], xy1[i, 1])) / self.AC[ia1] for i in
                 range(self.it + 1)])
            D[ia1, -1, :] = d
            ax_cl.plot(self.T[:self.it + 1], d * np.ones(self.it + 1), lw=0.7, c=colors[2], ls='-')

        ax_cl.plot([], [], lw=0.7, c=colors[5], ls='-', label='Distances between agents')
        ax_cl.plot([], [], lw=0.7, c=colors[2], ls='-', label='Distances from boundary')
        # ax_cl.plot(self.T[:self.it + 1], np.nanmin(D, axis=(0, 1)), lw=1.2, color=colors[0], ls='--',
        #          label='Minimal clearance for all agents')
        ax_cl.axhline(1.0, lw=1.2, color=colors[3], label='Minimal allowed clearance')

        ax_cl.set_yscale('log')
        if axes:
            legend = ax_cl.legend(loc='upper left', ncol=1)
            legend.get_frame().set_edgecolor('none')
            legend.get_frame().set_linewidth(0)
            legend.get_frame().set_alpha(0.3)
            ax_cl.set_ylim(0.7, np.nanmax(D) * 10)
        else:
            legend = ax_cl.legend(loc='upper center', ncol=4)
            legend.get_frame().set_edgecolor('k')
            legend.get_frame().set_linewidth(0.5)
            legend.get_frame().set_alpha(0.8)
            ax_cl.set_ylim(0.7, np.nanmax(D) * 2)

        ax_cl.set_ylabel('Normed distance\n' + r'$d / \delta$ [-]')
        ax_cl.spines['top'].set_visible(False)
        ax_cl.spines['right'].set_visible(False)
        # ax1.spines['bottom'].set_visible(False)
        # ax1.spines['left'].set_visible(False)
        ax_cl.set_facecolor('whitesmoke')
        ax_cl.grid(axis='y', c='grey', alpha=0.3, lw=0.5, which='both')


        ax_cmptime.bar(self.T[:self.it + 1] - self.dt/2, self.execution_time[0, :self.it + 1],
                width=self.dt, label='Coverage convolution',
                color=colors[0],
                # edgecolor='grey', lw=0.4,
                )
        ax_cmptime.bar(self.T[:self.it + 1] - self.dt/2, self.execution_time[1, :self.it + 1],
                width=self.dt, label='Potential field',
                color=colors[1],
                bottom=self.execution_time[0, :self.it + 1],
                # edgecolor='grey', lw=0.4,
                )
        ax_cmptime.bar(self.T[:self.it + 1] - self.dt/2, self.execution_time[2, :self.it + 1],
                width=self.dt, color=colors[4],
                bottom=self.execution_time[0, :self.it + 1] + self.execution_time[1, :self.it + 1],
                # edgecolor='grey', lw=0.4,
                label='Collision avoidance maneuver',
                )
        ax_cmptime.set_ylabel('Computation time\n[s]')
        ax_cmptime.set_xlabel('Search time [s]')

        if axes:
            ax_cmptime.set_ylim(0.0, np.nanmax(np.sum(self.execution_time, 0)) * 1.5)
            legend = ax_cmptime.legend(loc='upper left', ncol=1)
            legend.get_frame().set_edgecolor('none')
            legend.get_frame().set_linewidth(0)
            legend.get_frame().set_alpha(0.3)
        else:
            ax_cmptime.set_ylim(0.0, np.nanmax(np.sum(self.execution_time, 0)) * 1.4)
            legend = ax_cmptime.legend(loc='upper center', ncol=4)
            legend.get_frame().set_edgecolor('k')
            legend.get_frame().set_linewidth(0.5)
            legend.get_frame().set_alpha(0.8)
        ax_cmptime.spines['top'].set_visible(False)
        ax_cmptime.spines['right'].set_visible(False)
        # ax2.spines['bottom'].set_visible(False)
        # ax2.spines['left'].set_visible(False)
        ax_cmptime.set_facecolor('whitesmoke')
        ax_cmptime.grid(axis='y', c='grey', alpha=0.3, lw=0.5)
        self.log(f'Average computations times: {np.nanmean(self.execution_time, 1)}')
        self.log(f'Maximum computation time: {np.max(np.sum(self.execution_time, 0))}')

        if axes:

            axes[0].text(0.0, 1.1, f'Monitoring last {self.dt * 100} seconds',
                         # weight='bold',
                         fontsize=10,
                         horizontalalignment='left',
                         verticalalignment='bottom',
                         transform=axes[0].transAxes)

            for ax in axes:
                ax.axvline(self.T[self.it], color='k', lw=0.8)


        else:

            fig.align_ylabels()
            plt.savefig(f'{self.results_dir}search_analysis_{self.case_name}_{self.it:06d}.pdf',
                        dpi=600, facecolor=fig.get_facecolor()
                        )
            plt.close()

        self.toc()
