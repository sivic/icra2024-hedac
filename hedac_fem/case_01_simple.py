from hedacfem import HedacFem
import numpy as np
import netgen.read_gmsh as read_gsmh
import matplotlib.pyplot as plt

test = HedacFem('case_01_simple', delete_dir=False)

test.mesh = read_gsmh.ReadGmsh('case_01_simple.msh')
test.boundary_offset = 0.0

def m0(x, y):
    return 1.0

def phi(ia, x, y):
    return 1.5 * np.exp(-(x ** 2 + y ** 2) / (2 * 0.1 ** 2))

test.m0_fun = m0
na = 5  # Number of agents
test.AX0 = np.array([3, 4, 5, 10, 11])
test.AY0 = np.array([5, 5, 5, 0.5, 0.5])
test.AD0 = np.deg2rad([-90, -90, -90, 90, 90])
#test.AD0 = np.deg2rad([ 90, 90, 90, 90, 90, 90, -180, -180, -180, -180])
test.AV = np.ones(na) * 0.1 # m/s
test.AR = np.ones(na) * 0.1  # m
test.AC = np.ones(na) * 0.1 # m
# test.AA = np.ones(na) * 0.1
# test.AS = np.ones(na) * 10  # m
test.phi_fun = phi
test.AS = np.full(na, 120)

test.alpha = 0.2  # HEDAC's conduction parameter (greater alpha => global/broader search)
test.beta = 0.5

test.T = np.linspace(0, 600, 1501)  # Time discretization

test.plot_options['scale_xy'] = 0.05, -0.35
test.plot_options['scale_w'] = 1
test.plot_options['scale_units'] = 'm', 2.1
test.plot_options['ms'] = 0.02  # Size of the yellow arrow marking agent position and direction in the visualizations
test.plot_options['case_title'] = 'Test case 1: Simple domain'
test.state_save_steps = 1

test.initialize()
if not test.load_state(np.size(test.T) - 1):
    test.solve_trajecotries()
    test.plot_field(test.m, f'm_{test.it}.png')
    test.make_animation()

test.trajectory_analysis()

fig, ax = plt.subplots(figsize=(10.1, 4.65))
plt.subplots_adjust(left=0,
                    right=1,
                    bottom=0,
                    top=1)
fig.patch.set_facecolor('none')
test.plot_situation(ax, gradient_scale=25)
ax.set_xlim(-0.05, 12.65)
ax.set_ylim(-0.4, 5.45)
fig.savefig(f'{test.case_name}/{test.case_name}.png', facecolor=fig.get_facecolor())
plt.close(fig)
