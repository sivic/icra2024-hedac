# novigrad case
# bounding box coordinates
#   min: 45.314178805689956, 13.556105908180642
#   max: 45.318752100340554, 13.566289651156382
import matplotlib.pyplot as plt
from hedacfem import HedacFem
import numpy as np
import netgen.read_gmsh as read_gsmh

test = HedacFem('case_02_governors_island', delete_dir=False)

test.mesh = read_gsmh.ReadGmsh('governors_island_gmsh')
test.boundary_offset = 0.0

def m0(x, y):
    return 1.0

def phi(ia, x, y):
    # x is the heading direction
    # y is lateral direction
    mask = np.logical_and(np.logical_and(x > -10.88, x < 10.88),
                          np.logical_and(y > -14.52, y < 14.52))
    _phi = np.zeros_like(x)
    _phi[mask] = 0.1
    return _phi

test.m0_fun = m0
na = 8  # Number of agents
test.AX0 = np.array([-400, -350, -300, -250, -200, 225, 300, 375])
test.AY0 = np.array([-200, -200, -200, -200, -200, 150, 150, 150])
test.AD0 = np.deg2rad(np.full(na, 90))
test.AV = np.ones(na) * 2.0  # m/s
test.AR = np.ones(na) * 0.5  # m
test.AC = np.ones(na) * 1.2  # m
# test.AA = np.ones(na) * 1e-1
# test.AS = np.ones(na) * 10.0  # m
test.phi_fun = phi
test.AS = np.full(na, 16)

test.alpha = 2000  # HEDAC's conduction parameter (greater alpha => global/broader search)
test.beta = 1e-2

test.T = np.linspace(0, 60*30, 60*30+1)  # Time discretization (30 min)

test.plot_options['scale_xy'] = 350, -500
test.plot_options['scale_w'] = 100
test.plot_options['scale_units'] = 'm', 2.16
test.plot_options['ms'] = 3  # Size of the yellow arrow marking agent position and direction in the visualizations
test.plot_options['case_title'] = 'Test case 2: Governors Island'
test.state_save_steps = 1

test.initialize()
if not test.load_state(np.size(test.T) - 1):
    test.solve_trajecotries()
    test.plot_field(test.m, f'm_{test.it}.png')
    test.make_animation()

test.trajectory_analysis()

fig, ax = plt.subplots(figsize=(10.1, 8.7))
plt.subplots_adjust(left=0,
                    right=1,
                    bottom=0,
                    top=1)
fig.patch.set_facecolor('none')
test.plot_situation(ax, gradient_scale=2)
fig.savefig(f'{test.case_name}/{test.case_name}.png', facecolor=fig.get_facecolor())
plt.close(fig)
