Requirements:
- netgen and ngsolve for mesh and FEM
- shapely and descartes for geometry caluclations and visualization
- (in future) pymesh (https://pymesh.readthedocs.io/en/latest/) for mesh manipulation (instructions for installing in anaconda https://github.com/PyMesh/PyMesh/issues/110#issuecomment-422138489)